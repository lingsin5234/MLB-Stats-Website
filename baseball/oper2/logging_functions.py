# logging the processes to database
from datetime import datetime
from . import db_setup as dbs
from . import common_functions as cf
import time as t
import pandas as pd


def log_process(process_name, yearID, teamID, start_time):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # get process Id
    processID = cf.get_process(process_name)
    
    # current time
    curr_time = datetime.fromtimestamp(t.mktime(t.localtime()))
    
    finish_str = {
        'processID': processID,
        'yearID': yearID,
        'teamID': teamID,
        'time_elapsed': t.time() - start_time,
        'timestamp': curr_time
    }
    completion = pd.DataFrame([finish_str])
    completion.to_sql('process_log', conn, if_exists='append', index=False)

    return True


# test case logging
def log_test_case(yearID, teamID, stat_type):
    
    # get stat_typeID
    conn = dbs.engine.connect()
    result = conn.execute('SELECT Id FROM stat_type_dim WHERE stat_type=?', stat_type)
    stat_typeID = [Id for (Id,) in result][0]
    
    # current time
    curr_time = datetime.fromtimestamp(t.mktime(t.localtime()))
    
    # log test case
    test_case = {
        'yearID': yearID,
        'teamID': teamID,
        'stat_typeID': stat_typeID,
        'status': 'Not Started',
        'tests_count': 0,
        'init_time': curr_time
    }
    log_this = pd.DataFrame([test_case])
    log_this.to_sql('tests_log', conn, if_exists='append', index=False)
    
    '''
    # grab the test ID
    result = conn.execute('SELECT Id FROM tests_log WHERE yearID=? AND teamID=? AND stat_typeID=? AND status=?',
                          yearID, teamID, stat_typeID, 'Not Started')
    testID = [Id for (Id,) in result][0]
    '''
    
    return True


# log test case running
def log_test_running(testID):
    # log test completion
    conn = dbs.engine.connect()
    conn.execute('UPDATE tests_log SET status=? WHERE Id=?', 'Running', testID)
    
    return True


# log test case completion
def log_test_complete(testID, tests_count):
    
    # log test completion
    conn = dbs.engine.connect()
    conn.execute('UPDATE tests_log SET status=?, tests_count=? WHERE Id=?', 'Completed', tests_count, testID)
    
    return True


# test error logging
def log_test_error(playerID, stat_typeID, testID, error, self_value, test_value, known_error):
    
    # log test error
    conn = dbs.engine.connect()
    error_case = {
        'testID': testID,
        'playerID': playerID,
        'stat_typeID': stat_typeID,
        'error': error,
        'self_value': self_value,
        'test_value': test_value,
        'known_error': known_error,
        'resolved': False
    }
    log_this = pd.DataFrame([error_case])
    log_this.to_sql('test_errors', conn, if_exists='append', index=False)
    
    return True

