# tracking base running movement
# libraries
import re
from . import stat_collector as sc
from . import global_variables as gv
from . import pitcher_oper as po
from . import fielding_oper as fo


# stolen base tracking -- new version, no stat collection
def steal_processor2(this_line):

    # check if steal, then which base(s)
    if re.search(r'SB', this_line['play']):

        # SBH is listed first for double steal
        if re.search(r'SBH', this_line['play']):

            # steal
            this_line['SBH_runner'] = this_line['before_3B']
            this_line['R3_move'] = True
            this_line['R3_dest'] = 4
            # unearned?
            if bool(re.search(r'.*SBH([(/THE0-9NOBI)]+)?\(UR\).*', this_line['play'])):
                this_line['R3_dest'] = 5
            elif bool(re.search(r'.*SBH\(TUR\).*', this_line['play'])):
                this_line['R3_dest'] = 6

        # SB3 is listed next for double steal
        if re.search(r'SB3', this_line['play']):
            # if NOT an error on throwing
            if not (re.search(r'SB3.2-[3H]\(([0-9]+)?E([0-9]+)?', this_line['play'])):
                this_line['after_3B'] = this_line['before_2B']
                gv.bases_after = gv.bases_after[:len(gv.bases_after)-2] + '-2'
                # if there was a runner after, he would've went home/out at home
                # otherwise, no need to move the - ; e.g. -2- becomes --2- on steal of 3B
            this_line['SB3_runner'] = this_line['before_2B']
            this_line['R2_move'] = True
            this_line['R2_dest'] = 3

        # SB2 is listed last on steals
        if re.search(r'SB2', this_line['play']):
            # if NOT an error on throwing
            if not(re.search(r'SB2.1-[23H]\(([0-9]+)?E([0-9]+)?', this_line['play'])):
                this_line['after_2B'] = this_line['before_1B']

            this_line['SB2_runner'] = this_line['before_1B']
            this_line['R1_move'] = True
            this_line['R1_dest'] = 2

    # check if caught stealing, then which base(s); but NO ERROR for this particular CS
    if bool(re.search(r'CS', this_line['play'])) and \
            not(bool(re.search(r'CS[23H]\(([1-9]+)?E[1-9/TH]+\)', this_line['play']))):

        if re.search(r'CS2', this_line['play']):
            this_line['CS2_runner'] = this_line['before_1B']
            this_line['R1_move'] = True
            this_line['R1_dest'] = 0
            cs_play = re.sub(r'.*CS2\(([1-9/TH]+)[1-9]?.*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['PO'])

        if re.search(r'CS3', this_line['play']):
            this_line['CS3_runner'] = this_line['before_2B']
            this_line['R2_move'] = True
            this_line['R2_dest'] = 0
            cs_play = re.sub(r'.*CS3\(([1-9/TH]+)[1-9]?.*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['PO'])

        if re.search(r'CSH', this_line['play']):
            this_line['CSH_runner'] = this_line['before_3B']
            this_line['R3_move'] = True
            this_line['R3_dest'] = 0
            cs_play = re.sub(r'.*CSH\(([1-9/TH]+)[1-9]?.*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['PO'])

        # caught double steal DP
        if re.search(r'CS.*CS.*DP', this_line['play']):
            print('CAUGHT DOUBLE STEAL!!', this_line['play'])
            this_line['DP_flag'] = True

    # else if caught stealing but error
    elif bool(re.search(r'CS', this_line['play'])) and \
            bool(re.search(r'CS[23H]\(([1-9]+)?E[1-9/TH]+\)', this_line['play'])):

        if re.search(r'CS2', this_line['play']):
            this_line['CS2_runner'] = this_line['before_1B']
            this_line['R1_move'] = True
            this_line['R1_dest'] = 2
            cs_play = re.sub(r'.*CS2\(([1-9/THE]+)\).*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['E'])

        if re.search(r'CS3', this_line['play']):
            this_line['CS3_runner'] = this_line['before_2B']
            this_line['R2_move'] = True
            this_line['R2_dest'] = 3
            cs_play = re.sub(r'.*CS3\(([1-9/THE]+)\).*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['E'])
            # CS3(E1/TH).2-H(NR)(UR);1-3
        if re.search(r'CSH', this_line['play']):
            this_line['CSH_runner'] = this_line['before_3B']
            this_line['R3_move'] = True
            this_line['R3_dest'] = 4  # is it earned or unearned? maybe it's unearned if it says it's unearned
            # unearned?
            if bool(re.search(r'.*CSH([(/THE0-9NOBI)]+)?\(UR\).*', this_line['play'])):
                this_line['R3_dest'] = 5
            elif bool(re.search(r'.*CSH\(TUR\).*', this_line['play'])):
                this_line['R3_dest'] = 6
            cs_play = re.sub(r'.*CSH\(([1-9/THE]+)\).*', '\\1', this_line['play'])
            fo.fielding_assign_stats2(r'\D', cs_play, this_line, ['A'], ['E'])

    return this_line


# baserunner movements
def base_running3(this_line, run_play, pid, pitcher_id):

    # if there is running plays, then process
    if run_play is not None:

        # remove standalone (TH)
        if bool(re.search(r'\(TH\)', run_play)):
            run_play = re.sub(r'\(TH\)', '', run_play)
        # remove /RINT
        if bool(re.search(r'/RINT', run_play)):
            run_play = re.sub(r'/RINT', '', run_play)
        # remove (NR) -- handled in play_processor under RBIs
        if bool(re.search(r'\(NR\)', run_play)):
            run_play = re.sub(r'\(NR\)', '', run_play)
        # remove trailing numbers after the /TH
        if bool(re.search(r'/TH[1-9]', run_play)):
            run_play = re.sub(r'/TH[1-9]', '/TH', run_play)

        runners = run_play.split(';')

        for r in runners:
            this_line = runner_processor2(r, this_line, pitcher_id)

        # curr_bases = bases_occupied(this_line)

    # now assign bases based on the runner movement(s)
    this_line = assigning_bases(this_line)

    return this_line


# one-by-one base runner movements
def runner_processor2(runner, this_line, pitcher_id):

    # ----- SCORING ----- #
    if re.search(r'-H', runner):

        if re.search(r'3-', runner):
            this_line['R3_move'] = True
            this_line['R3_dest'] = 4
            # unearned?
            if bool(re.search(r'-H([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                this_line['R3_dest'] = 5
            elif bool(re.search(r'-H\(TUR\)', runner)):
                this_line['R3_dest'] = 6
        elif re.search(r'2-', runner):
            this_line['R2_move'] = True
            this_line['R2_dest'] = 4
            # unearned?
            if bool(re.search(r'-H([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                this_line['R2_dest'] = 5
            elif bool(re.search(r'-H\(TUR\)', runner)):
                this_line['R2_dest'] = 6
        elif re.search(r'1-', runner):
            this_line['R1_move'] = True
            this_line['R1_dest'] = 4
            # unearned?
            if bool(re.search(r'-H([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                this_line['R1_dest'] = 5
            elif bool(re.search(r'-H\(TUR\)', runner)):
                this_line['R1_dest'] = 6
        elif bool(re.search(r'B-', runner)):
            this_line['BT_dest'] = 4
            # unearned?
            if bool(re.search(r'-H([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                this_line['BT_dest'] = 5
            elif bool(re.search(r'-H\(TUR\)', runner)):
                this_line['BT_dest'] = 6

        # error on the play
        if bool(re.search('E', runner)):
            error_play = re.sub(r'[B123]-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)[1-9]?\).*', '\\2\\3', runner)
            fo.fielding_assign_stats2(r'E|\D', error_play, this_line, ['A'], ['E'])

            # multiple errors on same play! e.g. S7/L+.B-3(E6/TH)(E3/TH)
            if bool(re.search(r'[B123]-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)\(([1-9]+)?E([1-9/TH]+)\)', runner)):
                error2 = re.sub(r'[B123]-H(\(UR\))?\(([E1-9]+(/TH)?)[1-9]?\)\(([E1-9]+(/TH)?)[1-9]?\).*', '\\4', runner)
                fo.fielding_assign_stats2(r'E|\D', error2, this_line, ['A'], ['E'])

    # ----- SCORING ----- #

    # ----- STAND STILL ----- #
    elif bool(re.search(r'3-3', runner)):
        this_line['R3_move'] = False
    elif bool(re.search(r'2-2', runner)):
        this_line['R2_move'] = False
    elif bool(re.search(r'1-1', runner)):
        this_line['R1_move'] = False
    # ----- STAND STILL ----- #

    # ----- ADVANCED ----- #
    elif bool(re.search(r'2-3', runner)):

        this_line['R2_move'] = True
        this_line['R2_dest'] = 3
        this_line['after_3B'] = this_line['before_2B']

    elif bool(re.search(r'1-2', runner)):

        this_line['R1_move'] = True
        this_line['R1_dest'] = 2
        this_line['after_2B'] = this_line['before_1B']

    elif bool(re.search(r'1-3', runner)):

        this_line['R1_move'] = True
        this_line['R1_dest'] = 3
        this_line['after_3B'] = this_line['before_1B']
    # ----- ADVANCED ----- #

    # ----- BASE RUNNER OUTS - NO ERRORS AT ALL ----- #
    if bool(re.search(r'[B123]X[123H]', runner)) and not(bool(re.search(r'^([1-9]+)?E', this_line['play']))) and \
            not(bool(re.search(r'E', runner))):

        if bool(re.search(r'1X', runner)):
            this_line['R1_move'] = True
            this_line['R1_dest'] = 0
        elif bool(re.search(r'2X', runner)):
            this_line['R2_move'] = True
            this_line['R2_dest'] = 0
        elif bool(re.search(r'3X', runner)):
            this_line['R3_move'] = True
            this_line['R3_dest'] = 0
        else:
            this_line['BT_dest'] = 0

        made_play = re.sub(r'[B123]X[123H]\(([1-9/TH]+)[1-9]?\)', '\\1', runner)
        fo.fielding_assign_stats2(r'\D', made_play, this_line, ['A'], ['PO'])
    # ----- BASE RUNNER OUTS - NO ERRORS AT ALL ----- #

    # ----- BASE RUNNER SAFE ON THROWING ERROR ----- #  also includes ERROR + Recovery Out
    if bool(re.search(r'[B123]X[123H](\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)', runner)):

        # move the runner
        if bool(re.search(r'1X', runner)):
            this_line['R1_move'] = True
            if bool(re.search(r'1X1', runner)):
                this_line['R1_dest'] = 1
            elif bool(re.search(r'1X2', runner)):
                this_line['R1_dest'] = 2
            elif bool(re.search(r'1X3', runner)):
                this_line['R1_dest'] = 3
            elif bool(re.search(r'1XH', runner)):
                this_line['R1_dest'] = 4
                # unearned?
                if bool(re.search(r'XH([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                    this_line['R1_dest'] = 5
                elif bool(re.search(r'XH\(TUR\)', runner)):
                    this_line['R1_dest'] = 6

            # if out play made for same runner following the error
            if bool(re.search(r'1X[123H]\(([1-9]+)?E([1-9/TH]+)\)\(([1-9/TH]+)\)', runner)):
                this_line['R1_dest'] = 0
                putout = re.sub(r'1X[123H]\(([1-9]+)?E[1-9/TH]+\)\(([1-9/TH]+)[1-9]?', '\\2', runner)
                fo.fielding_assign_stats2(r'\D', putout, this_line, ['A'], ['PO'])

        elif bool(re.search(r'2X', runner)):
            this_line['R2_move'] = True
            if bool(re.search(r'2X2', runner)):
                this_line['R2_dest'] = 2
            elif bool(re.search(r'2X3', runner)):
                this_line['R2_dest'] = 3
            elif bool(re.search(r'2XH', runner)):
                this_line['R2_dest'] = 4
                # unearned?
                if bool(re.search(r'XH([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                    this_line['R2_dest'] = 5
                elif bool(re.search(r'XH\(TUR\)', runner)):
                    this_line['R2_dest'] = 6

            # if out play made for same runner following the error
            if bool(re.search(r'2X[23H]\(([1-9]+)?E([1-9/TH]+)\)\(([1-9/TH]+)\)', runner)):
                this_line['R2_dest'] = 0
                putout = re.sub(r'2X[23H]\(([1-9]+)?E[1-9/TH]+\)\(([1-9/TH]+)[1-9]?', '\\2', runner)
                fo.fielding_assign_stats2(r'\D', putout, this_line, ['A'], ['PO'])

        elif bool(re.search(r'3X', runner)):
            this_line['R3_move'] = True
            if bool(re.search(r'3X3', runner)):
                this_line['R3_dest'] = 3
            elif bool(re.search(r'3XH', runner)):
                this_line['R3_dest'] = 4
                # unearned?
                if bool(re.search(r'XH([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                    this_line['R3_dest'] = 5
                elif bool(re.search(r'XH\(TUR\)', runner)):
                    this_line['R3_dest'] = 6

            # if out play made for same runner following the error
            if bool(re.search(r'3X[3H]\(([1-9]+)?E([1-9/TH]+)\)\(([1-9/TH]+)\)', runner)):
                this_line['R3_dest'] = 0
                putout = re.sub(r'3X[3H]\(([1-9]+)?E[1-9/TH]+\)\(([1-9/TH]+)[1-9]?.*', '\\2', runner)
                fo.fielding_assign_stats2(r'\D', putout, this_line, ['A'], ['PO'])

        else:
            if bool(re.search(r'BX1', runner)):
                this_line['BT_dest'] = 1
            elif bool(re.search(r'BX2', runner)):
                this_line['BT_dest'] = 2
            elif bool(re.search(r'BX3', runner)):
                this_line['BT_dest'] = 3
            elif bool(re.search(r'BXH', runner)):
                this_line['BT_dest'] = 4
                # unearned?
                if bool(re.search(r'XH([(/THE0-9NORBI)]+)?\(UR\)', runner)):
                    this_line['BT_dest'] = 5
                elif bool(re.search(r'XH\(TUR\)', runner)):
                    this_line['BT_dest'] = 6

            # if out play made for same runner following the error
            if bool(re.search(r'BX[123H]\(([1-9]+)?E([1-9/TH]+)\)\(([1-9/TH]+)\)', runner)):
                this_line['BT_dest'] = 0
                putout = re.sub(r'BX[123H]\(([1-9]+)?E[1-9/TH]+\)\(([1-9/TH]+)[1-9]?.*', '\\2', runner)
                fo.fielding_assign_stats2(r'\D', putout, this_line, ['A'], ['PO'])

        error_play = re.sub(r'[B123]X[123H](\(UR\))?\(([1-9]+)?E([1-9])(/TH)?.*', '\\2\\3', runner)
        fo.fielding_assign_stats2(r'E|\D', error_play, this_line, ['A'], ['E'])

        # multiple errors on the same runner!
        if bool(re.search(
                r'[B123]X[123H](\(UR\))?(\(NR\))?\(([1-9]+)?E([1-9])(/TH[123H]?)?\)\(([1-9]+)?E([1-9])(/TH)?.*',
                runner)):
            error2 = re.sub(
                r'[B123]X[123H](\(UR\))?(\(NR\))?\(([1-9]+)?E([1-9])(/TH[123H]?)?\)\(([1-9]+)?E([1-9])(/TH)?.*',
                '\\6\\7', runner)
            fo.fielding_assign_stats2(r'E|\D', error2, this_line, ['A'], ['E'])
    # ----- BASE RUNNER SAFE ON THROWING ERROR ----- #

    # ----- BASE RUNNER OUT DESPITE THROWING ERROR (listed out of order) ----- #
    if bool(re.search(r'[B123]X[123H]\([1-9]+\)\(([1-9]+)?E([1-9/TH]+)\)', runner)):
        # putout play
        putout = re.sub(r'[B123]X[123H]\(([1-9]+)\)\(([1-9]+)?E([1-9/TH]+)[1-9]?.*', '\\1', runner)
        fo.fielding_assign_stats2(r'.*\(|\D', putout, this_line, ['A'], ['PO'])

        # error play
        error = re.sub(r'[B123]X[123H]\(([1-9]+)\)\(([1-9]+)?E([1-9/TH]+)[1-9]?.*', '\\2\\3', runner)
        fo.fielding_assign_stats2(r'.*\(|\D', error, this_line, ['A'], ['E'])

        # record the out
        if bool(re.search(r'^B', runner)):
            this_line['BT_dest'] = 0
        elif bool(re.search(r'^1', runner)):
            this_line['R1_move'] = True
            this_line['R1_dest'] = 0
        elif bool(re.search(r'^2', runner)):
            this_line['R2_move'] = True
            this_line['R2_dest'] = 0
        elif bool(re.search(r'^3', runner)):
            this_line['R3_move'] = True
            this_line['R3_dest'] = 0
    # ----- BASE RUNNER OUT DESPITE THROWING ERROR (listed out of order) ----- #

    # ----- BASE RUNNERS ADVANCE ON THROWING ERROR ----- #
    if bool(re.search(r'[B123]-[123](\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)', runner)):
        error_throw = re.sub(r'[B123]-[123](\(UR\))?\(([E1-9]+(/TH)?).*', '\\2', runner)
        fo.fielding_assign_stats2(r'E|\D', error_throw, this_line, ['A'], ['E'])

        # multiple errors on same play! e.g. S7/L+.B-3(E6/TH)(E3/TH)
        if bool(re.search(r'[B123]-[123](\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)\(([1-9]+)?E([1-9/TH]+)\)', runner)):
            error2 = re.sub(r'[B123]-[123](\(UR\))?\(([E1-9]+(/TH)?)[1-9]?\)\(([E1-9]+(/TH)?)[1-9]?\).*', '\\4', runner)
            fo.fielding_assign_stats2(r'E|\D', error2, this_line, ['A'], ['E'])
        # the running is handled above for baserunners and below for the batter
    # ----- BASE RUNNERS ADVANCE ON THROWING ERROR ----- #

    # ----- ERROR EVENT PLAY BUT A BASE RUNNER IS THROWN OUT ----- #
    if bool(re.search(r'^([1-9]+)?E', this_line['play'])) and \
            bool(re.search(r'[B123]X[123H](\(UR\))?\(([1-9/TH]+)\)', runner)):
        # error play taken care of in play_processor
        # error_play = re.sub(r'^([1-9]+)?E([1-9/TH]+)[1-9]?.*BX[123H]\(([1-9/TH]+)[1-9]?', '\\1\\2', this_line['play'])
        # fo.fielding_assign_stats2(r'\D', error_play, this_line, ['A'], ['E'])

        thrown_out = re.sub(r'[B123]X[123H]\(([1-9/TH]+)[1-9]?', '\\1', runner)
        fo.fielding_assign_stats2(r'\D', thrown_out, this_line, ['A'], ['PO'])

        # record the out
        if bool(re.search(r'^B', runner)):
            this_line['BT_dest'] = 0
        elif bool(re.search(r'^1', runner)):
            this_line['R1_move'] = True
            this_line['R1_dest'] = 0
        elif bool(re.search(r'^2', runner)):
            this_line['R2_move'] = True
            this_line['R2_dest'] = 0
        elif bool(re.search(r'^3', runner)):
            this_line['R3_move'] = True
            this_line['R3_dest'] = 0
        # if there is subsequent error on runner, then it is handled in error section above.
    # ----- ERROR EVENT PLAY BUT A BASE RUNNER IS THROWN OUT ----- #

    # ----- BATTER ADVANCE EXPLICITLY ----- #
    if bool(re.search(r'B-[123]', runner)):
        batter_move = re.sub(r'.*(B-[123]).*', '\\1', runner)
        if bool(re.search(r'-1', batter_move)):
            this_line['after_1B'] = this_line['playerID']
            this_line['BT_dest'] = 1
        elif bool(re.search(r'-2', batter_move)):
            this_line['after_2B'] = this_line['playerID']
            this_line['BT_dest'] = 2
        else:
            this_line['after_3B'] = this_line['playerID']
            this_line['BT_dest'] = 3
    # ----- BATTER ADVANCE EXPLICITLY ----- #

    # ----- Add Wild Pitch (WP) Flag ----- #
    if bool(re.search(r'\(WP\)', runner)):
        this_line['WP_flag'] = True
    # ----- Add Wild Pitch (WP) Flag ----- #

    return this_line


# track any runner movement for event
def check_runner_movement(this_line):
    # this will ignore SOLO HOME RUNS!
    if this_line['before_1B'] != this_line['after_1B']:
        return False
    if this_line['before_2B'] != this_line['after_2B']:
        return False
    if this_line['before_3B'] != this_line['after_3B']:
        return False
    return True


# bases occupied
def bases_occupied(this_line):

    # before
    bases_taken = []
    if this_line['before_1B']:
        bases_taken.append('1')
    else:
        bases_taken.append('-')
    if this_line['before_2B']:
        bases_taken.append('2')
    else:
        bases_taken.append('-')
    if this_line['before_3B']:
        bases_taken.append('3')
    else:
        bases_taken.append('-')
    bases_taken = ''.join(map(str, bases_taken))

    return bases_taken


# assigning bases
def assigning_bases(this_line):

    # handle runners, then batter
    # 3rd base runner
    if this_line['R3_move']:
        this_line = runner_advance(this_line, '3', this_line['R3_dest'])
    else:
        this_line = runner_advance(this_line, '3', 3)

    # 2nd base runner
    if this_line['R2_move']:
        this_line = runner_advance(this_line, '2', this_line['R2_dest'])
    else:
        this_line = runner_advance(this_line, '2', 2)

    # 1st base runner
    if this_line['R1_move']:
        this_line = runner_advance(this_line, '1', this_line['R1_dest'])
    else:
        this_line = runner_advance(this_line, '1', 1)

    # batter runner
    if this_line['BT_dest'] > 0:
        this_line = runner_advance(this_line, 'B', this_line['BT_dest'])

    return this_line


# runner advance
def runner_advance(this_line, runner, dest):

    if runner == '3':
        if dest == 0:
            this_line['after_3B'] = None
        elif dest == 3:
            this_line['after_3B'] = this_line['before_3B']
    elif runner == '2':
        if dest == 0:
            this_line['after_2B'] = None
        elif dest == 2:
            this_line['after_2B'] = this_line['before_2B']
        elif dest == 3:
            this_line['after_3B'] = this_line['before_2B']
    elif runner == '1':
        if dest == 0:
            this_line['after_1B'] = None
        elif dest == 1:
            this_line['after_1B'] = this_line['before_1B']
        elif dest == 2:
            this_line['after_2B'] = this_line['before_1B']
        elif dest == 3:
            this_line['after_3B'] = this_line['before_1B']
    else:
        if dest == 1:
            this_line['after_1B'] = this_line['playerID']
        elif dest == 2:
            this_line['after_2B'] = this_line['playerID']
        elif dest == 3:
            this_line['after_3B'] = this_line['playerID']

    return this_line

