# libraries
import re
from . import stat_collector as sc
from . import global_variables as gv
from . import base_running as br
import time as t
from . import pitcher_oper as po
from . import non_plate_appearance as npa
from . import fielding_oper as fo
from . import error_logger as el
from . import common_functions as cf
import pandas as pd
import numpy as np


# re-write to exclude stat collection, instead it will add columns of information
# such as RUNNERS, STATE, EVENT_CD as seen in the Baseball R textbook
def play_processor5(the_dict, games_roster, team_name, data_year):

    # the game id
    the_game_id = the_dict[0]['game_id']

    # handle pinch-hitters / pinch-runners staying in the game as fielders for GP; skip first inning
    pinch_hitters = []
    pinch_runners = []

    # rare cases
    # ph_ab_flag = 0  # flag -- PH during at bat
    previous_pitcher = None  # in case the pitcher is subbed out by fielder -- esp. NL games
    injured_strikeout = ''  # for injuries on 2 strike counts
    injury_PH = ''  # PH for injured player on 2 strike counts
    pinch_pitcher_flag = False
    add_pinch_pitcher = False
    debug_flag = False
    first_sub_occurred = False  # tracking substitutions, once first sub occured, run fielding assignments per line

    for i, this_line in enumerate(the_dict.values()):

        # DEBUG points
        # if (the_game_id == 'ARI201904050') & (this_line['half_innings'] == '7_1'):
        #     debug_flag = True

        # performance checkpoint
        q1_time = t.time()

        # clear rare-case flag -- actually don't need to.
        # if ph_ab_flag < i:
        #     ph_ab_flag = 0

        # assign all batting/fielding to be the same as previous line (except beginning of game)
        if first_sub_occurred:
            this_line = fo.lineup_assignments(this_line, prev_line)

        # check for new inning
        if this_line['new_half']:
            # clear inherited runners
            this_line['R1_inhp'] = this_line['R2_inhp'] = this_line['R3_inhp'] = None
            this_line['num_inh'] = 0
            routine_dp = False

            # assign the pitcher
            this_line['pitcherID'] = po.assign_pitcher2(this_line)

        # not new inning
        else:
            # start of game always new_half=True, so prev_line assigned at end of each iteration
            this_line['before_1B'] = prev_line['after_1B']
            this_line['before_2B'] = prev_line['after_2B']
            this_line['before_3B'] = prev_line['after_3B']
            this_line['inning'] = prev_line['inning']
            this_line['half'] = prev_line['half']
            this_line['half_innings'] = prev_line['half_innings']
            this_line['total_outs'] = prev_line['total_outs']
            this_line['pitcherID'] = prev_line['pitcherID']
            this_line['R1_inhp'] = prev_line['R1_inhp']
            this_line['R2_inhp'] = prev_line['R2_inhp']
            this_line['R3_inhp'] = prev_line['R3_inhp']
            this_line['num_inh'] = prev_line['num_inh']
            routine_dp = False

        # for the start of the top or bottom of 1st inning, record the GS/GP here
        # this is to account for cases of substitution in top half of inning -- e.g. Yelich case
        # if i == 0:
        #     sc.starters_stats(lineup, 0)  # away team is batting
        # else:
        #     if (this_line['half_innings'] == '1_1') and (prev_line['half_innings'] == '1_0'):
        #         sc.starters_stats(lineup, 1)  # home team is batting
        #         # print("HIT HERE", this_line['half_innings'], the_dict[i-1]['half_innings'])

        # performance checkpoint
        q2_time = t.time()
        
        # handle substitutions first
        if this_line['gm_type'] == 'sub':
            this_line['before_1B'] = this_line['after_1B'] = prev_line['after_1B']
            this_line['before_2B'] = this_line['after_2B'] = prev_line['after_2B']
            this_line['before_3B'] = this_line['after_3B'] = prev_line['after_3B']
            pid = this_line['playerID']
            this_line['EVENT_CD'] = gv.event_types['SUB']
            first_sub_occurred = True

            # add fielding team ID just for completion sake
            if this_line['half'] == '0':
                # away batting; home fielding
                this_line['fieldingTeamID'] = this_line['home_team']
            else:
                # home batting; away fielding
                this_line['fieldingTeamID'] = this_line['away_team']

            # batting team = the half inning
            if this_line['half'] == this_line['team_id']:
                
                # pinch runner only
                if this_line['fielding'] == '12':

                    # update the lineup for this person
                    substitution = fo.lineup_substitution2(this_line, pid, 'batting', 12)
                    this_line = substitution[0]
                    sub_player_id = substitution[1]
                    fielding_id = str(substitution[2])

                    # check the bases for the runner
                    if this_line['after_1B'] == sub_player_id:
                        this_line['after_1B'] = pid
                    elif this_line['after_2B'] == sub_player_id:
                        this_line['after_2B'] = pid
                    elif this_line['after_3B'] == sub_player_id:
                        this_line['after_3B'] = pid

                    # substitution first appearance in this game
                    this_line['sub_appearance'] = True

                    # DEBUG
                    # if the_game_id == 'TOR201707300' and this_line['half_innings'] == '3_0':
                    #     print(gv.bases_after, gv.runners_inherited)

                    # add pinch-runner to list to check next inning
                    pinch_runners.append({pid: fielding_id})

                    # if pinched for a pitcher (NL games, save pitcherID for next inning)
                    if fielding_id == '1':
                        previous_pitcher = sub_player_id

                # pinch hitter only
                elif this_line['fielding'] == '11':

                    # # check if this substitution is made during at bat -- other cases are treated as PH normally
                    # prev_ab = str(prev_line['pitch_count'])
                    # if bool(re.search(r'2$', prev_ab)):
                    #     ph_ab_flag = i + 1

                    # update the lineup for this person
                    substitution = fo.lineup_substitution2(this_line, pid, 'batting', 11)
                    this_line = substitution[0]
                    sub_player_id = substitution[1]
                    fielding_id = str(substitution[2])
                    batter_spot = str(substitution[3])

                    # substitution first appearance in this game
                    this_line['sub_appearance'] = True

                    # add pinch-hitter to list to check next inning
                    pinch_hitters.append({pid: fielding_id})

                    # if pinched for a pitcher (NL games, save pitcherID for next inning)
                    if fielding_id == '1':
                        previous_pitcher = sub_player_id

                    # if pinched hit for a batter who had TWO STRIKES
                    if (this_line['batting'] == batter_spot) & (bool(re.search('^[0123]2$', prev_line['pitch_count']))):
                        injured_strikeout = sub_player_id
                        injury_PH = pid

                # what other scenarios here?
                else:
                    print(this_line['half_innings'], this_line['team_id'],
                          this_line['playerID'], this_line['batting'], this_line['fielding'])
                    el.processing_errors('Missing Substitution Scenario: ' + this_line['half_innings'] +
                                         this_line['team_id'] + this_line['playerID'] + this_line['batting'] +
                                         this_line['fielding'],
                                         'play_processor', team_name, data_year,
                                         the_game_id, this_line['half_innings'])

            # fielding team = the half inning
            else:
                # pitching substitution
                if this_line['fielding'] == '1':

                    # update the lineup for this pitcher
                    substitution = fo.lineup_substitution2(this_line, pid, 'pitching', 1)
                    this_line = substitution[0]

                    # substitution first appearance in this game; False means ignore for the 'GP' stat collector
                    this_line['sub_appearance'] = True

                    # get previous pitcher ID
                    this_line['prev_pitcherID'] = prev_line['pitcherID']
                    previous_pitcher = prev_line['pitcherID']

                    # assign any inherited runners
                    this_line = po.add_inherited_runners(this_line)

                    # assign new pitcher
                    this_line['pitcherID'] = pid

                    # pinch-pitcher -- for my own comments sake
                    # relief pitcher comes in during the MIDDLE of an at-bat.
                    # Rule 9.16(h). This only matters on batter-favour count + a walk is issued.
                    if prev_line['pitch_count'] in ['20', '21', '30', '31', '32']:
                        pinch_pitcher_flag = True

                # other fielding substitutions
                else:
                    # update the lineup for this person
                    substitution = fo.lineup_substitution2(this_line, pid, 'fielding', int(this_line['fielding']))
                    this_line = substitution[0]

                    # add games played stat - as "batting" stat
                    # if substitution[2]:
                    #     sc.stat_collector(pid, lineup, this_line, ['GP'])

                    #  -----  i don't think this does anything...  -----  #
                    # check if pitcher was subbed out -- especially for NL games
                    keep_pitcher = prev_line['pitcherID']
                    if substitution[1] == keep_pitcher:
                        previous_pitcher = keep_pitcher

        # for plays
        elif this_line['gm_type'] == 'play':

            # store player_id, the play, and pitcher_id
            pid = this_line['playerID']
            hid = this_line['pitcherID']

            # divide up beginning scenario with the running scenarios
            split_play = this_line['play'].split('.')
            begin_play = split_play[0]
            run_play = None
            if len(split_play) > 1:
                run_play = split_play[1]

            # remove UREV, MREV and !
            begin_play = re.sub('/(MREV|UREV)', '', begin_play)
            begin_play = re.sub('!', '', begin_play)

            # divide up begin_play into plate-appearance (PA) plays and non-PA plays
            if bool(re.search(r'^(WP|NP|BK|PB|FLE|OA|SB|CS|PO|DI)', begin_play)):

                # handles all non-PA and running events thereafter
                this_line = npa.non_pa2(this_line, begin_play)

            # plate-appearance play
            else:

                # if it is a HIT
                if bool(re.search(r'^((S|D|T)([1-9]+)?/?|H/|HR|DGR)', begin_play)):

                    # if not error. if explicit batter movement, don't perform batter movement
                    if not(bool(re.search(r'E[0-9]+', begin_play))):

                        batter_move = bool(re.search('B(-|X)[123H]', this_line['play']))

                        # single
                        if re.search(r'^S', begin_play):
                            this_line['EVENT_CD'] = gv.event_types['S1B']
                            if not batter_move:
                                this_line['BT_dest'] = 1
                                this_line['after_1B'] = pid

                        # double
                        elif re.search(r'^D', begin_play):
                            this_line['EVENT_CD'] = gv.event_types['X2B']
                            if not batter_move:
                                this_line['BT_dest'] = 2
                                this_line['after_2B'] = pid

                        # triple
                        elif re.search(r'^T', begin_play):
                            this_line['EVENT_CD'] = gv.event_types['X3B']
                            if not batter_move:
                                this_line['BT_dest'] = 3
                                this_line['after_3B'] = pid

                        # home run
                        else:
                            this_line['EVENT_CD'] = gv.event_types['HR']
                            this_line['BT_dest'] = 4

                # Walk or Strikeout
                elif bool(re.search(r'^(I|IW|HP|W|K)', begin_play)):

                    # handle these scenarios normally
                    if bool(re.search(r'K', begin_play)):
                        this_line['EVENT_CD'] = gv.event_types['K']

                        # catchers credited with putout on standalone strikeouts
                        if bool(re.search(r'^K$', this_line['play'])):
                            fo.fielding_po3('2', '', this_line)

                        # handle defensive throws
                        elif bool(re.search(r'K[1-9]+', begin_play)):
                            k_play = re.sub(r'K([1-9]+).*', '\\1', begin_play)
                            fo.fielding_assign_stats2(r'', k_play, this_line, ['A'], ['PO'])

                        # catcher still credited with putout if subsequent play does not involve batter
                        elif not(bool(re.search(r'B(-|X)', this_line['play']))):
                            fo.fielding_po3('2', '', this_line)

                        # if injured strikeout scenario
                        if injured_strikeout != '':
                            this_line['injured_strikeout'] = injured_strikeout
                            injured_strikeout = ''

                    elif bool(re.search(r'HP', begin_play)):
                        this_line['EVENT_CD'] = gv.event_types['HBP']
                        this_line['BT_dest'] = 1

                    else:
                        this_line['EVENT_CD'] = gv.event_types['BB']
                        this_line['BT_dest'] = 1

                        # handle middle of at-bat walk on batter-favoured counts
                        if pinch_pitcher_flag:
                            this_line['pinch_pitcher'] = previous_pitcher
                            # this_line['R1_inhp'] = previous_pitcher  ## redundant, gets replaced in move_inherited
                            pinch_pitcher_flag = False
                            add_pinch_pitcher = True
                            # would they intentionally walk the batter?? hmm...

                        if bool(re.search(r'^(IW|I)', begin_play)):
                            this_line['EVENT_CD'] = gv.event_types['IBB']

                    # check if additional event happened
                    if bool(re.search(r'\+', begin_play)):

                        # run the NON-PA function
                        this_line = npa.non_pa2(this_line, begin_play)

                        # K+CS/DP Case
                        if bool(re.search(r'^K\+.*(DP|TP)', begin_play)):

                            # for CS, the base_running will handle the assists and putouts
                            # only take care of the DP/TP here
                            if not(bool(re.search(r'NDP', begin_play))) and bool(re.search(r'CS|PO', begin_play)):
                                if bool(re.search(r'DP', begin_play)):
                                    this_line['DP_flag'] = True
                                else:
                                    this_line['TP_flag'] = True

                        # K+E2/TH case
                        if bool(re.search(r'^K\+E2', begin_play)):
                            fo.fielding_assign_stats2(r'\D', '2', this_line, ['A'], ['E'])

                        # non_pa overwrites the EVENT_CD, write it back!
                        if bool(re.search(r'K', begin_play)):
                            this_line['EVENT_CD'] = gv.event_types['K']
                        elif bool(re.search(r'^W', begin_play)):
                            this_line['EVENT_CD'] = gv.event_types['BB']
                        elif bool(re.search(r'^IW', begin_play)):
                            this_line['EVENT_CD'] = gv.event_types['IBB']

                    # if no batter movements, then put batter on first!
                    if run_play is not None:
                        if not(bool(re.search(r'B', run_play))) and not(bool(re.search(r'K', begin_play))):
                            this_line['after_1B'] = pid
                            this_line['BT_dest'] = 1
                    else:
                        if not(bool(re.search(r'K', begin_play))):
                            this_line['after_1B'] = pid
                            this_line['BT_dest'] = 1

                    # handle batter interference and dp scenario
                    if bool(re.search(r'^K([1-9]+)?/(DP/BINT|BINT/DP)', begin_play)):
                        this_line['DP_flag'] = True
                        this_line['BINT_flag'] = True
                    elif bool(re.search(r'^K/BINT', begin_play)):
                        this_line['BINT_flag'] = True

                # Fielding Plays that are not FC
                elif bool(re.search(r'^([0-9]+)?E?[0-9]+', begin_play)):

                    # DP or TP -- exclude NDPs, so political
                    if bool(re.search(r'(DP|TP)', begin_play)) and not(bool(re.search(r'NDP', begin_play))):
                        this_line['EVENT_CD'] = gv.event_types['Out']

                        '''
                        - Handle the batter -- out or advance; move runner(s); apply assist(s) and put out
                        - Manage the baserunner outs -- as double play / triple play
                        - Manage the force-out runners -- out or advance
                        - Do not double-assign assists for baserunner + force-outs
                        - maximum of one assist per player per out recorded  -- so two assists for 363(1)43/GDP.2-3
                        '''
                        #  -----  BATTER IS OUT!  -----  #
                        check_batter = [p for p in re.sub(r'/.*', '', begin_play)]
                        if (check_batter[len(check_batter) - 1].isdigit()) or (bool(re.search(r'\(B\)', begin_play))):

                            # since batter is out, last fielder is putout, everyone else is assist
                            # unless otherwise specified - e.g. 43(B)6(1)/GDP
                            if bool(re.search(r'\(B\)', begin_play)):
                                # assign the batter put out
                                if bool(re.search(r'.*\)([1-9]+)\(B\)', begin_play)):
                                    # grab the last fielder, the fielder who made the previous putout
                                    double_play = re.sub(r'.*([1-9])\([123]\)([1-9]+)\(B\).*', '\\1\\2', begin_play)
                                else:
                                    double_play = re.sub(r'^([1-9]+)\(B\).*', '\\1', begin_play)
                                fo.fielding_assign_stats2(r'\D', double_play, this_line, ['A'], ['PO'])

                            else:
                                # assign the batter put out
                                if bool(re.search(r'.*\)([1-9]+)/', begin_play)):
                                    double_play = re.sub(r'.*([1-9])\([123]\)([1-9]+)/.*', '\\1\\2', begin_play)
                                else:
                                    double_play = re.sub(r'^([1-9]+)/.*', '\\1', begin_play)
                                fo.fielding_assign_stats2(r'\D', double_play, this_line, ['A'], ['PO'])

                            # assign the batter as OUT
                            this_line['BT_dest'] = 0
                            routine_dp = True

                        #  -----  BATTER IS SAFE  -----  #
                        else:
                            # assign the batter as SAFE
                            this_line['BT_dest'] = 1
                            # if ran into out, will be changed in base_running
                        #  ----------------------------  #

                        #  -----  HANDLE FORCE OUTS  -----  #
                        force_out_play = re.sub(r'/.*', '', begin_play)

                        # check (1), 1B runner is forced out
                        if bool(re.search(r'\(1\)', force_out_play)):
                            this_line['R1_move'] = True
                            this_line['R1_dest'] = 0
                            if bool(re.search(r'.*\)([1-9]+)\(1\)', force_out_play)):
                                # grab the last fielder, the fielder who made the previous putout
                                fo_play = re.sub(r'.*([1-9])\([B23]\)([1-9]+)\(1\).*', '\\1\\2', force_out_play)
                            else:
                                fo_play = re.sub(r'^([1-9]+)\(1\).*', '\\1', force_out_play)

                            fo.fielding_assign_stats2(r'\D', fo_play, this_line, ['A'], ['PO'])
                            routine_dp = True

                        # check (2), 2B runner is forced out
                        if bool(re.search(r'\(2\)', force_out_play)):
                            this_line['R2_move'] = True
                            this_line['R2_dest'] = 0
                            if bool(re.search(r'.*\)([1-9]+)\(2\)', force_out_play)):
                                # grab the last fielder, the fielder who made the previous putout
                                fo_play = re.sub(r'.*([1-9])\([B13]\)([1-9]+)\(2\).*', '\\1\\2', force_out_play)
                            else:
                                fo_play = re.sub(r'^([1-9]+)\(2\).*', '\\1', force_out_play)
                            fo.fielding_assign_stats2(r'\D', fo_play, this_line, ['A'], ['PO'])
                            routine_dp = True

                        # check (3), 3B runner is forced out
                        if bool(re.search(r'\(3\)', force_out_play)):
                            this_line['R3_move'] = True
                            this_line['R3_dest'] = 0
                            if bool(re.search(r'.*\)([1-9]+)\(3\)', force_out_play)):
                                # grab the last fielder, the fielder who made the previous putout
                                fo_play = re.sub(r'.*([1-9])\([B12]\)([1-9]+)\(3\).*', '\\1\\2', force_out_play)
                            else:
                                fo_play = re.sub(r'^([1-9]+)\(3\).*', '\\1', force_out_play)
                            fo.fielding_assign_stats2(r'\D', fo_play, this_line, ['A'], ['PO'])
                            routine_dp = True
                        #  -------------------------------  #

                    # check force out before normal outs
                    elif bool(re.search(r'^[0-9]+.*/FO', begin_play)):
                        this_line['EVENT_CD'] = gv.event_types['Out']
                        # pt = ['BF']

                        # a runner is out
                        if re.search(r'\(B\)', begin_play):
                            this_line['BT_dest'] = 0
                            fo.fielding_po3(begin_play, r'\(B\).*', this_line)  # record PO

                        # batter is safe, R1/2/3 is out
                        elif re.search(r'\([123]\)', begin_play):
                            # batter is safe
                            this_line['BT_dest'] = 1
                            this_line['after_1B'] = pid

                            # R1 is out
                            if re.search(r'\(1\)', begin_play):
                                this_line['R1_move'] = True
                                this_line['R1_dest'] = 0
                                fo.fielding_po3(begin_play, r'\(1\).*', this_line)  # record PO
                            # R2 is out
                            elif re.search(r'\(2\)', begin_play):
                                this_line['R2_move'] = True
                                this_line['R2_dest'] = 0
                                fo.fielding_po3(begin_play, r'\(2\).*', this_line)  # record PO
                            # R3 is out
                            else:
                                this_line['R3_move'] = True
                                this_line['R3_dest'] = 0
                                fo.fielding_po3(begin_play, r'\(3\).*', this_line)  # record PO

                        # assign assists
                        fielders = fo.fielding_unique(r'\([\dB]+\)|\D', begin_play)[0]
                        for idx in range(0, len(fielders)):
                            # record Assist for not-last fielder
                            ft = ['A']
                            fo.fielding_processor3(fielders[idx], this_line, ft)

                    # fielding error
                    elif bool(re.search(r'^([0-9]+)?E', begin_play)):
                        # batter is safe - unless specified in run_play
                        this_line['BT_dest'] = 1
                        this_line['EVENT_CD'] = gv.event_types['ERR']

                        # if not explicit, then place the batter
                        if not(bool(re.search('B(-|X)', this_line['play']))):
                            this_line['after_1B'] = pid

                        # assign assists and errors
                        error_play = re.sub(r'^([1-9]+)?E([1-9]).*', '\\1\\2', begin_play)
                        fo.fielding_assign_stats2(r'E|\D', error_play, this_line, ['A'], ['E'])

                    # normal out -- this should be last to handle all cases prior
                    elif bool(re.search(r'^[0-9]+', begin_play)):
                        this_line['EVENT_CD'] = gv.event_types['Out']

                        # this case might come back: 54(B)/BG25/SH.1-2
                        # putout by fielder not normally covering that base

                        # batter is out
                        this_line['BT_dest'] = 0

                        # record assist(s) and put out
                        normal_out = re.sub('/.*', '', begin_play)
                        fo.fielding_assign_stats2(r'\D', normal_out, this_line, ['A'], ['PO'])

                    # fielding plays that are not included above
                    else:
                        print('Fielding Plays not included: ', begin_play)
                        el.processing_errors('Fielding Plays not included: ' + begin_play,
                                             'play_processor', team_name, data_year,
                                             the_game_id, this_line['half_innings'])

                # Fielder's Choice
                elif bool(re.search(r'^FC', begin_play)):

                    # DPs are handled by the runner marked out.
                    before_outs = this_line['total_outs']
                    this_line['EVENT_CD'] = gv.event_types['FC']

                    # determine if anyone is out
                    if bool(re.search(r'[B123]X[23H]', this_line['play'])) and \
                            not(bool(re.search(r'[B123]X[23H]\([1-9]?E[1-9]+', this_line['play']))):
                        # this is handled in the base-running section
                        pass

                    # move batter unless explicit
                    if bool(re.search(r'B(-|X)[123H]', this_line['play'])):
                        # print("Explicit Batter - FC", this_line['play'])
                        # base-running section will move the batter accordingly
                        pass
                    else:
                        # move batter to 1B
                        this_line['BT_dest'] = 1
                        this_line['after_1B'] = pid

                    # if there was an out recorded, then add IP to pitching
                    if this_line['total_outs'] > before_outs:
                        pass

                # Catcher Interference
                elif bool(re.search(r'C/E[1-3]', begin_play)):
                    this_line['EVENT_CD'] = gv.event_types['CI']
                    this_line['BT_dest'] = 1
                    this_line['after_1B'] = pid

                    # catcher interference is an error on catcher unless C/E1 or C/E3
                    if bool(re.search(r'C/E1', begin_play)):
                        fo.fielding_assign_stats2(r'E|\D', 'E1', this_line, ['A'], ['E'])
                    elif bool(re.search(r'C/E3', begin_play)):
                        fo.fielding_assign_stats2(r'E|\D', 'E3', this_line, ['A'], ['E'])
                    else:
                        fo.fielding_assign_stats2(r'E|\D', 'E2', this_line, ['A'], ['E'])

                # find other plays:
                else:
                    print('Category Needed: ', begin_play)
                    el.processing_errors('Category Needed: ' + begin_play,
                                         'play_processor', team_name, data_year,
                                         the_game_id, this_line['half_innings'])

                # if pinch-pitcher flag was on, it can now be removed
                if pinch_pitcher_flag:
                    pinch_pitcher_flag = False

            # handle all base-running plays
            this_line = br.base_running3(this_line, run_play, pid, hid)

            # record the TP flag
            if bool(re.search(r'/(TP|GTP)', begin_play)):
                this_line['TP_flag'] = True
            # record the DP flag but not NDP
            elif bool(re.search(r'/(DP|GDP|LDP)', begin_play)):
                this_line['DP_flag'] = True

            # only GDP count; GTP do not. WOW!!!
            # most likely because TP ends the inning for sure. DP not always the case.
            if re.search('GDP', begin_play):
                this_line['GDP_flag'] = True

            # record any SH/SF plays
            if bool(re.search(r'/SH', begin_play)):
                this_line['SH_flag'] = True
            elif bool(re.search(r'/SF', begin_play)):
                this_line['SF_flag'] = True

            # record RINT plays - Runner Interference
            if bool(re.search(r'/RINT', this_line['play'])):
                this_line['RINT_flag'] = True

            # record FINT plays - Fan Interference
            if bool(re.search(r'/FINT', begin_play)):
                this_line['FINT_flag'] = True

            # record the outs based on the BT/R(123) destinations
            this_line = po.record_outs(this_line)

            # assign RBIs and tally runs scored
            if this_line['BT_dest'] >= 4:
                this_line['runs_scored'] += 1
                # award if B-H(E2)(UR)(NR), as the NR will cancel it out.
                if bool(re.search(r'B-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?\(NR\)', this_line['play'])):
                    this_line['RBI_awarded'] += 1
                # no RBI for SB/CS+error and K(+event), WP, PB, Balks, OA, Errors (Walks+event are recorded as Walks)
                elif this_line['EVENT_CD'] not in [3, 4, 6, 7, 8, 9, 10, 11, 12, 18]:
                    # no DP or Error
                    if not(this_line['DP_flag']) and \
                            not(bool(re.search(r'B-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1

            if this_line['R1_dest'] >= 4:
                this_line['runs_scored'] += 1
                # award if 1-H(E2)(UR)(NR), as the NR will cancel it out.
                if bool(re.search(r'1-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?\(NR\)', this_line['play'])):
                    this_line['RBI_awarded'] += 1
                # no RBI for SB/CS+error and K(+event), WP, PB, Balks, OA, Errors (Walks+event are recorded as Walks)
                elif this_line['EVENT_CD'] not in [3, 4, 6, 7, 8, 9, 10, 11, 12, 18]:
                    # no DP or Error
                    if not (this_line['DP_flag']) and \
                            not (bool(re.search(r'1-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1
                # RBI awarded, if also a runner at 3rd; this would trigger an NR that gets cancelled after
                elif (this_line['EVENT_CD'] == 18) & (prev_line['total_outs'] < 2) & (this_line['R3_move']):
                    if not(bool(re.search(r'1-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1

            if this_line['R2_dest'] >= 4:
                this_line['runs_scored'] += 1
                # score the SF an RBI unless ERROR but not NR; if NR, then add the RBI, deducted later
                if (this_line['SF_flag'] and not(bool(re.search(r'2-H(\(UR\))?\(([1-9]+)?E', this_line['play'])))) or \
                        (this_line['SF_flag'] and
                         bool(re.search(r'2-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?\(NR\)', this_line['play']))):
                    this_line['RBI_awarded'] += 1
                # award if 2-H(E2)(UR)(NR), as the NR will cancel it out.
                elif bool(re.search(r'2-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?\(NR\)', this_line['play'])):
                    this_line['RBI_awarded'] += 1
                # no RBI for SB/CS+error and K(+event), WP, PB, Balks, OA, Errors (Walks+event are recorded as Walks)
                elif this_line['EVENT_CD'] not in [3, 4, 6, 7, 8, 9, 10, 11, 12, 18]:
                    # S6/G.2XH(652E5)(UR);1X3(632) on 2 outs # 36(1)/FO/G.3-H;2XH(6E1)(UR);B-1 no outs
                    if bool(re.search(r'2XH(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?(;|$)', this_line['play'])):
                        pass
                    # not on W+WP
                    elif bool(re.search(r'W\+WP', this_line['play'])):
                        pass
                    # if double play but not routine
                    elif (this_line['DP_flag']) and not routine_dp:
                        this_line['RBI_awarded'] += 1
                    # no DP or Error; this should work: # S8/L.3-H;2XH(UR)(8E3)(NR);B-2
                    elif not(this_line['DP_flag']) and \
                            not(bool(re.search(r'2-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1
                # RBI awarded, if also a runner at 3rd; this would trigger an NR that gets cancelled after
                elif (this_line['EVENT_CD'] in [18, 19]) & (prev_line['total_outs'] < 2) & (this_line['R3_move']):
                    # E4/FO/G.3-H;2-H(NR);1-2 -- give RBI, cancelled later
                    # E5/FO/G.3-H;2-H(UR);1-3 -- do not give RBI
                    if bool(re.search(r'([1-9]+)?E.*2-H(\(UR\))?', this_line['play'])):
                        if bool(re.search(r'([1-9]+)?E.*2-H(\(UR\))?\(NR\)', this_line['play'])):
                            this_line['RBI_awarded'] += 1
                    elif not(bool(re.search(r'2-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1

            if this_line['R3_dest'] >= 4:
                this_line['runs_scored'] += 1
                # score the SF an RBI
                if this_line['SF_flag']:
                    this_line['RBI_awarded'] += 1
                # award if 3-H(E2)(UR)(NR), as the NR will cancel it out.
                elif bool(re.search(r'3-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?\(NR\)', this_line['play'])):
                    this_line['RBI_awarded'] += 1
                # no RBI for SB/CS+error and K(+event), WP, PB, Balks, OA, Errors (Walks+event are recorded as Walks)
                elif this_line['EVENT_CD'] not in [3, 4, 6, 7, 8, 9, 10, 11, 12, 18]:
                    # FC3/G.3XH(3E2)(UR);B-1
                    if bool(re.search(r'3XH(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)(\(UR\))?(;|$)', this_line['play'])):
                        pass
                    # W+SBH(UR).1-2
                    elif bool(re.search(r'SBH(\(UR\))?', this_line['play'])) and \
                            not(bool(re.search(r'SBH(\(UR\))?\(NR\)', this_line['play']))):
                        pass
                    # allow DP but not GDP on 3-H
                    elif not(this_line['GDP_flag']) and \
                            not(bool(re.search(r'3-H(\(UR\))?\(([1-9]+)?E', this_line['play']))):
                        this_line['RBI_awarded'] += 1
                # RBI for Errors or Fielder's Choice with runner at 3rd and it WAS less than two outs
                elif (this_line['EVENT_CD'] in [18, 19]) & (prev_line['total_outs'] < 2):
                    # but not another error on runner; if (UR)(NR), it will not enter this statement
                    if (bool(re.search(r'3-H(\(UR\))?\(([1-9]+)?E', this_line['play']))) and \
                            not(bool(re.search(
                                r'3-H(\(UR\))?\(([1-9]+)?E([1-9/TH]+)\)\(NR\)', this_line['play']))):
                        pass
                    else:
                        this_line['RBI_awarded'] += 1

            # find how many NR there are, and remove that many from RBI
            NR_count = len(re.findall(r'\((NR|NORBI)\)', this_line['play']))
            this_line['RBI_awarded'] -= NR_count
            if this_line['RBI_awarded'] < 0:
                this_line['RBI_awarded'] = 0

            # CHECK INHERITED RUNNERS AFTER EACH PLAY FOR REMOVAL #
            this_line = po.move_inherited_runners(this_line)

            # handle Bequeated Runners Scored for FIELDER's CHOICE! (aka the inherited runners)
            # make sure hte out was made on FC, and not an FC + ERROR
            # track # of inherited runners only outs where you are trading baserunners (FC / FO)
            # so FC + no error | FO out

            # prevents the MREV/UREV for searching for 'E'
            if this_line['EVENT_CD'] == 19:
                check_play = re.sub('/(MREV|UREV)', '', this_line['play'])
            else:
                check_play = this_line['play']

            if (this_line['recorded_outs'] == 1) & (
                    ((this_line['EVENT_CD'] == 19) & (not bool(re.search(r'E', check_play)))) |
                    ((this_line['EVENT_CD'] == 2) & (bool(re.search(r'/FO', this_line['play']))))):
                this_line = po.reassign_inherited(this_line, prev_line)

            # handle bequeated runners - double play
            elif this_line['recorded_outs'] == 2:
                # should handle this case: 62(3)3/GDP.2-3;1-2 then E6/TH/G.3-H(NR)(UR);2-3
                if (prev_line['R3_inhp'] is not None) & (this_line['R3_dest'] == 0) & (this_line['DP_flag']):
                    this_line = po.reassign_inherited(this_line, prev_line)
                elif (prev_line['R2_inhp'] is not None) & (this_line['R2_dest'] == 0) & (this_line['DP_flag']):
                    this_line = po.reassign_inherited(this_line, prev_line)

            # otherwise, remove inherited based on number of outs except batter outs
            elif (this_line['recorded_outs'] > 0) & (this_line['runs_scored'] == 0) & (this_line['BT_dest'] != 0) & \
                    (this_line['R1_move'] | this_line['R2_move'] | this_line['R3_move']):
                this_line['num_inh'] -= this_line['recorded_outs']
                if this_line['num_inh'] < 0:
                    this_line['num_inh'] = 0

                '''
                # DEBUG
                # if (this_line['pitcherID'] == 'altad001') & (the_game_id == 'HOU201909060'):
                if (this_line['pitcherID'] == 'oterd001') & (the_game_id == 'CLE201904202'):
                    print(sum(inh_runners), inh_runners)
                    print(sum(cur_runners), cur_runners)
                    print(previous_pitcher)
                    print(this_line)
                    quit()
                '''
            # if pinch_pitcher was added, add as inherited runner; only walk charged to previous pitcher
            if add_pinch_pitcher:
                if this_line['BT_dest'] == 1:
                    this_line['R1_inhp'] = this_line['pinch_pitcher']
                    this_line['num_inh'] += 1
                add_pinch_pitcher = False

            # remove the injury PH if this is his at bat; and clear the injured strikeout
            if this_line['playerID'] == injury_PH:
                injury_PH = ''
                injured_strikeout = ''

        # # performance checkpoint
        # q3_time = t.time()
        #
        # DEBUG
        # if this_line['play'] == 'S7/L+.B-3(E6/TH)(E3/TH)':
        #     print(this_line)
        #
        # set this line as previous line for next iteration
        prev_line = this_line
        #
        # print(the_game_id, this_line['play'], this_line['before_1B'], this_line['before_2B'], this_line['before_3B'],
        #       this_line['outs'], this_line['after_1B'], this_line['after_2B'], this_line['after_3B'], gv.bases_after,
        #       gv.runners_inherited)
        #
        # # performance checkpoint
        # q4_time = t.time()
        #
        # # store to log
        # fgp = open('GAMEPLAY.LOG', mode='a')
        #
        # # performance review
        # fgp.write('LINE #' + str(i) + '\n')
        # fgp.write('setup: ' + str(q2_time - q1_time) + '\n')
        # fgp.write('play/sub: ' + str(q3_time - q2_time) + '\n')
        # fgp.write('reassign: ' + str(q4_time - q3_time) + '\n')
        # fgp.write('total: ' + str(q4_time - q1_time) + '\n')
        # fgp.close()

    return the_dict
