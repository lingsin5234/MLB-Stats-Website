# libraries
from . import stat_collector as sc
from . import global_variables as gv
from . import fielding_oper as fo
from . import common_functions as cf
import re

# constant
max_pitch = 30  # maximum pitches thrown in at-bat, in case it ever gets beaten!


# assign the pitcher
# v2: no lineup table needed
def assign_pitcher2(this_line):

    # check which team is batting
    if this_line['team_id'] == '0':
        pitcher_id = this_line['home_P']
    else:
        pitcher_id = this_line['away_P']

    return pitcher_id


# pitching stats collector
def pitch_collector(hid, lineup, the_line, stat_types):

    # get game info
    game_info = pitch_get_game_info(lineup, the_line)
    team_name, game_id, this_half, actual_play, \
        num_outs, bases_taken, stat_team = game_info
    pitch_count = the_line['pitch_count']
    data_year = the_line['data_year']

    # for each stat_type, call stat_appender
    for s_type in stat_types:
        sc.stat_appender(hid, team_name, data_year, game_id, this_half, s_type, 1, actual_play, pitch_count,
                         num_outs, bases_taken, stat_team, 'pitching')

        # every time there is a Strikeout -- K, add Inn to the entire lineup; only if batter is out
        if s_type == 'K' and not(bool(re.search(r'B-[123H]', the_line['play']))) and \
                not(bool(re.search(r'BX[123H](\(UR\))?(\(NR\))?([1-9]?E[1-9])', the_line['play']))):
            fo.fielder_innings(lineup, the_line, team_name, data_year, game_id, this_half, actual_play, pitch_count,
                               num_outs, bases_taken, stat_team, 'Inn')

        # every time there is a Batter Faced -- BF, add BF for entire lineup for fielding stat
        if s_type == 'BF':
            fo.fielder_innings(lineup, the_line, team_name, data_year, game_id, this_half, actual_play, pitch_count,
                               num_outs, bases_taken, stat_team, 'BF')

    return True


# game info for pitcher collectors
def pitch_get_game_info(lineup, the_line):

    # game info values
    game_id = the_line['game_id']
    this_half = the_line['half_innings']
    actual_play = the_line['play']

    # get vis_team and home_team
    vis_team = lineup.loc[0, 'team_name']
    home_team = lineup.loc[10, 'team_name']

    # which team? find out in the_line -- THIS IS OPPOSITE to stat_collector!
    if the_line['half'] == '0':
        team_name = home_team  # home is pitching
        stat_team = 'HOME'
    else:
        team_name = vis_team  # visitor is pitching
        stat_team = 'VISIT'

    # gather more information about the play
    num_outs = the_line['outs']

    # base runners
    bases_taken = []
    if the_line['before_1B']:
        bases_taken.append('1')
    else:
        bases_taken.append('-')
    if the_line['before_2B']:
        bases_taken.append('2')
    else:
        bases_taken.append('-')
    if the_line['before_3B']:
        bases_taken.append('3')
    else:
        bases_taken.append('-')
    bases_taken = ''.join(map(str, bases_taken))

    return [team_name, game_id, this_half, actual_play, num_outs, bases_taken, stat_team]


# pitch count collector
def pitch_count_collector(pitcherID, playerID, lineup, the_line):

    # get game info
    game_info = pitch_get_game_info(lineup, the_line)
    team_name, game_id, this_half, actual_play, \
        num_outs, bases_taken, stat_team = game_info
    pitch_count = the_line['pitch_count']
    data_year = the_line['data_year']

    # the pitches
    pc = the_line['pitches']

    # pitches thrown - ignore N for no pitch
    thrown = re.subn('[A-MO-Z]', '', pc, max_pitch)[1]

    # strikes thrown
    strikes = re.subn('[CFKLMOQRSTXY]', '', pc, max_pitch)[1]

    # foul balls
    fouls = re.subn('[FLORT]', '', pc, max_pitch)[1]

    # balls thrown
    balls = re.subn('[BIPV]', '', pc, max_pitch)[1]

    # unknown or missed pitch - U

    # call stat_appender for PT, ST, FL, BT
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'PT', thrown, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'ST', strikes, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'FL', fouls, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'BT', balls, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')

    return True


# PITCHER SUBSTITUTION: assign the inherited baserunners: pid is the PREVIOUS pitcherID
def assign_baserunners(this_line, pid):

    br_num = len(gv.bases_after)
    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]

    # if there are baserunners, then they need to be inherited!
    if br_num > 0:
        for r in runners:
            if r is not None:
                if r in gv.runners_inherited:
                    # if runner is already inherited by a pitcher, then pass
                    pass
                else:
                    # otherwise, add the playerID: pitcherID key-pair
                    gv.runners_inherited[r] = pid

    return True


# AFTER A PLAY: evaluate the baserunners after a play for removal
def eval_baserunners(this_line):

    ri_num = len(gv.runners_inherited)
    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]

    # check inherited to see if they are still on base
    remove_keys = []
    if ri_num > 0:
        for i, key in enumerate(gv.runners_inherited):
            if key not in runners:
                # if not a baserunner anymore, pop it after
                remove_keys.append(key)

    for r in remove_keys:
        gv.runners_inherited.pop(r)

    return True


# PINCH-RUNNER: adjust inherited runner for pinch-runner: br_id is the PREVIOUS baserunner playerID
def pinch_runner_inherited(this_line):

    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]
    prev_runners = [this_line['before_1B'], this_line['before_2B'], this_line['before_3B']]

    # use SET difference set(b) - set(a), for set(b) values different in set(a)
    runner_list = list(set(prev_runners) - set(runners))
    br_id = ''
    # print(runner_list)
    if len(runner_list) > 0:
        br_id = runner_list[0]

    # if previous runner is on the runners_inherited list
    # then the pitching substitution was made while that runner was on base
    if br_id in gv.runners_inherited:
        # pitcherID of previous runner
        pid = gv.runners_inherited[br_id]

        # add new inherited runner (the pinch-runner) with pitcher assigned previously
        gv.runners_inherited[this_line['playerID']] = pid

        # pop the previous runner
        gv.runners_inherited.pop(br_id)

    # otherwise, the pitcher has not changed, so no need to add to runners_inherited

    return True


# ON RUNNER SCORING: check if runner is inherited, and assign pitcherID accordingly
def inherit_runner_scored(this_line, runner_id):

    # if runner id is part of inherited runners, return the pitcher id
    if runner_id in gv.runners_inherited:
        return gv.runners_inherited[runner_id]
    # otherwise, return the current pitcher id
    else:
        return this_line['pitcherID']


# adding inherited runners
def add_inherited_runners(this_line):

    # track number of inherited runners
    num_inherited = 0

    # R1
    if (this_line['before_1B'] is not None) & (this_line['R1_inhp'] is None):
        this_line['R1_inhp'] = this_line['prev_pitcherID']
        num_inherited += 1

    # R2
    if (this_line['before_2B'] is not None) & (this_line['R2_inhp'] is None):
        this_line['R2_inhp'] = this_line['prev_pitcherID']
        num_inherited += 1

    # R3
    if (this_line['before_3B'] is not None) & (this_line['R3_inhp'] is None):
        this_line['R3_inhp'] = this_line['prev_pitcherID']
        num_inherited += 1

    # add number of inherited runners
    this_line['num_inh'] += num_inherited

    return this_line


# move the inherited runners
def move_inherited_runners(this_line):

    # track runners scoring
    runners_scored = 0

    # R3
    if this_line['R3_move']:
        if this_line['R3_dest'] >= 4:
            this_line['inh_run1'] = cf.xstr(this_line['R3_inhp'])
            this_line['R3_inhp'] = None
            if this_line['R3_dest'] != 5:
                this_line['inh_earned1'] = this_line['inh_run1']
            runners_scored += 1
        elif this_line['R3_dest'] == 0:
            this_line['R3_inhp'] = None

    # R2
    if this_line['R2_move']:
        if this_line['R2_dest'] >= 4:
            if this_line['inh_run1'] == '':
                this_line['inh_run1'] = cf.xstr(this_line['R2_inhp'])
                if this_line['R2_dest'] != 5:
                    this_line['inh_earned1'] = this_line['inh_run1']
            else:
                this_line['inh_run2'] = cf.xstr(this_line['R2_inhp'])
                if this_line['R2_dest'] != 5:
                    if this_line['inh_earned1'] == '':
                        this_line['inh_earned1'] = this_line['inh_run2']
                    else:
                        this_line['inh_earned2'] = this_line['inh_run2']
            this_line['R2_inhp'] = None
            runners_scored += 1
        elif this_line['R2_dest'] == 3:
            this_line['R3_inhp'] = this_line['R2_inhp']
            this_line['R2_inhp'] = None
        elif this_line['R2_dest'] == 0:
            this_line['R2_inhp'] = None

    # R1
    if this_line['R1_move']:
        if this_line['R1_dest'] >= 4:
            if this_line['inh_run1'] == '':
                this_line['inh_run1'] = cf.xstr(this_line['R1_inhp'])
                if this_line['R1_dest'] != 5:
                    this_line['inh_earned1'] = this_line['inh_run1']
            elif this_line['inh_run2'] == '':
                this_line['inh_run2'] = cf.xstr(this_line['R1_inhp'])
                if this_line['R1_dest'] != 5:
                    if this_line['inh_earned1'] == '':
                        this_line['inh_earned1'] = this_line['inh_run2']
                    else:
                        this_line['inh_earned2'] = this_line['inh_run2']
            else:
                this_line['inh_run3'] = cf.xstr(this_line['R1_inhp'])
                if this_line['R1_dest'] != 5:
                    if this_line['inh_earned1'] == '':
                        this_line['inh_earned1'] = this_line['inh_run3']
                    elif this_line['inh_earned2'] == '':
                        this_line['inh_earned2'] = this_line['inh_run3']
                    else:
                        this_line['inh_earned3'] = this_line['inh_run3']
            this_line['R1_inhp'] = None
            runners_scored += 1
        elif this_line['R1_dest'] == 3:
            this_line['R3_inhp'] = this_line['R1_inhp']
            this_line['R1_inhp'] = None
        elif this_line['R1_dest'] == 2:
            this_line['R2_inhp'] = this_line['R1_inhp']
            this_line['R1_inhp'] = None
        elif this_line['R1_dest'] == 0:
            this_line['R1_inhp'] = None

    # reduce inherited (keep at 0 if below)
    this_line['num_inh'] -= runners_scored
    if this_line['num_inh'] < 0:
        this_line['num_inh'] = 0

    return this_line


# recording outs based on the TRUE + 0's
def record_outs(this_line):

    # handle batter, only 0's on PA events
    if (this_line['BT_dest'] == 0) & (this_line['EVENT_CD'] in gv.PA_events):
        this_line['recorded_outs'] += 1
        this_line['total_outs'] += 1

    # handle the runners, based on move flag = TRUE
    if (this_line['R1_move']) & (this_line['R1_dest'] == 0):
        this_line['recorded_outs'] += 1
        this_line['total_outs'] += 1
    if (this_line['R2_move']) & (this_line['R2_dest'] == 0):
        this_line['recorded_outs'] += 1
        this_line['total_outs'] += 1
    if (this_line['R3_move']) & (this_line['R3_dest'] == 0):
        this_line['recorded_outs'] += 1
        this_line['total_outs'] += 1

    return this_line


# re-assign inherited runners
def reassign_inherited(this_line, prev_line):

    # if no inherited, then leave
    if this_line['num_inh'] == 0:
        return this_line

    # previous inherited list
    prev_list = [prev_line['R3_inhp'], prev_line['R2_inhp'], prev_line['R1_inhp']]

    # if runner scored, remove from prev_list
    if this_line['R1_dest'] >= 4:  # in [4, 6]:
        prev_list.pop(2)
    if this_line['R2_dest'] >= 4:  # in [4, 6]:
        prev_list.pop(1)
    if this_line['R3_dest'] >= 4:  # in [4, 6]:
        prev_list.pop(0)

    # find current base runners
    br_list = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]

    # carry over if less than current runners; will carry over None if none available.
    # if sum(inh_runners) != this_line['num_inh']:
    '''
    # bases loaded case
    ryanb001-ryanb001-ryanb001 -- fielder's choice at home then becomes
    ryanb001-ryanb001-(pitcher) but should remain the same instead.
    
    # -- this case the if statement will catch, as the num_inh will reduce
    ryanb001-ryanb001-ryanb001 -- fielder's choice on other bases, but runner scores
    ryanb001-ryanb001-(pitcher) is outcome (same with error)
    
    # not bases loaded case
    ryanb001-NA-ryanb001 -- no runner at 2B
    ryanb001-(pitcher)-NA -- runner FO error, 1-3, B-2, 3-H(UR) but should become
    ryanb001-ryanb001-NA  -- scored runner was unearned, so this new runner is re-inherited
    
    # double play case
    ryanb001-ryanb001-ryanb001 -- home and 1st are out
    ryanb001-ryanb001-NA -- need to remove one
    '''
    prev_list = list(filter(None, prev_list))  # remove None
    # print(prev_list)
    # print(br_list)
    br_flag = 0
    for i, p in enumerate(prev_list):
        # go thru each base runner in reverse order (3, 2, 1)
        if (br_flag == 0) & (br_list[2] is not None):
            this_line['R3_inhp'] = prev_list[i]
            br_flag = 3
        elif (br_flag in [0, 3]) & (br_list[1] is not None):
            this_line['R2_inhp'] = prev_list[i]
            br_flag = 2
        else:
            this_line['R1_inhp'] = prev_list[i]
            # if i == 2, check if num_inh is 3, otehrwise, it was double play, do not add.
            if (i == 2) and (this_line['num_inh'] == 3) and (this_line['DP_flag']):
                # remove 1 inherited runner if there were 3 on a DP
                this_line['R1_inhp'] = None
                this_line['num_inh'] = 2

    return this_line
