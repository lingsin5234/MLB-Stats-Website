# functions used for navigating the lahman database
import sqlalchemy as sa
import pandas as pd


# read from lahman database merge with retrosheetID
def read_lahman(query, *args):

    # set up engine
    engine = sa.create_engine('sqlite:///lahmansbaseballdb.sqlite', echo=True)
    c = engine.connect()

    # execute query
    results = c.execute(query, args)
    output = results.fetchall()
    col_nm = results.keys()

    # retrosheetID
    retro = c.execute('SELECT * FROM People')
    retroDF = retro.fetchall()
    retroCol = retro.keys()

    # data frame results
    df = pd.DataFrame(output, columns=col_nm)
    retroDF = pd.DataFrame(retroDF, columns=retroCol)

    # merge only the retrosheetID
    # print(retroDF[["playerID", "retroID"]])
    df = df.merge(retroDF[["playerID", "retroID"]], on=["playerID"]).rename(
        columns={'playerID': 'lahmanID', 'retroID': 'playerID'})
    # print(df)

    return df


# get birth Year from lahman db; june 30 is cutoff; else + 1
def get_birthYear(retroID):
    
    # set up engine
    engine = sa.create_engine('sqlite:///lahmansbaseballdb.sqlite', echo=True)
    conn = engine.connect()
    
    # get birthdate
    query = 'SELECT birthYear, birthMonth FROM People WHERE retroID=?'
    results = conn.execute(query, retroID).fetchall()
    
    # get year of birth; june 30 is cutoff, if july or later, add 1 to birth year
    mob = [m for (y, m) in results][0]
    yob = [y for (y, m) in results][0]
    if mob >= 7:
        yob += 1
    
    return yob
