# this file is to run analysis on the baseball stats
from . import db_setup as dbs
from . import lahman_functions as lf
from . import common_functions as cf
import pandas as pd


# get season-by-season stats --- BATTING
def get_batting_stats(retroID):
    
    # get batting information
    conn = dbs.engine.connect()
    query = 'SELECT y.year, p.first_name, p.last_name, p.retroID, b.* FROM batting b ' \
            'LEFT JOIN players_dim p ON b.playerID=p.Id ' \
            'LEFT JOIN years_dim y ON b.yearID=y.Id WHERE p.retroID=?'
    results = conn.execute(query, retroID)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    # print(df)
    
    # group by to combine stints
    group_col = ['year', 'first_name', 'last_name', 'retroID']
    df = df.groupby(group_col, as_index=False)[[c for c in df.columns if c not in group_col]].sum()
    
    # get birth date from lahman db; age as of June 30
    yob = lf.get_birthYear(retroID)
    # print(yob)
    
    # remove ID columns, add age
    df.drop([col for col in df.columns if ('ID' in col) or ('Id' in col)], axis=1, inplace=True)
    df['AGE'] = df['year'].apply(lambda x: x - yob)
    # print(df)
    
    # OPS = on-base + slugging
    df.rename(columns={'2B': 'X2B', '3B': 'X3B'}, inplace=True)
    df['AVG'] = df.eval('H / AB')
    df['SLG'] = df.eval('(H - X2B - X3B - HR + 2 * X2B + 3 * X3B + 4 * HR) / AB')
    df['OBP'] = df[['H', 'BB', 'HBP']].sum(axis=1) / df[['AB', 'BB', 'HBP', 'SF']].sum(axis=1)
    df['OPS'] = df['SLG'] + df['OBP']
    # print(df)
    
    return df


# get season-by-season stats --- PITCHING
def get_pitching_stats(retroID):
    
    # get pitching information
    conn = dbs.engine.connect()
    query = 'SELECT y.year, p.first_name, p.last_name, p.retroID, h.* FROM pitching h ' \
            'LEFT JOIN players_dim p ON h.playerID=p.Id ' \
            'LEFT JOIN years_dim y ON h.yearID=y.Id WHERE p.retroID=?'
    results = conn.execute(query, retroID)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    # print(df)

    # group by to combine stints
    group_col = ['year', 'first_name', 'last_name', 'retroID']
    df = df.groupby(group_col, as_index=False)[[c for c in df.columns if c not in group_col]].sum()
    
    # get birth date from lahman db; age as of June 30
    yob = lf.get_birthYear(retroID)
    # print(yob)
    
    # remove ID columns, add age
    df.drop([col for col in df.columns if ('ID' in col) or ('Id' in col)], axis=1, inplace=True)
    df['AGE'] = df['year'].apply(lambda x: x - yob)
    # print(df)
    
    # ERA
    df['IP'] = df['IPouts'].apply(cf.convert_innings)
    df['ERA'] = df.eval('9 * ER / (IPouts / 3)')
    df['WHIP'] = df.eval('(BB + H) / (IPouts / 3)')
    df['K9'] = df.eval('9 * K / (IPouts / 3)')
    df['KBB'] = df.eval('K / BB')
    # print(df)
    
    return df


# get season-by-season stats --- FIELDING
def get_fielding_stats(retroID):
    
    # get fielding information
    conn = dbs.engine.connect()
    query = 'SELECT y.year, p.first_name, p.last_name, p.retroID, f.* FROM fielding f ' \
            'LEFT JOIN players_dim p ON f.playerID=p.Id ' \
            'LEFT JOIN years_dim y ON f.yearID=y.Id WHERE p.retroID=?'
    results = conn.execute(query, retroID)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    # print(df)

    # group by to combine stints
    group_col = ['year', 'first_name', 'last_name', 'retroID']
    df = df.groupby(group_col, as_index=False)[[c for c in df.columns if c not in group_col]].sum()
    
    # get birth date from lahman db; age as of June 30
    yob = lf.get_birthYear(retroID)
    # print(yob)
    
    # remove ID columns, add age
    df.drop([col for col in df.columns if ('ID' in col) or ('Id' in col)], axis=1, inplace=True)
    df['AGE'] = df['year'].apply(lambda x: x - yob)
    # print(df)
    
    return df


# get season-by-season %-stats for circular barplot
def get_circleBar_stats(retroID, playerType):

    # for batters, get batting and fielding
    if playerType == 'batting':
        df1 = get_batting_stats(retroID)
        df2 = get_fielding_stats(retroID)
        
        # AVG, SLG, OBP, OPS, K%, BB%, SB%, FLD%
        df = pd.merge(left=df1, right=df2, on=['year', 'first_name', 'last_name'])
        df['K%'] = df.eval('K / AB')
        df['BB%'] = df.eval('BB / AB')
        df['SB%'] = df.eval('SB_x / (SB_x + CS_x)')
        df['FLD%'] = df.eval('(PO + A) / (PO + A + E)')
        df = df[['first_name', 'year', 'AB', 'H', 'HR', 'AVG', 'SLG',
                 'OBP', 'OPS', 'K%', 'BB%', 'SB%', 'FLD%']].copy()
        df.fillna(0, inplace=True)
        # print(df)
    
    # for pitchers, get batting, pitching and fielding
    else:
        df1 = get_pitching_stats(retroID)
        df2 = get_fielding_stats(retroID)
        
        # ERA, WHIP, AVG-, K9, BB9, CS%, FLD%, HR/H
        df = pd.merge(left=df1, right=df2, on=['year', 'first_name', 'last_name'])
        df['IP'] = df['IPouts'].apply(cf.convert_innings)
        df['BB9'] = df.eval('BB / (IPouts / 3)')
        # df['AVG'] = df.eval('H / BF_x')  # future, when BF is implemented in fielding
        df['AVG'] = df.eval('H / BF')
        df['CS%'] = df.eval('CS / (SB + CS) * 9')  # x9 to balance it in chart
        df['FLD%'] = df.eval('(PO_y + A) / (PO_y + A + E) * 9')  # x9 to balance it in chart
        df['HR%'] = df.eval('HR / H * 9')  # x9 to balance it in chart
        df = df[['first_name', 'year', 'W', 'K', 'ER', 'IP', 'ERA', 'WHIP',
                 'AVG', 'K9', 'BB9', 'HR%', 'CS%', 'FLD%']].copy()
        df.fillna(0, inplace=True)
        # print(df)

    return df

'''
get_batting_stats('suzui001')
get_pitching_stats('hallr001')
get_pitching_stats('kersc001')
get_batting_stats('bettm001')
get_pitching_stats('verlj001')
get_batting_stats('staim001')
'''
# get_circleBar_stats('suzui001', 'batter')
# get_circleBar_stats('hallr001', 'pitcher')
