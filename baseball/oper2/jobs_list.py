# list of jobs (processes) to be run, similar to what running_tests.py was doing before manually
from . import import_retrosheet as ir
from . import extract_teams_players as etp
from . import process_imports as pi
from . import stat_collector as sc
from . import lahman_test as lt
from . import db_init as dbi
from . import db_setup as dbs
from . import roster_load as rst
from . import logging_functions as lf
from . import gameplay_load as gs
from . import common_functions as cf
from . import stats_load as ss
from . import class_structure as cs
from . import clear_staging as cg
import pandas as pd
import time as t


year = 2019
# --- SETUP --- #
try:
    # initial db setup
    dbi.db_init()
except Exception:
    print("Database already exists")
    t.sleep(5)
else:
    print("Database initial setup completed.")


def run_jobs(year):
    
    # --- JOB 1 --- #
    # import_year
    ir.import_data(year)
    
    # get year ID
    yearID = cf.get_year(year)
    
    # mark year imported
    conn = dbs.engine.connect()
    conn.execute('UPDATE import_years SET imported=? WHERE yearID=?', True, yearID)
    
    # once this is done, add a new entry for imported years
    results = conn.execute('SELECT yearID FROM import_years WHERE imported=?', True)
    completed_years = '(' + ','.join([str(Id) for (Id,) in results]) + ')'
    results = conn.execute('SELECT Id FROM years_dim WHERE Id NOT IN {} ORDER BY year DESC'.format(completed_years))
    latest_yearID = [Id for (Id,) in results][0]
    
    # insert into import_years as the next year to work on
    year_dict = {'yearID': latest_yearID, 'imported': False}
    conn.execute(cs.import_years.insert(), year_dict)
    
    # --- JOB 2 --- #
    # extract teams
    etp.extract_teams(year)
    
    # --- JOB 3 --- #
    # extract rosters
    etp.extract_players(year)
    
    # --- JOB 4 --- #
    # load players, rosters
    rst.roster_load(year)
    
    # --- JOB 5 x num of teams --- #
    # main processing for the gameplay by home team
    teams = cf.get_teams_list(year)
    for team in teams:
        pi.process_data_single_team(year, team)
    
    # --- JOB 6 (part a) x num of teams --- #
    # stat collector and staging the stats tables
    conn = dbs.engine.connect()
    for team in teams:
        
        # start time
        start_time = t.time()
        
        # get gameplay
        results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=? AND home_team=?', year, team)
        gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
        
        #  -- run stat collector, stats staging and tests -- #
        # batting
        stat_output = sc.stat_collector2(gameplay, 'batting')
        ss.stats_staging(stat_output, 'batting')
    
        # pitching
        stat_output = sc.stat_collector2(gameplay, 'pitching')
        ss.stats_staging(stat_output, 'pitching')
    
        # fielding
        stat_output = sc.stat_collector2(gameplay, 'fielding')
        ss.stats_staging(stat_output, 'fielding')
        
        # log completion
        lf.log_process('stat_collection', cf.get_year(year), cf.get_team(year, team), start_time)
        
        # add test case logs for each stat_type
        lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'batting')
        lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'pitching')
        lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'fielding')
    
    # --- JOB 7 x num of teams --- #
    # running batting test cases
    for team in teams:
        
        # start time
        start_time = t.time()
    
        # get test case
        testID = cf.get_test_case(year, team, 'batting')
    
        # run test case
        lt.test_lahman2(year, team, 'batting', testID)
    
    # --- JOB 8 x num of teams --- #
    # running pitching test cases
    for team in teams:
        
        # start time
        start_time = t.time()
        
        # get test case
        testID = cf.get_test_case(year, team, 'pitching')
        
        # run test case
        lt.test_lahman2(year, team, 'pitching', testID)
    
    # --- JOB 9 x num of teams --- #
    # running fielding test cases
    for team in teams:
        
        # start time
        start_time = t.time()
        
        # get test case
        testID = cf.get_test_case(year, team, 'fielding')
        
        # run test case
        lt.test_lahman2(year, team, 'fielding', testID)
    
    # --- JOB 10 & 11 x num of teams --- #
    # load games and gameplay
    for team in teams:
        
        # run gameplay load
        gs.gameplay_load(cf.get_year(year), year, cf.get_team(year, team), team)
    
    # --- JOB 12 x num of teams --- #
    # run batting stats staging
    t1 = t.time()
    ss.stats_load(cf.get_year(year), 'batting')
    lf.log_process('batting_load', cf.get_year(year), None, t1)
    
    # --- JOB 13 x num of teams --- #
    # run pitching stats staging
    t2 = t.time()
    ss.stats_load(cf.get_year(year), 'pitching')
    lf.log_process('pitching_load', cf.get_year(year), None, t2)
    
    # --- JOB 14 x num of teams --- #
    # run fielding stats staging
    t3 = t.time()
    ss.stats_load(cf.get_year(year), 'fielding')
    lf.log_process('fielding_load', cf.get_year(year), None, t3)
    
    # --- JOB 15 --- #
    # clear staging for gameplay
    t4 = t.time()
    print("Gameplay Staging Cleared: ", cg.clear_gameplay_staging())
    
    # clear staging for batting, pitching, fielding
    print("Stats Staging Cleared: ", cg.clear_stats_staging())
    lf.log_process('clear_staging', cf.get_year(year), None, t4)
    
    # NOTE: cf.get_year(year) are too repetitive? that's one call each time just to grab the yearID?
    # but necessary if jobs are all run at different times (e.g. separately) rather than all at once

    return True


# --- LOOP --- #
for yr in reversed(range(1998, 2020)):
    run_jobs(yr)
