# initial entries for the database
from . import db_setup as dbs
# from . import postgres_connect as pgs
from . import class_structure as cs
from datetime import datetime as dt


# function to run the initial declarations
def db_init():
    
    # declare database
    # conn = pgs.engine.connect()
    conn = dbs.engine.connect()
    conn.fast_executemany = True
    
    # YEARS
    current_year = dt.today().year
    start_year = 1916  # this is the earliest record in retrosheet
    years = list(range(start_year, current_year))  # 1916 to 2019 (2020 - 1 when using range)
    conn.execute(cs.years_dim.insert(), [{'year': yr} for yr in years])
    
    # STAT TYPES
    stat_types = ['batting', 'pitching', 'fielding']
    conn.execute(cs.stat_type.insert(), [{'stat_type': st} for st in stat_types])
    
    # PROCESSES
    processes = [{'name': 'import_year', 'group_name': 'ETL Year and Rosters',
                  'desc': 'imports a year of retrosheet data'},
                 
                 {'name': 'extract_teams', 'group_name': 'ETL Year and Rosters',
                  'desc': 'extract team names for year imported'},
                 
                 {'name': 'extract_rosters', 'group_name': 'ETL Year and Rosters',
                  'desc': 'extract rosters for year imported'},
                 
                 {'name': 'roster_load', 'group_name': 'ETL Year and Rosters',
                  'desc': 'check and load rosters to players and rosters tables'},
                 
                 {'name': 'gameplay_process', 'group_name': 'Gameplay Processing',
                  'desc': 'process the game play for team by team and load to staging'},
                 
                 {'name': 'stat_collection', 'group_name': 'Stat Collection',
                  'desc': 'from staging, set the bat/pitch/field testing tables'},
                 
                 {'name': 'batting_test', 'group_name': 'Run Test Cases',
                  'desc': 'run batting test cases vs the gameplay data'},
                 
                 {'name': 'pitching_test', 'group_name': 'Run Test Cases',
                  'desc': 'run pitching test cases vs the gameplay data'},
                 
                 {'name': 'fielding_test', 'group_name': 'Run Test Cases',
                  'desc': 'run fielding test cases vs the gameplay data'},
                 
                 {'name': 'games_load', 'group_name': 'Load Games',
                  'desc': 'update the games_dim table with latest game_ids'},
                 
                 {'name': 'gameplay_load', 'group_name': 'Load Gameplay',
                  'desc': 'update the gameplay table with latest season of gameplay'},
                 
                 {'name': 'batting_load', 'group_name': 'Load Statistics',
                  'desc': 'move stat collection data to batting'},
                 
                 {'name': 'pitching_load', 'group_name': 'Load Statistics',
                  'desc': 'move stat collection data to pitching'},
                 
                 {'name': 'fielding_load', 'group_name': 'Load Statistics',
                  'desc': 'move stat collection data to fielding'},
                 
                 {'name': 'clear_staging', 'group_name': 'Load Statistics',
                  'desc': 'clear staging tables for gameplay and stats'}]
    conn.execute(cs.process.insert(), processes)
    
    # POSITIONS (FIELDING)
    positions = [{'pos_abb': 'P', 'pos_name': 'Pitcher', 'pos_num': 1},
                 {'pos_abb': 'C', 'pos_name': 'Catcher', 'pos_num': 2},
                 {'pos_abb': '1B', 'pos_name': 'First Baseman', 'pos_num': 3},
                 {'pos_abb': '2B', 'pos_name': 'Second Baseman', 'pos_num': 4},
                 {'pos_abb': '3B', 'pos_name': 'Third Baseman', 'pos_num': 5},
                 {'pos_abb': 'SS', 'pos_name': 'Shortstop', 'pos_num': 6},
                 {'pos_abb': 'LF', 'pos_name': 'Left Fielder', 'pos_num': 7},
                 {'pos_abb': 'CF', 'pos_name': 'Center Fielder', 'pos_num': 8},
                 {'pos_abb': 'RF', 'pos_name': 'Right Fielder', 'pos_num': 9},
                 {'pos_abb': 'DH', 'pos_name': 'Designated Hitter', 'pos_num': 10},
                 {'pos_abb': 'PH', 'pos_name': 'Pinch Hitter', 'pos_num': 11},
                 {'pos_abb': 'PR', 'pos_name': 'Pinch Runner', 'pos_num': 12},
                 {'pos_abb': 'IF', 'pos_name': 'Infielder', 'pos_num': 13},
                 {'pos_abb': 'OF', 'pos_name': 'Outfielder', 'pos_num': 14}]
    conn.execute(cs.positions.insert(), positions)
    
    # EVENTS (from Retrosheet)
    events = [{'event_cd': 0, 'desc': 'Unknown Event'},
              {'event_cd': 1, 'desc': 'No Event'},
              {'event_cd': 2, 'desc': 'Generic Out'},
              {'event_cd': 3, 'desc': 'Strikeout'},
              {'event_cd': 4, 'desc': 'Stolen Base'},
              {'event_cd': 5, 'desc': 'Defensive Indifference'},
              {'event_cd': 6, 'desc': 'Caught Stealing'},
              {'event_cd': 7, 'desc': 'Pick-off Error'},
              {'event_cd': 8, 'desc': 'Picked-off'},
              {'event_cd': 9, 'desc': 'Wild Pitch'},
              {'event_cd': 10, 'desc': 'Passed Ball'},
              {'event_cd': 11, 'desc': 'Balk'},
              {'event_cd': 12, 'desc': 'Other Running Plays'},
              {'event_cd': 13, 'desc': 'Foul Fly Ball Error'},
              {'event_cd': 14, 'desc': 'Walk'},
              {'event_cd': 15, 'desc': 'Intentional Walk'},
              {'event_cd': 16, 'desc': 'Hit By Pitch'},
              {'event_cd': 17, 'desc': 'Catcher Interference'},
              {'event_cd': 18, 'desc': 'Error'},
              {'event_cd': 19, 'desc': 'Fielders Choice'},
              {'event_cd': 20, 'desc': 'Single'},
              {'event_cd': 21, 'desc': 'Double'},
              {'event_cd': 22, 'desc': 'Triple'},
              {'event_cd': 23, 'desc': 'Home Run'},
              {'event_cd': 24, 'desc': 'Missing Play'},
              {'event_cd': 25, 'desc': 'Substitution'}]
    conn.execute(cs.events.insert(), events)
    
    # LAHMAN CONVERT
    lahman = [{'stat_typeID': 1, 'my_col': 'GP', 'lahman_col': 'G'},
              {'stat_typeID': 1, 'my_col': 'K', 'lahman_col': 'SO'},
              {'stat_typeID': 1, 'my_col': 'GDP', 'lahman_col': 'GIDP'},
              {'stat_typeID': 2, 'my_col': 'GP', 'lahman_col': 'G'},
              {'stat_typeID': 2, 'my_col': 'BF', 'lahman_col': 'BFP'},
              {'stat_typeID': 2, 'my_col': 'K', 'lahman_col': 'SO'},
              {'stat_typeID': 3, 'my_col': 'GP', 'lahman_col': 'G'}]
    conn.execute(cs.convert_lahman.insert(), lahman)
    
    # IMPORT YEARS -- initialize with 2019 season
    import_years = [{'yearID': 104, 'imported': False}]
    conn.execute(cs.import_years.insert(), import_years)
    
    # check results
    # results = conn.execute('SELECT * FROM events_dim').fetchall()
    # print(results)

    return True
