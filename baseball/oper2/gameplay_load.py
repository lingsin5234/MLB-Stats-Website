# move the gameplay staging to main gameplay table
from . import global_variables as gv
from . import db_setup as dbs
from . import stat_collector as sc
from . import logging_functions as lf
import pandas as pd
import time as t


# move gameplay staging to games and gameplay -- do this by team
def gameplay_load(yearID, year, teamID, team):

    # set timers for performance
    s1 = t.time()

    # connect to db
    conn = dbs.engine.connect()

    # get gameplay_staging
    results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=? AND home_team=?', year, team)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    df.drop(columns=['Id'], inplace=True)  # omit the Id column
    s2 = t.time()
    print("gameplay: ", s2 - s1)
    
    #  -----  LOAD TO GAMES DIM TABLE  -----  #
    
    # extract games, win/lose/save
    game_ids = df[['game_id', 'win_id', 'loss_id', 'save_id']].drop_duplicates()
    game_ids['yearID'] = yearID
    
    # merge player ids for win, loss and save separately
    results = conn.execute('SELECT Id, retroID FROM players_dim')
    players = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # wins
    game_ids = game_ids.merge(players, left_on='win_id', right_on='retroID')
    game_ids.drop(['win_id', 'retroID'], axis=1, inplace=True)
    game_ids.rename(columns={'Id': 'winID'}, inplace=True)
    
    # losses
    game_ids = game_ids.merge(players, left_on='loss_id', right_on='retroID')
    game_ids.drop(['loss_id', 'retroID'], axis=1, inplace=True)
    game_ids.rename(columns={'Id': 'lossID'}, inplace=True)
    
    # saves -- allow blanks and change to None
    game_ids = game_ids.merge(players, left_on='save_id', right_on='retroID', how='left')
    game_ids.drop(['save_id', 'retroID'], axis=1, inplace=True)
    game_ids.rename(columns={'Id': 'saveID'}, inplace=True)
    game_ids.where(game_ids.notnull(), None)  # df.where --> if not null, else None
    
    # load to db
    game_ids.to_sql('games_dim', conn, if_exists='append', index=False)
    
    # log completion
    lf.log_process('games_load', yearID, teamID, s1)
    #  -------------------------------------  #
    
    #  -----  LOAD TO GAMEPLAY TABLE  ----- #

    # gameplay start time
    game_time = t.time()
    
    # need to convert foreign keys: gameID, home_teamID, away_teamID, yearID, playerID, pitcherID, EVENT_CD,
    #     playerTeamID, fieldingTeamID
    df['data_year'] = yearID
    df['home_team'] = teamID
    df.drop(['win_id', 'loss_id', 'save_id'], axis=1, inplace=True)  # W/L/SV not needed in gameplay
    df.rename(columns={'data_year': 'yearID', 'home_team': 'home_teamID'}, inplace=True)
    s3 = t.time()
    print("yearID & home_teamID: ", s3 - s2)
    
    # game ID
    results = conn.execute('SELECT Id, game_id FROM games_dim WHERE yearID=?', yearID)
    game_ids = pd.DataFrame(results.fetchall(), columns=results.keys())
    df = df.merge(game_ids, on="game_id")
    df.drop(columns=['game_id'], inplace=True)
    df.rename(columns={'Id': 'gameID'}, inplace=True)
    s4 = t.time()
    print("gameID: ", s4 - s3)
    
    # player ID
    df = df.merge(players, left_on='playerID', right_on='retroID')
    df.drop(columns=['playerID', 'retroID'], inplace=True)
    df.rename(columns={'Id': 'playerID'}, inplace=True)
    s5 = t.time()
    print("playerID: ", s5 - s4)
    
    # pitcher ID
    df = df.merge(players, left_on='pitcherID', right_on='retroID')
    df.drop(columns=['pitcherID', 'retroID'], inplace=True)
    df.rename(columns={'Id': 'pitcherID'}, inplace=True)
    players = None  # clear memory?
    s6 = t.time()
    print("pitcherID: ", s6 - s5)
    
    # EVENT CD
    results = conn.execute('SELECT Id, event_cd FROM events_dim')
    events = pd.DataFrame(results.fetchall(), columns=results.keys())
    df.loc[:, 'EVENT_CD'] = df.loc[:, 'EVENT_CD'].astype(int)  # convert from string to int
    df = df.merge(events, left_on="EVENT_CD", right_on="event_cd")
    df.drop(columns={'EVENT_CD', 'event_cd'}, inplace=True)
    df.rename(columns={'Id': 'eventID'}, inplace=True)
    s7 = t.time()
    print("eventID: ", s7 - s6)
    
    # TEAM IDS -- for THIS YEAR ONLY
    results = conn.execute('SELECT Id, team_abb FROM teams_dim WHERE yearID=?', yearID)
    teams = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # away_team
    df = df.merge(teams, left_on="away_team", right_on="team_abb")
    df.drop(columns={'away_team', 'team_abb'}, inplace=True)
    df.rename(columns={'Id': 'away_teamID'}, inplace=True)
    s8 = t.time()
    print("away_teamID: ", s8 - s7)
    
    # playerTeamID
    df = df.merge(teams, left_on="playerTeamID", right_on="team_abb")
    df.drop(columns={'playerTeamID', 'team_abb'}, inplace=True)
    df.rename(columns={'Id': 'playerTeamID'}, inplace=True)
    s9 = t.time()
    print("playerTeamID: ", s9 - s8)
    
    # fieldingTeamID
    df = df.merge(teams, left_on="fieldingTeamID", right_on="team_abb")
    df.drop(columns={'fieldingTeamID', 'team_abb'}, inplace=True)
    df.rename(columns={'Id': 'fieldingTeamID'}, inplace=True)
    s10 = t.time()
    print("fieldingTeamID: ", s10 - s9)

    # convert integers (excl. data_year, EVENT_CD) & bools & blank to None
    int_cols = gv.gameplay_int.copy()
    int_cols.remove('data_year')
    int_cols.remove('EVENT_CD')
    df = sc.stat_col_converter(df, int_cols, gv.gameplay_bool, gv.gameplay_nullable)
    s11 = t.time()
    print("Column Conversion: ", s11 - s10)
    
    # load to database
    df.to_sql('gameplay', conn, if_exists='append', index=False)
    s12 = t.time()
    print("Loaded to DB: ", s12 - s11)

    # log completion
    lf.log_process('gameplay_load', yearID, teamID, game_time)

    return True
