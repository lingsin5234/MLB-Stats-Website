# libraries
from . import global_variables as gv
from functools import reduce
import pandas as pd
import time as t
import re


# stats collector, collect for one season (year)
def stat_collector2(season, stat_type):

    s_time = t.time()

    # convert columns to int/bool
    season = stat_col_converter(season, gv.gameplay_int, gv.gameplay_bool, gv.gameplay_nullable)
    col_time = t.time()
    print("Column Conversion: ", col_time - s_time)

    # ##### BATTING STATS ###### #
    if stat_type == 'batting':
        stat_output = batting_stats(season)
        bat_time = t.time()
        print("Batting: ", bat_time - col_time)
    # ########################## #

    # ##### PITCHING STATS ##### #
    elif stat_type == 'pitching':
        stat_output = pitching_stats(season)
        pitch_time = t.time()
        print("Pitching: ", pitch_time - col_time)
    # ########################## #

    # ##### FIELDING STATS ##### #
    else:
        stat_output = fielding_stats(season)
        field_time = t.time()
        print("Fielding: ", field_time - col_time)
    # ########################## #
    
    return stat_output


# lineup group by aggregates
def lineup_aggregate(df, init_stat, fin_stat, agg_type, team_id):
    
    # check for blank df
    if len(df) == 0:
        return df
    
    # combine playerID and playerTeamID / fieldingTeamID to make unstacking easier
    agg_cols = [c for c in df.columns if c not in [init_stat, team_id]]
    for col in agg_cols:
        df[col] = df[col] + ';' + df[team_id]
        
    # remove playerTeamID / fieldingTeamID so that it is not part of unstacking
    df.drop(columns=team_id, inplace=True)
    
    # using the game id as index, unstack, drop dupes, group by and agg
    unstack_df = df.set_index(init_stat).unstack().reset_index().rename(columns={init_stat: fin_stat, 0: 'playerID'})
    
    # agg_type - count or sum
    if agg_type == 'count':
        agg_df = unstack_df[[fin_stat, 'playerID']].drop_duplicates().groupby(
            ['playerID'], as_index=False)[fin_stat].count()
    else:
        # do not drop duplicates
        agg_df = unstack_df[[fin_stat, 'playerID']].groupby(
            ['playerID'], as_index=False)[fin_stat].sum()
        
    # split out the playerID (account for DH = None) and playerTeamID / fieldingTeamID
    agg_df[team_id] = agg_df['playerID'].apply(lambda x: x.split(';')[1])
    agg_df['playerID'] = agg_df['playerID'].apply(lambda x: None if x.split(';')[0] == '' else x.split(';')[0])

    return agg_df


# stat converter -- converting the specific columns to integer/bool
def stat_col_converter(df, int_cols, bool_cols, nullable_cols):
    
    # fix the integers
    df.loc[:, int_cols] = df.loc[:, int_cols].astype(int)
    
    # bools were change to 0 (False) and 1 (True) when transferred to staging
    for col in bool_cols:
        df.loc[df[col] == '0', col] = False
        df.loc[df[col] == '1', col] = True
    df.loc[:, bool_cols] = df.loc[:, bool_cols].astype(bool)
    
    # change blank str to None
    for col in nullable_cols:
        df.loc[df[col] == '', col] = None
    
    return df


# generate batting stats
def batting_stats(season):
    
    # come back to GP/GS/PH/PR later (also catcher interference)
    PA_ = season[season['EVENT_CD'].isin(gv.PA_events)]
    AB_ = season[season['EVENT_CD'].isin(gv.AB_events)]
    AB_ = AB_[~(AB_['SH_flag']) & ~(AB_['SF_flag'])]
    H_ = season[season['EVENT_CD'].isin([20, 21, 22, 23])]
    D_ = season[season['EVENT_CD'] == 21]
    T_ = season[season['EVENT_CD'] == 22]
    HR_ = season[season['EVENT_CD'] == 23]
    RBI_ = season[season['RBI_awarded'] > 0]
    R_ = season[season['runs_scored'] > 0]
    BB_ = season[season['EVENT_CD'].isin([14, 15])]
    IW_ = season[season['EVENT_CD'] == 15]
    K_ = season[season['EVENT_CD'] == 3]
    SB_ = season[((season['SB2_runner'].notnull()) | (season['SB3_runner'].notnull()) |
                  (season['SBH_runner'].notnull()))]
    CS_ = season[((season['CS2_runner'].notnull()) | (season['CS3_runner'].notnull()) |
                  (season['CSH_runner'].notnull()))]
    # official at-bats: at least 1 out and at least 1 runner
    LOB_ = AB_[(AB_['recorded_outs'] > 0 & ((AB_['before_1B'].notnull()) | (AB_['before_2B'].notnull()) |
                                            (AB_['before_3B'].notnull())))]
    RLSP_ = LOB_[LOB_['before_2B'].notnull() | LOB_['before_3B'].notnull()]
    GDP_ = season[season['GDP_flag']]
    HBP_ = season[season['EVENT_CD'] == 16]
    SH_ = PA_[PA_['SH_flag']]
    SF_ = PA_[PA_['SF_flag']]
    CI_ = season[season['EVENT_CD'] == 17]
    
    # aggregate, then merge + reduce
    H_ = H_.groupby(['playerID', 'playerTeamID'],
                    as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'H'})
    D_ = D_.groupby(['playerID', 'playerTeamID'],
                    as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': '2B'})
    T_ = T_.groupby(['playerID', 'playerTeamID'],
                    as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': '3B'})
    HR_ = HR_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'HR'})
    RBI_ = RBI_.groupby(['playerID', 'playerTeamID'],
                        as_index=False)['RBI_awarded'].sum().rename(columns={'RBI_awarded': 'RBI'})
    BB_ = BB_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'BB'})
    IW_ = IW_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'IBB'})
    GDP_ = GDP_.groupby(['playerID', 'playerTeamID'],
                        as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'GDP'})
    HBP_ = HBP_.groupby(['playerID', 'playerTeamID'],
                        as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'HBP'})
    SH_ = SH_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'SH'})
    SF_ = SF_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'SF'})
    CI_ = CI_.groupby(['playerID', 'playerTeamID'],
                      as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'CI'})
    
    # Handle the 2-strike PH -- PA, AB, K
    KN_ = K_[K_['injured_strikeout'].isnull()].groupby(
        ['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'K'})
    KB_ = K_[K_['injured_strikeout'].notnull()].groupby(
        ['injured_strikeout', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'injured_strikeout': 'playerID', 'EVENT_CD': 'K'})
    K_ = KN_.append(KB_, ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['K'].sum()
    
    # Plate Appearance -- injured strikeout PH is same team as batter that subbed out
    PAN_ = PA_[PA_['injured_strikeout'].isnull()].groupby(
        ['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'PA'})
    PAB_ = PA_[PA_['injured_strikeout'].notnull()].groupby(
        ['injured_strikeout', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'injured_strikeout': 'playerID', 'EVENT_CD': 'PA'})
    PA_ = PAN_.append(PAB_, ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['PA'].sum()
    # lahman doesn't include Catcher Interference for PA
    PAC = PA_.merge(CI_, on=["playerID", "playerTeamID"], how="outer")
    PAC.loc[PAC['CI'].isna(), 'CI'] = 0
    PAC['PA'] = PAC['PA'] - PAC['CI']
    PAC = PAC.loc[:, ['playerID', 'playerTeamID', 'PA']]
    
    ABN_ = AB_[AB_['injured_strikeout'].isnull()].groupby(
        ['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'AB'})
    ABB_ = AB_[AB_['injured_strikeout'].notnull()].groupby(
        ['injured_strikeout', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'injured_strikeout': 'playerID', 'EVENT_CD': 'AB'})
    AB_ = ABN_.append(ABB_, ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['AB'].sum()
    
    # Stolen Base Aggregate -- base stealer same team as batter
    SB2_ = SB_[SB_['SB2_runner'].notnull()].groupby(
        ['SB2_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'SB2_runner': 'playerID', 'EVENT_CD': 'SB'})
    SB3_ = SB_[SB_['SB3_runner'].notnull()].groupby(
        ['SB3_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'SB3_runner': 'playerID', 'EVENT_CD': 'SB'})
    SBH_ = SB_[SB_['SBH_runner'].notnull()].groupby(
        ['SBH_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'SBH_runner': 'playerID', 'EVENT_CD': 'SB'})
    SB_ = SB2_.append([SB3_, SBH_], ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['SB'].sum()
    
    # Caught Stealing Aggregate -- base stealer same team as batter
    CS2_ = CS_[CS_['CS2_runner'].notnull()].groupby(
        ['CS2_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'CS2_runner': 'playerID', 'EVENT_CD': 'CS'})
    CS3_ = CS_[CS_['CS3_runner'].notnull()].groupby(
        ['CS3_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'CS3_runner': 'playerID', 'EVENT_CD': 'CS'})
    CSH_ = CS_[CS_['CSH_runner'].notnull()].groupby(
        ['CSH_runner', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'CSH_runner': 'playerID', 'EVENT_CD': 'CS'})
    CS_ = CS2_.append([CS3_, CSH_], ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['CS'].sum()
    
    # Runs Scored Aggregate
    RB_ = R_[R_['BT_dest'] >= 4].groupby(['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'EVENT_CD': 'R'})
    R1_ = R_[R_['R1_dest'] >= 4].groupby(['before_1B', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'before_1B': 'playerID', 'EVENT_CD': 'R'})
    R2_ = R_[R_['R2_dest'] >= 4].groupby(['before_2B', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'before_2B': 'playerID', 'EVENT_CD': 'R'})
    R3_ = R_[R_['R3_dest'] >= 4].groupby(['before_3B', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'before_3B': 'playerID', 'EVENT_CD': 'R'})
    R_ = RB_.append([R1_, R2_, R3_], ignore_index=True).groupby(['playerID', 'playerTeamID'], as_index=False)['R'].sum()
    
    # GAMES STARTED and GAMES PLAYED
    TOP_START = season[(season['half_innings'] == '1_0') & season['new_half']]
    BOT_START = season[(season['half_innings'] == '1_1') & season['new_half']]

    # filter out any STARTS that are 'NP', find the next line, which should be substitution
    NP = TOP_START[TOP_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
    
        # drop the rows with those Ids
        TOP_START = TOP_START[TOP_START['play'] != 'NP']
    
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        TOP_START = pd.concat([TOP_START, add_df])

    NP = BOT_START[BOT_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
    
        # drop the rows with those Ids
        BOT_START = BOT_START[BOT_START['play'] != 'NP']
    
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        BOT_START = pd.concat([BOT_START, add_df])
    
    # away batting
    away_list = ['game_id', 'away_team']
    away_list.extend(gv.away_batting)
    AWAY_BAT_START = TOP_START[away_list].drop_duplicates().rename(columns={'away_team': 'playerTeamID'})
    AWAY_BAT_START = lineup_aggregate(AWAY_BAT_START, 'game_id', 'GS', 'count', 'playerTeamID')
    
    # home batting
    home_list = ['game_id', 'home_team']
    home_list.extend(gv.home_batting)
    HOME_BAT_START = BOT_START[home_list].drop_duplicates().rename(columns={'home_team': 'playerTeamID'})
    HOME_BAT_START = lineup_aggregate(HOME_BAT_START, 'game_id', 'GS', 'count', 'playerTeamID')
    
    # ALL STARTS
    GS_ = AWAY_BAT_START.append([HOME_BAT_START]).groupby(['playerID', 'playerTeamID'], as_index=False)['GS'].sum()
    
    # GAMES PLAYED --- SPLIT HOME AND AWAY
    AWAY_GP = season[away_list].drop_duplicates().rename(columns={'away_team': 'playerTeamID'})
    AWAY_GP = lineup_aggregate(AWAY_GP, 'game_id', 'GP', 'count', 'playerTeamID')
    
    HOME_GP = season[home_list].drop_duplicates().rename(columns={'home_team': 'playerTeamID'})
    HOME_GP = lineup_aggregate(HOME_GP, 'game_id', 'GP', 'count', 'playerTeamID')
    
    # ALL GAMES PLAYED
    GP_ = AWAY_GP.append([HOME_GP]).groupby(['playerID', 'playerTeamID'], as_index=False)['GP'].sum()
    
    # PINCH-HITTING and PINCH-RUNNING
    PH_ = season[(season['EVENT_CD'] == '25') & (season['fielding'] == '11')]
    PH_ = PH_.groupby(
        ['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'PH'})
    PR_ = season[(season['EVENT_CD'] == '25') & (season['fielding'] == '12')]
    PR_ = PR_.groupby(
        ['playerID', 'playerTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'PR'})
    
    # LOB -- on base minus those that scored
    LOB_['LOB'] = LOB_['before_1B'].apply(lambda x: 1 if str(x) != 'None' else 0) + \
                  LOB_['before_2B'].apply(lambda x: 1 if str(x) != 'None' else 0) + \
                  LOB_['before_3B'].apply(lambda x: 1 if str(x) != 'None' else 0) - \
                  (LOB_['R1_dest'].apply(lambda x: 1 if x in [4, 5, 6] else 0) +
                   LOB_['R2_dest'].apply(lambda x: 1 if x in [4, 5, 6] else 0) +
                   LOB_['R3_dest'].apply(lambda x: 1 if x in [4, 5, 6] else 0))
    LOB_ = LOB_.groupby(['playerID', 'playerTeamID'], as_index=False)['LOB'].sum()
    
    # RLSP -- similar but ignore 1st base
    RLSP_['RLSP'] = RLSP_['before_2B'].apply(lambda x: 1 if str(x) != 'None' else 0) + \
                    RLSP_['before_3B'].apply(lambda x: 1 if str(x) != 'None' else 0) - \
                    (RLSP_['R2_dest'].apply(lambda x: 1 if x in [4, 5, 6] else 0) +
                     RLSP_['R3_dest'].apply(lambda x: 1 if x in [4, 5, 6] else 0))
    RLSP_ = RLSP_.groupby(['playerID', 'playerTeamID'], as_index=False)['RLSP'].sum()
    
    # combine all batting (lahman: no GS, CI, PH or PR); excluding LOB_, RLSP_ for now
    df_list = [GP_, GS_, PAC, AB_, H_, D_, T_, HR_, RBI_, R_, BB_, IW_, HBP_, K_, GDP_,
               SH_, SF_, SB_, CS_, CI_, PH_, PR_, LOB_, RLSP_]
    batting = reduce(
        lambda left, right: pd.merge(left, right, on=['playerID', 'playerTeamID'], how='outer'), df_list).fillna(0)
    
    # make numeric columns integers
    int_col = [c for c in batting.columns if c not in ["playerID", "playerTeamID"]]
    batting.loc[:, int_col] = batting.loc[:, int_col].astype(int)
    batting.rename(columns={'playerTeamID': 'team_id'}, inplace=True)
    # print(batting)
    # print(batting[batting['playerID']=='gricr001'])
    
    return batting


# pitching stats
def pitching_stats(season):
    
    # stats from batting
    H_ = season[season['EVENT_CD'].isin([20, 21, 22, 23])]
    HR_ = season[season['EVENT_CD'] == 23]
    R_ = season[season['runs_scored'] > 0]
    BB_ = season[season['EVENT_CD'].isin([14, 15])]
    IW_ = season[season['EVENT_CD'] == 15]
    K_ = season[season['EVENT_CD'] == 3]
    HBP_ = season[season['EVENT_CD'] == 16]
    
    # come back to GP/GS later (also catcher interference)
    IP_ = season[season['recorded_outs'] > 0]
    BF_ = season[season['EVENT_CD'].isin(gv.PA_events)]
    # R_ same dataset for runs scored for pitchers
    ER_ = season[((season['BT_dest'] >= 4) | (season['R1_dest'] >= 4) |
                  (season['R2_dest'] >= 4) | (season['R3_dest'] >= 4))]
    # H_, HR_, K, Walks, HBP are the same data set
    POA_ = season[season['pitches'].str.contains("1|2|3", na=False)]
    PO_ = season[season['EVENT_CD'] == 8]
    WP_ = season[(season['EVENT_CD'] == 9) | (season['WP_flag'] == True)]
    PB_ = season[(season['EVENT_CD'] == 10) | (season['PB_flag'] == True)]
    BK_ = season[season['EVENT_CD'] == 11]
    DI_ = season[season['EVENT_CD'] == 5]
    PT_ = season[season['pitches'] != '']  # used for strikes, foul balls and balls as well
    
    # aggregate, then merge + reduce
    IP_ = IP_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['recorded_outs'].sum().rename(
        columns={'recorded_outs': 'IPouts'})
    H_ = H_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'H'})
    HR_ = HR_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'HR'})
    K_ = K_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'K'})
    IW_ = IW_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'IBB'})
    HBP_ = HBP_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'HBP'})
    PO_ = PO_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'PO'})
    WP_ = WP_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'WP'})
    PB_ = PB_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'PB'})
    BK_ = BK_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'BK'})
    DI_ = DI_.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'DI'})
    
    # rare pitching substitution with batter-favoured count resulting in a walk by relief pitcher
    BF1_ = BF_[BF_['pinch_pitcher'].isnull()].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['playerID'].count().rename(columns={'playerID': 'BF'})
    BF2_ = BF_[BF_['pinch_pitcher'].notnull()].groupby(
        ['pinch_pitcher', 'fieldingTeamID'], as_index=False)['playerID'].count().rename(
        columns={'pinch_pitcher': 'pitcherID', 'playerID': 'BF'})
    BF_ = BF1_.append([BF2_]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['BF'].sum()
    
    BB1 = BB_[BB_['pinch_pitcher'].isnull()].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'BB'})
    BB2 = BB_[BB_['pinch_pitcher'].notnull()].groupby(
        ['pinch_pitcher', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'pinch_pitcher': 'pitcherID', 'EVENT_CD': 'BB'})
    BB_ = BB1.append([BB2]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['BB'].sum()
    
    # Runs Aggregate
    # no inherited
    RN_ = R_[R_['inh_run1'].isnull()].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['runs_scored'].sum().rename(columns={'runs_scored': 'R'})
    RI_ = R_[R_['inh_run1'].notnull()]
    
    # handle inherited
    RI1 = RI_.groupby(['inh_run1', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_run1': 'pitcherID', 'EVENT_CD': 'R'})
    RI2 = RI_[RI_['inh_run2'].notnull()].groupby(
        ['inh_run2', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_run2': 'pitcherID', 'EVENT_CD': 'R'})
    RI3 = RI_[RI_['inh_run3'].notnull()].groupby(
        ['inh_run3', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_run3': 'pitcherID', 'EVENT_CD': 'R'})
    
    # handle inherited + regular
    RIB = RI_.reset_index(drop=True)
    RIB['total_scored'] = RIB['runs_scored'] - \
                          (RIB['inh_run1'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                           RIB['inh_run2'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                           RIB['inh_run3'].apply(lambda x: 1 if str(x) != 'None' else 0))
    RIB = RIB.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['total_scored'].sum().rename(columns={'total_scored': 'R'})
    
    # combine all runs against
    R_ = RN_.append([RI1, RI2, RI3, RIB]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['R'].sum()
    
    # Earned Runs Main - Aggregate
    ERM_ = ER_[ER_['inh_earned1'].isnull()]
    ERB_ = ERM_[ERM_['BT_dest'].isin([4, 6])].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'ER'})
    ER1_ = ERM_[ERM_['R1_dest'].isin([4, 6])].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'ER'})
    ER2_ = ERM_[ERM_['R2_dest'].isin([4, 6])].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'ER'})
    ER3_ = ERM_[ERM_['R3_dest'].isin([4, 6])].groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(columns={'EVENT_CD': 'ER'})
    ERMT = ERB_.append([ER1_, ER2_, ER3_]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['ER'].sum()
    
    # Earned Runs Inherited - Aggregate
    ERI_ = ER_[ER_['inh_earned1'].notnull()]
    # the inherited earned runs
    ERI1 = ERI_.groupby(['inh_earned1', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_earned1': 'pitcherID', 'EVENT_CD': 'ER'})
    ERI2 = ERI_[ERI_['inh_earned2'].notnull()].groupby(
        ['inh_earned2', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_earned2': 'pitcherID', 'EVENT_CD': 'ER'})
    ERI3 = ERI_[ERI_['inh_earned3'].notnull()].groupby(
        ['inh_earned3', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count().rename(
        columns={'inh_earned3': 'pitcherID', 'EVENT_CD': 'ER'})
    ERIT = ERI1.append([ERI2, ERI3]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['ER'].sum()
    
    # the earned runs not inherited but also inherited also scored on same play
    ERIN = ERI_.reset_index(drop=True)
    ERIN['total_earned'] = (ERIN['BT_dest'].apply(lambda x: 1 if x in [4, 6] else 0) +
                            ERIN['R1_dest'].apply(lambda x: 1 if x in [4, 6] else 0) +
                            ERIN['R2_dest'].apply(lambda x: 1 if x in [4, 6] else 0) +
                            ERIN['R3_dest'].apply(lambda x: 1 if x in [4, 6] else 0)) - \
                           (ERIN['inh_earned1'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                            ERIN['inh_earned2'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                            ERIN['inh_earned3'].apply(lambda x: 1 if str(x) != 'None' else 0))
    ERIN = ERIN.groupby(
        ['pitcherID', 'fieldingTeamID'], as_index=False)['total_earned'].sum().rename(columns={'total_earned': 'ER'})
    
    # combine all ER against
    ER_ = ERMT.append([ERIT, ERIN]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['ER'].sum()
    
    # Pick-Offs Aggregate
    POA1_ = POA_[POA_['pitches'].str.contains("1")].reset_index(drop=True)
    POA1_['POA'] = POA1_['pitches'].str.count("1")
    POA1_ = POA1_.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['POA'].sum()
    
    POA2_ = POA_[POA_['pitches'].str.contains("2")].reset_index(drop=True)
    POA2_['POA'] = POA2_['pitches'].str.count("2")
    POA2_ = POA2_.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['POA'].sum()
    
    POA3_ = POA_[POA_['pitches'].str.contains("3")].reset_index(drop=True)
    POA3_['POA'] = POA3_['pitches'].str.count("3")
    POA3_ = POA3_.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['POA'].sum()
    
    POA_ = POA1_.append([POA2_, POA3_]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['POA'].sum()

    # GAMES STARTED and GAMES PLAYED
    TOP_START = season[(season['half_innings'] == '1_0') & season['new_half']]
    BOT_START = season[(season['half_innings'] == '1_1') & season['new_half']]
    
    # filter out any STARTS that are 'NP', find the next line, which should be substitution
    NP = TOP_START[TOP_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
        
        # drop the rows with those Ids
        TOP_START = TOP_START[TOP_START['play'] != 'NP']
        
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        TOP_START = pd.concat([TOP_START, add_df])
        
    NP = BOT_START[BOT_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
        
        # drop the rows with those Ids
        BOT_START = BOT_START[BOT_START['play'] != 'NP']
        
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        BOT_START = pd.concat([BOT_START, add_df])
    
    # away pitching -- bottom is when away is pitching
    away_list = ['game_id', 'away_P', 'away_team']
    AWAY_PITCHING = BOT_START[away_list].drop_duplicates().rename(columns={'away_team': 'fieldingTeamID'})
    AWAY_PITCH_START = lineup_aggregate(AWAY_PITCHING, 'game_id', 'GS', 'count', 'fieldingTeamID')
    
    # home batting -- top is when home is pitching
    home_list = ['game_id', 'home_P', 'home_team']
    HOME_PITCHING = TOP_START[home_list].drop_duplicates().rename(columns={'home_team': 'fieldingTeamID'})
    HOME_PITCH_START = lineup_aggregate(HOME_PITCHING, 'game_id', 'GS', 'count', 'fieldingTeamID')
    
    # ALL STARTS
    GS_ = AWAY_PITCH_START.append([HOME_PITCH_START]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['GS'].sum()
    GS_ = GS_.rename(columns={'playerID': 'pitcherID'})
    
    # GAMES PLAYED
    AWAY_PITCHING = season[(season['half'] == 1) & (season['gm_type'] == 'play') & (season['play'] != 'NP')]
    HOME_PITCHING = season[(season['half'] == 0) & (season['gm_type'] == 'play') & (season['play'] != 'NP')]
    AWAY_PITCHING = AWAY_PITCHING[away_list].drop_duplicates().rename(columns={'away_team': 'fieldingTeamID'})
    HOME_PITCHING = HOME_PITCHING[home_list].drop_duplicates().rename(columns={'home_team': 'fieldingTeamID'})
    AWAY_PITCHING = lineup_aggregate(AWAY_PITCHING, 'game_id', 'GP', 'count', 'fieldingTeamID')
    HOME_PITCHING = lineup_aggregate(HOME_PITCHING, 'game_id', 'GP', 'count', 'fieldingTeamID')
    GP_ = AWAY_PITCHING.append([HOME_PITCHING]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['GP'].sum()
    GP_ = GP_.rename(columns={'playerID': 'pitcherID'})
    
    # Wins Losses and Saves --- need to assign the team_id based on the pitcher
    W_ = season[['game_id', 'win_id', 'pitcherID', 'fieldingTeamID']].drop_duplicates()
    W_ = W_[W_['win_id'] == W_['pitcherID']].groupby(['win_id', 'fieldingTeamID'], as_index=False)['game_id'].count()
    W_.rename(columns={'win_id': 'pitcherID', 'game_id': 'W'}, inplace=True)
    
    L_ = season[['game_id', 'loss_id', 'pitcherID', 'fieldingTeamID']].drop_duplicates()
    L_ = L_[L_['loss_id'] == L_['pitcherID']].groupby(['loss_id', 'fieldingTeamID'], as_index=False)['game_id'].count()
    L_.rename(columns={'loss_id': 'pitcherID', 'game_id': 'L'}, inplace=True)
    
    SV_ = season[['game_id', 'save_id', 'pitcherID', 'fieldingTeamID']].drop_duplicates()
    SV_ = SV_[SV_['save_id'] == SV_['pitcherID']].groupby(
        ['save_id', 'fieldingTeamID'], as_index=False)['game_id'].count()
    SV_.rename(columns={'save_id': 'pitcherID', 'game_id': 'SV'}, inplace=True)
    
    # --- COMPLETE GAMES AND SHUTOUTS --- #
    
    # HOME TEAM PITCHING
    HOME_OUTS = season[season['half'] == 0]
    HOME_OUTS = HOME_OUTS[['game_id', 'pitcherID', 'fieldingTeamID', 'runs_scored']]
    HOME_OUTS = HOME_OUTS.groupby(['game_id', 'pitcherID', 'fieldingTeamID'], as_index=False)['runs_scored'].sum()
    
    # get complete games, only 1 pitcher
    GET_HOME_CG = HOME_OUTS['game_id'].value_counts()
    GET_HOME_CG = GET_HOME_CG[GET_HOME_CG == 1].index.tolist()
    if len(GET_HOME_CG) > 0:
        HOME_OUTS = HOME_OUTS[HOME_OUTS['game_id'].isin(GET_HOME_CG)]
        HOME_CG = HOME_OUTS.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['game_id'].count()
        HOME_CG.rename(columns={'game_id': 'CG'}, inplace=True)
        
        # shutout
        HOME_SHO = HOME_OUTS[HOME_OUTS['runs_scored'] == 0].copy()
        if len(HOME_SHO) > 0:
            HOME_SHO = HOME_SHO.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['game_id'].count()
            HOME_SHO.rename(columns={'game_id': 'SHO'}, inplace=True)
        else:
            HOME_SHO = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'SHO'])
    else:
        HOME_CG = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'CG'])
        HOME_SHO = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'SHO'])
        
    # AWAY TEAM PITCHING
    AWAY_OUTS = season[season['half'] == 1]
    AWAY_OUTS = AWAY_OUTS[['game_id', 'pitcherID', 'fieldingTeamID', 'runs_scored']]
    AWAY_OUTS = AWAY_OUTS.groupby(['game_id', 'pitcherID', 'fieldingTeamID'], as_index=False)['runs_scored'].sum()
    
    # get complete games, only 1 pitcher
    GET_AWAY_CG = AWAY_OUTS['game_id'].value_counts()
    GET_AWAY_CG = GET_AWAY_CG[GET_AWAY_CG==1].index.tolist()
    if len(GET_AWAY_CG) > 0:
        AWAY_OUTS = AWAY_OUTS[AWAY_OUTS['game_id'].isin(GET_AWAY_CG)]
        AWAY_CG = AWAY_OUTS.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['game_id'].count()
        AWAY_CG.rename(columns={'game_id': 'CG'}, inplace=True)
        
        # shutout
        AWAY_SHO = AWAY_OUTS[AWAY_OUTS['runs_scored'] == 0].copy()
        if len(AWAY_SHO) > 0:
            AWAY_SHO = AWAY_SHO.groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['game_id'].count()
            AWAY_SHO.rename(columns={'game_id': 'SHO'}, inplace=True)
        else:
            AWAY_SHO = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'SHO'])
    else:
        AWAY_CG = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'CG'])
        AWAY_SHO = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'SHO'])
    
    # combine away and home
    if (len(AWAY_CG) > 0) & (len(HOME_CG) > 0):
        CG_ = HOME_CG.append([AWAY_CG]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['CG'].sum()
    elif len(AWAY_CG) > 0:
        CG_ = AWAY_CG
    elif len(HOME_CG) > 0:
        CG_ = HOME_CG
    else:
        CG_ = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'CG'])
    
    if (len(AWAY_SHO) > 0) & (len(HOME_SHO) > 0):
        SHO_ = HOME_SHO.append([AWAY_SHO]).groupby(['pitcherID', 'fieldingTeamID'], as_index=False)['SHO'].sum()
    elif len(AWAY_SHO) > 0:
        SHO_ = AWAY_SHO
    elif len(HOME_SHO) > 0:
        SHO_ = HOME_SHO
    else:
        SHO_ = pd.DataFrame(columns=['pitcherID', 'fieldingTeamID', 'SHO'])

    # ----------------------------------- #
    
    # combine all pitching (no POA, PO, PB, DI or Pitches Thrown in Lahman); ignore PT_ for now
    df_list = [GS_, GP_, IP_, BF_, CG_, SHO_, W_, L_, SV_, R_, ER_,
               H_, HR_, K_, BB_, IW_, HBP_, POA_, PO_, WP_, PB_, BK_, DI_]
    pitching = reduce(
        lambda left, right: pd.merge(left, right, on=['pitcherID', 'fieldingTeamID'], how='outer'), df_list).fillna(0)
    
    # numeric columns into integer
    int_cols = [c for c in pitching.columns if c not in ['pitcherID', 'fieldingTeamID']]
    pitching.loc[:, int_cols] = pitching.loc[:, int_cols].astype(int)
    pitching = pitching.rename(columns={'pitcherID': 'playerID', 'fieldingTeamID': 'team_id'})
    # print(pitching)
    # print(pitching[pitching['CG'] == 1])
    
    return pitching


# fielding stats
def fielding_stats(season):
    
    # also used in pitching
    IP_ = season[season['recorded_outs'] > 0]
    
    # Lahman: G   GS  InnOuts   PO   A   E   DP   PB    WP    SB    CS    ZR
    # Catcher Interference?
    # IP_ is same as pitching
    A_ = season[season['assists'] != '']
    PO_ = season[season['putouts'] != '']
    ERR_ = season[season['errors'] != '']
    
    # aggregate
    A_ = A_.groupby(['assists', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count()
    A_ = pd.DataFrame(A_['assists'].str.split(';').tolist(),
                      index=[A_['EVENT_CD'], A_['fieldingTeamID']]).stack().reset_index()
    A_ = A_.rename(columns={'EVENT_CD': 'A', 0: 'playerID'}).groupby(
        ['playerID', 'fieldingTeamID'], as_index=False)['A'].sum()
    A_ = A_[A_['playerID'] != '']  # remove the blanks generated from split
    
    PO_ = PO_.groupby(['putouts', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count()
    PO_ = pd.DataFrame(PO_['putouts'].str.split(';').tolist(),
                       index=[PO_['EVENT_CD'], PO_['fieldingTeamID']]).stack().reset_index()
    PO_ = PO_.rename(columns={'EVENT_CD': 'PO', 0: 'playerID'}).groupby(
        ['playerID', 'fieldingTeamID'], as_index=False)['PO'].sum()
    PO_ = PO_[PO_['playerID'] != '']  # remove the blanks generated from split
    
    # double play balls - combine assist people and putouts people
    # prevent doubling the DP on plays (e.g. unassisted DP)
    DPAO = season[season['DP_flag']].copy()
    DPAO.loc[:, 'combined'] = DPAO.loc[:, 'putouts'].astype(str) + DPAO.loc[:, 'assists'].astype(str)
    DPAO.loc[:, 'combined'] = DPAO.loc[:, 'combined'].apply(
        lambda x: re.sub(r"\'", '', re.sub(r'{(.*)}', '\\1', str(set(str(x).split(';'))))))

    # split to get by players individually
    DPAO = pd.DataFrame(DPAO['combined'].str.split(', ').tolist(),
                        index=[DPAO['DP_flag'], DPAO['fieldingTeamID']]).stack().reset_index()
    DPAO = DPAO.rename(columns={'DP_flag': 'DP', 0: 'playerID'}).groupby(
        ['playerID', 'fieldingTeamID'], as_index=False)['DP'].count()
    DPAO = DPAO[DPAO['playerID'] != '']
    DPAO = DPAO[DPAO['playerID'] != 'nan']
    
    ERR_ = ERR_.groupby(['errors', 'fieldingTeamID'], as_index=False)['EVENT_CD'].count()
    ERR_ = pd.DataFrame(ERR_['errors'].str.split(';').tolist(),
                        index=[ERR_['EVENT_CD'], ERR_['fieldingTeamID']]).stack().reset_index()
    ERR_ = ERR_.rename(columns={'EVENT_CD': 'E', 0: 'playerID'}).groupby(
        ['playerID', 'fieldingTeamID'], as_index=False)['E'].sum()
    ERR_ = ERR_[ERR_['playerID'] != '']  # remove the blanks generated from split

    # GAMES STARTED and GAMES PLAYED
    TOP_START = season[(season['half_innings'] == '1_0') & season['new_half']]
    BOT_START = season[(season['half_innings'] == '1_1') & season['new_half']]

    # filter out any STARTS that are 'NP', find the next line, which should be substitution
    NP = TOP_START[TOP_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
    
        # drop the rows with those Ids
        TOP_START = TOP_START[TOP_START['play'] != 'NP']
    
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        TOP_START = pd.concat([TOP_START, add_df])

    NP = BOT_START[BOT_START['play'] == 'NP']
    if len(NP) > 0:
        add_id = [int(Id) + 1 for Id in NP['Id'].tolist()]
    
        # drop the rows with those Ids
        BOT_START = BOT_START[BOT_START['play'] != 'NP']
    
        # add the rows with those Ids
        add_df = season[season['Id'].isin(add_id)]
        BOT_START = pd.concat([BOT_START, add_df])
    
    # Games Started
    # need to exclude DH as a "fielder"
    # away fielding -- bottom is when away on defense
    away_list = ['game_id', 'away_team']
    away_list.extend(gv.away_fielding)
    away_list = [i for i in away_list if i != 'away_DH']
    AWAY_FIELD_START = BOT_START[away_list].drop_duplicates().rename(columns={'away_team': 'fieldingTeamID'})
    AWAY_FIELD_START = lineup_aggregate(AWAY_FIELD_START, 'game_id', 'GS', 'count', 'fieldingTeamID')
    
    # home fielding -- top is when home on defense
    home_list = ['game_id', 'home_team']
    home_list.extend(gv.home_fielding)
    home_list = [i for i in home_list if i != 'home_DH']
    HOME_FIELD_START = TOP_START[home_list].drop_duplicates().rename(columns={'home_team': 'fieldingTeamID'})
    HOME_FIELD_START = lineup_aggregate(HOME_FIELD_START, 'game_id', 'GS', 'count', 'fieldingTeamID')
    
    # combine, aggregate
    GS_ = AWAY_FIELD_START.append([HOME_FIELD_START]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['GS'].sum()
    
    # Games Played
    AWAY_FIELDING = season[(season['half'] == 1) & (season['gm_type'] == 'play') & (season['play'] != 'NP')]
    HOME_FIELDING = season[(season['half'] == 0) & (season['gm_type'] == 'play') & (season['play'] != 'NP')]
    AWAY_FIELDING = AWAY_FIELDING[away_list].drop_duplicates().rename(columns={'away_team': 'fieldingTeamID'})
    HOME_FIELDING = HOME_FIELDING[home_list].drop_duplicates().rename(columns={'home_team': 'fieldingTeamID'})
    AWAY_FIELDING = lineup_aggregate(AWAY_FIELDING, 'game_id', 'GP', 'count', 'fieldingTeamID')
    HOME_FIELDING = lineup_aggregate(HOME_FIELDING, 'game_id', 'GP', 'count', 'fieldingTeamID')
    GP_ = AWAY_FIELDING.append([HOME_FIELDING]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['GP'].sum()
    
    # Innings Pitched
    IP_BOT = IP_[(IP_['half'] == 1) & (IP_['recorded_outs'] > 0)]  # away on defense
    IP_TOP = IP_[(IP_['half'] == 0) & (IP_['recorded_outs'] > 0)]  # home on defense
    # remove game_id, add recorded_outs to capture all out events; game_id doesn't matter just want innings
    away_list.append('recorded_outs')
    away_list.remove('game_id')
    home_list.append('recorded_outs')
    home_list.remove('game_id')
    AWAY_INNINGS = IP_BOT[away_list].rename(columns={'away_team': 'fieldingTeamID'})
    HOME_INNINGS = IP_TOP[home_list].rename(columns={'home_team': 'fieldingTeamID'})
    AWAY_INNINGS = lineup_aggregate(AWAY_INNINGS, 'recorded_outs', 'InnOuts', 'sum', 'fieldingTeamID')
    HOME_INNINGS = lineup_aggregate(HOME_INNINGS, 'recorded_outs', 'InnOuts', 'sum', 'fieldingTeamID')
    IP_ = AWAY_INNINGS.append([HOME_INNINGS]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['InnOuts'].sum()
    
    # SB, CS AGAINST
    SB_ = season[((season['SB2_runner'].notnull()) | (season['SB3_runner'].notnull()) |
                  (season['SBH_runner'].notnull()))]
    CS_ = season[((season['CS2_runner'].notnull()) | (season['CS3_runner'].notnull()) |
                  (season['CSH_runner'].notnull()))]

    # split home/away to determine who is on defense
    SB_0 = SB_[SB_['team_id'] == '0'].copy()  # away steal; home on defense
    SB_0['total'] = (SB_0['SB2_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     SB_0['SB3_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     SB_0['SBH_runner'].apply(lambda x: 1 if str(x) != 'None' else 0))
    SB_0 = SB_0[['total', 'home_P', 'home_C', 'fieldingTeamID']]
    SB_0 = lineup_aggregate(SB_0, 'total', 'SB', 'sum', 'fieldingTeamID')
    
    CS_0 = CS_[CS_['team_id'] == '0'].copy()
    CS_0['total'] = (CS_0['CS2_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     CS_0['CS3_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     CS_0['CSH_runner'].apply(lambda x: 1 if str(x) != 'None' else 0))
    CS_0 = CS_0[['total', 'home_P', 'home_C', 'fieldingTeamID']]
    CS_0 = lineup_aggregate(CS_0, 'total', 'CS', 'sum', 'fieldingTeamID')
    
    SB_1 = SB_[SB_['team_id'] == '1'].copy()
    SB_1['total'] = (SB_1['SB2_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     SB_1['SB3_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     SB_1['SBH_runner'].apply(lambda x: 1 if str(x) != 'None' else 0))
    SB_1 = SB_1[['total', 'away_P', 'away_C', 'fieldingTeamID']]
    SB_1 = lineup_aggregate(SB_1, 'total', 'SB', 'sum', 'fieldingTeamID')
    
    CS_1 = CS_[CS_['team_id'] == '1'].copy()
    CS_1['total'] = (CS_1['CS2_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     CS_1['CS3_runner'].apply(lambda x: 1 if str(x) != 'None' else 0) +
                     CS_1['CSH_runner'].apply(lambda x: 1 if str(x) != 'None' else 0))
    CS_1 = CS_1[['total', 'away_P', 'away_C', 'fieldingTeamID']]
    CS_1 = lineup_aggregate(CS_1, 'total', 'CS', 'sum', 'fieldingTeamID')
    
    # combine SBs and CSs each
    SB_ = SB_0.append([SB_1]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['SB'].sum()
    CS_ = CS_0.append([CS_1]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['CS'].sum()
    
    # handling PB and WP the same way
    WP_ = season[(season['EVENT_CD'] == 9) | (season['WP_flag'])]
    PB_ = season[(season['EVENT_CD'] == 10) | (season['PB_flag'])]
    
    # split home and away
    WP_0 = WP_[WP_['team_id'] == '0'].copy()  # away batting, home is defense
    WP_0 = lineup_aggregate(
        WP_0[['EVENT_CD', 'home_P', 'home_C', 'fieldingTeamID']].copy(), 'EVENT_CD', 'WP', 'count', 'fieldingTeamID')
    
    PB_0 = PB_[PB_['team_id'] == '0'].copy()
    PB_0 = lineup_aggregate(
        PB_0[['EVENT_CD', 'home_P', 'home_C', 'fieldingTeamID']].copy(), 'EVENT_CD', 'PB', 'count', 'fieldingTeamID')
    
    WP_1 = WP_[WP_['team_id'] == '1']
    WP_1 = lineup_aggregate(
        WP_1[['EVENT_CD', 'away_P', 'away_C', 'fieldingTeamID']].copy(), 'EVENT_CD', 'WP', 'count', 'fieldingTeamID')
    
    PB_1 = PB_[PB_['team_id'] == '1'].copy()
    PB_1 = lineup_aggregate(
        PB_1[['EVENT_CD', 'away_P', 'away_C', 'fieldingTeamID']].copy(), 'EVENT_CD', 'PB', 'count', 'fieldingTeamID')
    
    # combine WPs and PBs each
    WP_ = WP_0.append([WP_1]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['WP'].sum()
    PB_ = PB_0.append([PB_1]).groupby(['playerID', 'fieldingTeamID'], as_index=False)['PB'].sum()

    # combine all fielding stats (ignore TP_)
    df_list = [GS_, GP_, IP_, A_, PO_, DPAO, ERR_, SB_, CS_, WP_, PB_]
    fielding = reduce(lambda left, right: pd.merge(
        left, right, on=['playerID', 'fieldingTeamID'], how='outer'), df_list).fillna(0)
    
    # convert numeric columns to int
    int_cols = [c for c in fielding.columns if c not in ['playerID', 'fieldingTeamID']]
    fielding.loc[:, int_cols] = fielding.loc[:, int_cols].astype(int)
    
    # change fieldingTeamID to team_id
    fielding.rename(columns={'fieldingTeamID': 'team_id'}, inplace=True)
    # print(fielding)
    
    return fielding


# game start tracker
def game_tracker(all_starts, data_year):

    # convert to dictionary
    games_dict = {}
    for g in range(len(all_starts)):
        # get team and lineup info
        game_id = all_starts[g][0].split(',')[1].split('\n')[0]
        vis_team = all_starts[g][2].split(',')[2].split('\n')[0]
        home_team = all_starts[g][3].split(',')[2].split('\n')[0]
        lineups = all_starts[g][-2]  # 2nd last item is all starting lineups

        # push lineup into dictionary
        for starter in lineups:

            ss = starter.split('\n')[0].split(',')
            if ss[3] == '0':
                team_nm = vis_team
            else:
                team_nm = home_team
            lineup_dict = {'game_id': game_id,
                           'data_year': data_year,
                           'player_id': ss[1],
                           'player_nm': ss[2].replace('"', ''),
                           'team_id': ss[3],
                           'team_name': team_nm,
                           'bat_lineup': ss[4],
                           'fielding': ss[5]}
            games_dict[gv.gr_idx] = lineup_dict
            gv.gr_idx += 1

    return games_dict
