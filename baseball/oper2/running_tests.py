# running tests for the new oper2
from . import import_retrosheet as ir
from . import extract_teams_players as etp
from . import process_imports as pi
from . import stat_collector as sc
from . import lahman_test as lt
from . import global_variables as gv
from . import date_time as dt
from . import db_init as dbi
from . import db_setup as dbs
from . import roster_load as rst
from . import logging_functions as lf
from . import gameplay_load as gs
from . import common_functions as cf
from . import stats_load as ss
from . import class_structure as cs
# from . import postgres_connect as pgs
import pandas as pd
import os
import re
import time as t


# test run - run import
def run_testing(yr):
    # '''
    # DEBUG csv
    df = pd.DataFrame(columns=gv.gameplay_cols)
    df.to_csv('DEBUG.csv', header=True, index=False)

    # year import
    print("Import Completed: ", ir.import_data(yr))  # --- JOB 1 --- #
    # create an OUTPUT file for later use in processing
    df = pd.DataFrame(columns=gv.gameplay_cols)
    df.to_csv('OUTPUT.csv', mode='w', index=False, header=True)
    
    # run extractions and staging rosters
    print("Team Extract Completed: ", etp.extract_teams(yr))  # --- JOB 2 --- #
    print("Roster Extract Completed: ", etp.extract_players(yr))  # --- JOB 3 --- #
    print("Staging Rosters Completed: ", rst.roster_staging(yr))  # --- JOB 4 --- #
    # '''
    # teams processing --- gameplay_process
    files = os.listdir('baseball/import/' + str(yr))
    files = [f[4:7] for f in files if f.find('ROS') == -1 and f.find('TEAM') == -1]
    for f in files:
        pi.process_data_single_team(yr, f)  # --- JOB 5 x num of teams --- #
    
    # get year ID
    yearID = cf.get_year(yr)

    # RUN STAT COLLECTOR
    conn = dbs.engine.connect()
    # '''
    # produce tables to run against test cases by team --- stat_collection
    for tm in files:
        
        # get teamID
        teamID = cf.get_team(yr, tm)
        
        # get gameplay
        results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=? AND home_team=?', yr, tm)
        gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
        
        # run stat collector
        stat_output = sc.stat_collector2(gameplay)  # --- JOB 6 (part a) x num of teams --- #

        # set up stats staging and tests
        ss.stats_staging(stat_output)  # --- JOB 6 (part b) x num of teams --- #
    
        # log completion
        lf.log_process('stat_collection', yearID, None, s_time)

        # add test case logs for each stat_type
        lf.log_test_case(yearID, teamID, 'batting')
        lf.log_test_case(yearID, teamID, 'pitching')
        lf.log_test_case(yearID, teamID, 'fielding')
    gameplay = None  # clear memory just in case
    
    # run tests --- batting_test
    for tm in files:
        bat_time = t.time()
        
        # get teamID
        teamID = cf.get_team(yr, tm)
        
        # get test case
        testID = cf.get_test_case(yr, tm, 'batting')

        # run test case
        lt.test_lahman2(yr, tm, 'batting', testID)  # --- JOB 7 x num of teams --- #
    
    # run tests --- pitching_test
    for tm in files:
        pitch_time = t.time()

        # get teamID
        teamID = cf.get_team(yr, tm)

        # get test case
        testID = cf.get_test_case(yr, tm, 'pitching')

        # run test case
        lt.test_lahman2(yr, tm, 'pitching', testID)  # --- JOB 8 x num of teams --- #
        
    # run tests --- fielding_test
    for tm in files:
        field_time = t.time()

        # get teamID
        teamID = cf.get_team(yr, tm)

        # get test case
        testID = cf.get_test_case(yr, tm, 'fielding')

        # run test case
        lt.test_lahman2(yr, tm, 'fielding', testID)  # --- JOB 9 x num of teams --- #
    # '''
    # run gameplay staging
    for tm in files:
        
        # get teamID
        teamID = cf.get_team(yr, tm)
        
        # run gameplay staging
        gs.gameplay_load(yearID, yr, teamID, tm)
    
    # run batting stats staging
    t1 = t.time()
    ss.stats_load(yearID, 'batting')
    lf.log_process('batting_load', yearID, None, t1)

    # run pitching stats staging
    t2 = t.time()
    ss.stats_load(yearID, 'batting')
    lf.log_process('pitching_load', yearID, None, t2)

    # run fielding stats staging
    t3 = t.time()
    ss.stats_load(yearID, 'fielding')
    lf.log_process('fielding_load', yearID, None, t3)
    
    return True


# DEBUG lahman test
# lt.test_lahman2(2018, 'ANA', 'batting', 91)

# DEBUG stat_collector 2019
'''
conn = dbs.engine.connect()
# get gameplay
results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=? AND home_team=?', 2019, 'ANA')
gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
# print(gameplay)

#  -- run stat collector, stats staging and tests -- #
# batting
# stat_output = sc.stat_collector2(gameplay, 'batting')
# ss.stats_staging(stat_output, 'batting')

# pitching
stat_output = sc.stat_collector2(gameplay, 'pitching')
# ss.stats_staging(stat_output, 'pitching')

# fielding
# stat_output = sc.stat_collector2(gameplay, 'fielding')
# ss.stats_staging(stat_output, 'fielding')
# '''

# '''
# quick update for test logs to run
# dbi.db_init()
conn = dbs.engine.connect()
# query = 'SELECT pl.*, po.* FROM players_dim pl ' \
#         'LEFT JOIN positions_dim po ON pl.positionID=po.Id ' \
#         'LEFT JOIN rosters r ON pl.Id=r.playerID ' \
#         'LEFT JOIN teams_dim t ON r.teamID=t.Id ' \
#         'lEFT JOIN years_dim y ON t.yearID=y.Id ' \
#         'WHERE pl.retroID=? AND y.year=?'
# results = conn.execute('SELECT pd.name, y.year, pl.timestamp FROM process_log pl '
#                        'LEFT JOIN process_dim pd ON pl.processID=pd.Id '
#                        'LEFT JOIN years_dim y ON pl.yearID=y.Id '
#                        'WHERE year <= 1999')
# results = conn.execute(query, 'phamt001', 2017)
# results = conn.execute('SELECT DISTINCT positionID FROM players_dim')
# results = conn.execute('SELECT DISTINCT yearID, COUNT(yearID) FROM gameplay GROUP BY yearID')
# results = conn.execute('SELECT * FROM gameplay_staging')
# results = conn.execute('SELECT yearID, COUNT(*) FROM rosters GROUP BY yearID')
# results = conn.execute('SELECT * FROM pitching_staging LIMIT 20')
# results = conn.execute('SELECT DISTINCT error, COUNT(error) FROM test_errors GROUP BY error')
# results = conn.execute('SELECT yearID, COUNT(yearID) FROM batting GROUP BY yearID')
# results = conn.execute('SELECT Id, year FROM years_dim WHERE Id>85')
results = conn.execute('SELECT * FROM process_dim')
# print(results.fetchall())
# results = conn.execute('SELECT * FROM games_dim WHERE yearID=104')
# conn = pgs.engine.connect()
# results = conn.execute('SELECT * FROM stat_type_dim', False)
# results = conn.execute('SELECT error, COUNT(error) FROM test_errors GROUP BY error')
df = pd.DataFrame(results.fetchall(), columns=results.keys())
# df['TEAM'] = df['game_id'].apply(lambda x: x[:3])
# df = df.groupby(['TEAM'], as_index=False)['game_id'].count()
# df = df[['processID', 'time_elapsed']].groupby(['processID'], as_index=False)['time_elapsed'].sum()
print(df)
# query = 'SELECT (p.first_name || " " || p.last_name) as player_nm, p.retroID, t.* FROM test_errors t ' \
#         'LEFT JOIN players_dim p ON t.playerID=p.Id ' \
#         'WHERE known_error=? AND testID>90'
# results = conn.execute(query, False)
# df = pd.DataFrame(results.fetchall(), columns=results.keys())
# lt.test_lahman2(2019, 'BAL', 'batting', 1)
# print(df)
# print(pd.DataFrame(results.fetchall(), columns=results.keys()))
# execute query
# query = 'UPDATE tests_log SET status=?'
# conn.execute(query, 'Not Started')
# query = 'DELETE FROM test_errors'  # removes all entries
# conn.execute(query)
# query = 'DELETE FROM games_dim'  # removes all entries
# conn.execute(query)
# '''

'''
# FULL RUN HERE
# DB DECLARATIONS
dbi.db_init()

# full run
fgp = open('TESTS.LOG', mode='w')
fgp.close()
fgp = open('TESTS_FAILED.LOG', mode='w')
fgp.close()
# [2014, 2013, 2012, 2011, 2010, 2009]:  # [2019, 2018, 2017, 2016, 2015]:
# [1989, 1988, 1987, 1986, 1985, 1984, 1983, 1982, 1981, 1980]:
for yr in [2019]:
    s_time = t.time()
    run_testing(yr)
    e_time = t.time()

    # record timing
    fgp = open('TESTS.LOG', mode='a')
    fgp.write(str(yr) + ' - Total Time Elapsed: ' + str(dt.seconds_convert(e_time - s_time)) + '\n')
    fgp.close()
# '''

'''
# RUN STAT_COLLECTOR
conn = dbs.engine.connect()
results = conn.execute('SELECT * FROM gameplay_staging WHERE game_id=?', 'ANA201908300')
gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
# run stat collector
stat_output = sc.stat_collector2(gameplay, 'batting')
# '''

'''
# test stat collector
output_csv = pd.read_csv('OUTPUT.csv')
games = sc.stat_collector2(output_csv)

# load gameplay staging

# run tests
batting_test(games, 2019, output_csv)
pitching_test(games, 2019, output_csv)
fielding_test(games, 2019, output_csv)
# '''

'''
# DB DECLARATIONS
# dbi.db_init()
# yr = 2019  # Id 104 on 2020-10-28

# running process checks
# print("Import Completed: ", ir.import_data(yr))
# print("Team Extract Completed: ", etp.extract_teams(yr))
# print("Roster Extract Completed: ", etp.extract_players(yr))
# rst.roster_staging(yr)

# process one team
# pi.process_data_single_team(2019, 'TOR')

# process all teams
conn = dbs.engine.connect()
# result = conn.execute('SELECT team_abb FROM teams_dim WHERE yearID=?', 104).fetchall()
# teams = [tm for (tm,) in result]
# for tm in teams:
#     pi.process_data_single_team(yr, tm)

# check database
# query = 'SELECT * FROM test_errors'
query = 'SELECT * FROM process_log'
results = conn.execute(query)
print(pd.DataFrame(results.fetchall(), columns=results.keys()))
# '''

'''
# get year
yr = 2019
conn = dbs.engine.connect()
result = conn.execute('SELECT Id FROM years_dim WHERE yearID=?', yr).fetchall()
yearID = [y for (y,) in result][0]
s_time = t.time()
results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=?', yr)
print(t.time() - s_time)
gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
print(t.time() - s_time)
results = None # clear memory?
stat_output = sc.stat_collector2(gameplay, yr)

bat_time = t.time()
test_info = lf.log_test_case(yearID, 'batting')
batting_test(stat_output, yr, gameplay, test_info)
lf.log_process('batting_test', yearID, None, bat_time)
print(test_info)
lf.log_test_complete(test_info)

pitch_time = t.time()
test_info = lf.log_test_case(yearID, 'pitching')
pitching_test(stat_output, yr, gameplay, test_info)
lf.log_process('pitching_test', yearID, None, pitch_time)
lf.log_test_complete(test_info)

field_time = t.time()
test_info = lf.log_test_case(yearID, 'fielding')
fielding_test(stat_output, yr, gameplay, test_info)
lf.log_process('fielding_test', yearID, None, field_time)
lf.log_test_complete(test_info)
# '''

# gameplay staging
# year = 2019
# gs.gameplay_staging(year, 'TOR')
