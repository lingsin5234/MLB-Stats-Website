# this file has functions pertaining to career trajectories
from . import db_setup as dbs
from . import lahman_functions as lf
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression


# career trajectory function - BATTING
def run_career_trajectory_batting(retroID):
    
    # get batting data
    conn = dbs.engine.connect()
    query = 'SELECT y.year, b.* FROM batting b ' \
            'LEFT JOIN players_dim p ON b.playerID=p.Id ' \
            'LEFT JOIN years_dim y ON b.yearID=y.Id ' \
            'WHERE p.retroID=?'
    results = conn.execute(query, retroID)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get age and add to data
    yob = lf.get_birthYear(retroID)
    df['AGE'] = df['year'] - yob
    df.sort_values(['AGE'], inplace=True)
    
    # get OPS
    df.rename(columns={'2B': 'X2B', '3B': 'X3B'}, inplace=True)
    df['OBP'] = df.eval('(H - X2B - X3B - HR + 2 * X2B + 3 * X3B + 4 * HR) / AB')
    df['SLG'] = df.eval('(H + BB + HBP) / (AB + BB + HBP + SF)')
    df['OPS'] = df.eval('OBP + SLG')
    # print(df)
    
    # if get Infinite (NaN), remove
    df.fillna(0, inplace=True)
    
    # fit linear model -- cannot force it into quadratic
    # lm = LinearRegression()
    # ages = [[y] for y in df['AGE'].tolist()]
    # quadratic_ages = [[age - 30 + (age - 30)**2] for age in df['AGE'].tolist()]
    # lm.fit(quadratic_ages, df['OPS'].tolist())
    # print(lm.predict(ages))
    
    # fit quadratic model -- degree=2
    ages = [[y] for y in df['AGE'].tolist()]
    lm = make_pipeline(PolynomialFeatures(degree=2), LinearRegression())
    lm.fit(ages, df['OPS'].tolist())
    # print(lm.predict(ages))
    
    # graph -- only for debugging
    # plt.plot(ages, lm.predict(ages))
    # plt.show()
    
    # convert predict values to df
    trj_df = pd.DataFrame({'Age': df['AGE'].tolist(), 'Fit': lm.predict(ages)})
    # print(trj_df)
    
    return trj_df


# career trajectory function - PITCHING
def run_career_trajectory_pitching(retroID):
    
    # get pitching data
    conn = dbs.engine.connect()
    query = 'SELECT y.year, b.* FROM pitching b ' \
            'LEFT JOIN players_dim p ON b.playerID=p.Id ' \
            'LEFT JOIN years_dim y ON b.yearID=y.Id ' \
            'WHERE p.retroID=?'
    results = conn.execute(query, retroID)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get age and add to data
    yob = lf.get_birthYear(retroID)
    df['AGE'] = df['year'] - yob
    df.sort_values(['AGE'], inplace=True)
    
    # get WHIP
    df['WHIP'] = df.eval('(W + H) / (IPouts / 3)')
    # print(df)

    # if get Infinite (NaN), remove
    df.fillna(0, inplace=True)
    
    # fit polynomial model -- degree=3
    ages = [[y] for y in df['AGE'].tolist()]
    lm = make_pipeline(PolynomialFeatures(degree=3), LinearRegression())
    lm.fit(ages, df['WHIP'].tolist())
    # print(lm.predict(ages))
    
    # graph -- only for debugging
    # plt.plot(ages, lm.predict(ages))
    # plt.show()
    
    # convert predict values to df
    trj_df = pd.DataFrame({'Age': df['AGE'].tolist(), 'Fit': lm.predict(ages)})
    # print(trj_df)
    
    return trj_df


# run_career_trajectory_batting('suzui001')
# run_career_trajectory_pitching('hallr001')
