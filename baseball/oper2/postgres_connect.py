# connecting to postgresql database
import sqlalchemy as sa
import os
from . import class_structure as cl

# try to connect, if cannot, that means db not created yet
try:

    # main engine for connecting to baseball database on postgresql
    engine = sa.create_engine(os.environ.get('POSTGRES_ENGINE'), echo=True)

except Exception:
    
    # setup engine and database
    eng = sa.create_engine(os.environ.get('POSTGRES_ENG'), echo=True)
    conn = eng.connect()
    conn.execute('commit')
    conn.execute('CREATE DATABASE baseball')
    conn.close()

    # main engine for connecting to baseball database on postgresql
    engine = sa.create_engine(os.environ.get('POSTGRES_ENGINE'), echo=True)
    
# safe to call multiple times as it will FIRST check for table presence
cl.metadata.create_all(engine)
