# this file consists of the moving stat collector to stats staging / stat tests; and moving stats staging to main
from . import db_setup as dbs
from . import global_variables as gv
from . import common_functions as cf
from . import logging_functions as lf
import pandas as pd


# prep staging, moving stat_collector output to stats_staging and [stat]_testing
def stats_staging(stat_output, stat_type):

    # connect to database
    conn = dbs.engine.connect()

    #  -----  BATTING  -----  #
    if stat_type == 'batting':
        df = stat_output
        df.rename(columns={'playerID': 'player_id'}, inplace=True)  # follow db format for ids
        # print(df.head())
        
        # prep and send to staging
        df.to_sql('batting_staging', conn, if_exists='append', index=False)
        
        # prep and send to batting_test
        df.rename(columns={'player_id': 'playerID'}, inplace=True)  # change back for testing
        df = df[gv.for_lahman_bat]
        df.to_sql('batting_test', conn, if_exists='append', index=False)
    
    #  ---------------------  #
    
    #  -----  PITCHING  -----  #
    elif stat_type == 'pitching':
        df = stat_output
        df.rename(columns={'playerID': 'player_id'}, inplace=True)  # follow db format for ids
        # print(df.head())
    
        # prep and send to staging
        df.to_sql('pitching_staging', conn, if_exists='append', index=False)
    
        # prep and send to pitching_test
        df.rename(columns={'player_id': 'playerID'}, inplace=True)  # change back for testing
        df = df[gv.for_lahman_pitch]
        df.to_sql('pitching_test', conn, if_exists='append', index=False)

    #  ----------------------  #

    #  -----  FIELDING  -----  #
    else:
        df = stat_output
        df.rename(columns={'playerID': 'player_id'}, inplace=True)  # follow db format for ids
        # print(df.head())
    
        # prep and send to staging
        df.to_sql('fielding_staging', conn, if_exists='append', index=False)
    
        # prep and send to fielding_test
        df.rename(columns={'player_id': 'playerID'}, inplace=True)  # change back for testing
        df = df[gv.for_lahman_field]
        df.to_sql('fielding_test', conn, if_exists='append', index=False)
    
    #  ----------------------  #

    return True


# load stats from staging to main tables
def stats_load(yearID, stat_type):
    
    # connect to database
    conn = dbs.engine.connect()
    
    #  -----  BATTING  -----  #
    if stat_type == 'batting':
        
        # load from batting_staging
        results = conn.execute('SELECT * FROM batting_staging')
        df = pd.DataFrame(results.fetchall(), columns=results.keys())
        
        # set yearID. all missing values set to None on to_sql load (LOB, RLSP)
        df['yearID'] = yearID

    #  ----------------------  #
    
    #  -----  PITCHING  -----  #
    elif stat_type == 'pitching':
        
        # load from pitching_staging
        results = conn.execute('SELECT * FROM pitching_staging')
        df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
        # set yearID. set innings; all missing values set to None on to_sql load (HD, CI, DI, PT etc.)
        df['yearID'] = yearID
        # df['IPouts'] = df['IPouts'].apply(cf.convert_innings)
        # df.rename(columns={'IPouts': 'IP'}, inplace=True)

    #  ----------------------  #
    
    #  -----  FIELDING  -----  #
    else:
        # load from fielding_staging
        results = conn.execute('SELECT * FROM fielding_staging')
        df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
        # set yearID. set innings; all missing values set to None on to_sql load (BF, CI, DI)
        df['yearID'] = yearID
        # df['InnOuts'] = df['InnOuts'].apply(cf.convert_innings)
        # df.rename(columns={'InnOuts': 'Inn'}, inplace=True)
    
    #  ----------------------  #
    
    # set playerID
    df.drop('Id', axis=1, inplace=True)
    results = conn.execute('SELECT Id, retroID FROM players_dim')
    players = pd.DataFrame(results.fetchall(), columns=results.keys())
    df = df.merge(players, left_on='player_id', right_on='retroID')
    df.drop(['player_id', 'retroID'], axis=1, inplace=True)
    df.rename(columns={'Id': 'playerID'}, inplace=True)
    
    # set teamID
    results = conn.execute('SELECT Id, team_abb FROM teams_dim WHERE yearID=?', yearID)
    teams = pd.DataFrame(results.fetchall(), columns=results.keys())
    df = df.merge(teams, left_on='team_id', right_on='team_abb')
    df.drop(['team_id', 'team_abb'], axis=1, inplace=True)
    df.rename(columns={'Id': 'teamID'}, inplace=True)
    
    # combine rows -- home + all away games -- by stint
    df = df.groupby(
        ['playerID', 'teamID', 'yearID'],
        as_index=False)[[c for c in df.columns if c not in ['playerID', 'teamID', 'yearID']]].sum()
    
    # load df to database
    if stat_type == 'batting':
        df.to_sql('batting', conn, if_exists='append', index=False)
    
    elif stat_type == 'pitching':
        df.to_sql('pitching', conn, if_exists='append', index=False)
        
    # fielding
    else:
        df.to_sql('fielding', conn, if_exists='append', index=False)
    
    return True
