# this file is for error logging functionality
import time as t
import pandas as pd
from datetime import datetime
from . import db_setup as dbs
from . import common_functions as cf


# error logging function
def error_logger(error, process_name, team, year, stat_type):

    # connect to database
    conn = dbs.engine.connect()
    
    # get process Id, year Id, team Id, stat_type Id
    processID = cf.get_process(process_name)
    yearID = cf.get_year(year)
    
    if team is not None:
        teamID = cf.get_team(year, team)
    else:
        teamID = None
        
    if stat_type is not None:
        stat_typeID = cf.get_stat_type(stat_type)
    else:
        stat_typeID = None

    # current time
    curr_time = datetime.fromtimestamp(t.mktime(t.localtime()))
    
    # log error
    error_dict = {
        'processID': processID,
        'yearID': yearID,
        'teamID': teamID,
        'stat_typeID': stat_typeID,
        'gameID': None,
        'error': str(error),
        'timestamp': curr_time
    }
    error_df = pd.DataFrame.from_records([error_dict])
    error_df.to_sql('error_log', conn, if_exists='append', index=False)

    return True


# processing errors function
def processing_errors(error, process_name, team, year, game_id, half_inning):
    # connect to database
    conn = dbs.engine.connect()
    
    # get process Id, year Id, team Id, stat_type Id
    processID = cf.get_process(process_name)
    yearID = cf.get_year(year)
    teamID = cf.get_team(year, team)
    gameID = cf.get_game(game_id)
    
    # current time
    curr_time = datetime.fromtimestamp(t.mktime(t.localtime()))
    
    # log error
    error_dict = {
        'processID': processID,
        'yearID': yearID,
        'teamID': teamID,
        'stat_typeID': None,
        'gameID': gameID,
        'error': 'Half Inning: ' + str(half_inning) + ' - ' + str(error),
        'timestamp': curr_time
    }
    error_df = pd.DataFrame.from_dict(error_dict)
    error_df.to_sql('error_log', conn, if_exists='append', index=False)

    return True
