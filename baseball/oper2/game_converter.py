# libraries
import pandas as pd
import time as t
from . import date_time as dt


# convert play by play to tables
def convert_games(all_games, games_roster, game_info_dict):
    # get game id from game_rosters
    game_id = games_roster.game_id.unique()
    year = games_roster.data_year.unique()[0]
    
    games_dfs = []
    for g, each_game in enumerate(all_games):
        
        # performance check
        g1 = t.time()
        
        # convert to dictionary
        game_dict = {}
        idx = 0
        
        # remove \n, then split by comma
        this_list = [i.split('\n')[0] for i in each_game[-1]]
        this_list = [i.split(',') for i in this_list]
        
        # get the starting lineup for this game (home + away)
        home_lineup = games_roster[(games_roster['game_id'] == game_id[g]) & (games_roster['team_id'] == '1')]
        away_lineup = games_roster[(games_roster['game_id'] == game_id[g]) & (games_roster['team_id'] == '0')]
        
        # for NL games, put None for DH; check each (in case of Bumgarner = DH)
        if len(home_lineup.loc[home_lineup['fielding'] == '10', 'player_id']) == 0:
            home_DH = None
        else:
            home_DH = home_lineup.loc[home_lineup['fielding'] == '10', 'player_id'].values[0]

        if len(away_lineup.loc[away_lineup['fielding'] == '10', 'player_id']) == 0:
            away_DH = None
        else:
            away_DH = away_lineup.loc[away_lineup['fielding'] == '10', 'player_id'].values[0]
        
        # --- GAME INFO --- #
        # str.split game info embedded lists, convert to dataframe
        game_info = list(map(lambda x: x.split(','), game_info_dict[game_id[g]]))
        info_df = pd.DataFrame(game_info, columns=['info', 'item', 'value'])
        
        #  grab win / loss and save ids
        win_id = info_df.loc[info_df['item'] == 'wp', 'value'].values[0]
        loss_id = info_df.loc[info_df['item'] == 'lp', 'value'].values[0]
        save_id = info_df.loc[info_df['item'] == 'save', 'value'].values[0]
        
        # game_id, home team, away team
        single_game_id = game_id[g]
        home_team = info_df.loc[info_df['item'] == 'hometeam', 'value'].values[0]
        away_team = info_df.loc[info_df['item'] == 'visteam', 'value'].values[0]
        
        # game_info dict
        single_game_dict = {
            'game_id': single_game_id,
            'home_team': home_team,
            'away_team': away_team,
            'win_id': win_id,
            'loss_id': loss_id,
            'save_id': save_id,
            'play_id': idx,
            'data_year': year,
        }
        single_game_df = pd.DataFrame.from_dict(single_game_dict, orient="index").transpose()
        
        # ----------------- #
        
        # --- LOOP EVENTS --- #
        
        # loop through and assign dict value
        for idx, i in enumerate(this_list):
            
            if i[0] == 'play':
                
                # AWAY TEAM batting
                if i[2] == '0':
                    dct = {'gm_type': i[0],
                           'inning': i[1],
                           'half': i[2],
                           'half_innings': i[1] + '_' + i[2],
                           'playerID': i[3],
                           'playerTeamID': away_team,
                           'pitch_count': i[4],
                           'pitches': i[5],
                           'play': i[6],
                           'player_name': None,
                           'team_id': i[2],
                           'batting': None,
                           'fielding': None,
                           'fieldingTeamID': home_team
                           }
                # HOME TEAM batting
                else:
                    dct = {'gm_type': i[0],
                           'inning': i[1],
                           'half': i[2],
                           'half_innings': i[1] + '_' + i[2],
                           'playerID': i[3],
                           'playerTeamID': home_team,
                           'pitch_count': i[4],
                           'pitches': i[5],
                           'play': i[6],
                           'player_name': None,
                           'team_id': i[2],
                           'batting': None,
                           'fielding': None,
                           'fieldingTeamID': away_team
                           }
            else:
                # AWAY TEAM substitution
                if i[3] == '0':
                    dct = {'gm_type': i[0],
                           'inning': None,
                           'half': None,
                           'half_innings': None,
                           'playerID': i[1],
                           'playerTeamID': away_team,
                           'pitch_count': '',
                           'pitches': None,
                           'play': None,
                           'player_name': i[2],
                           'team_id': i[3],
                           'batting': i[4],
                           'fielding': i[5],
                           'fieldingTeamID': None
                           }
                # HOME TEAM substitutions
                else:
                    dct = {'gm_type': i[0],
                           'inning': None,
                           'half': None,
                           'half_innings': None,
                           'playerID': i[1],
                           'playerTeamID': home_team,
                           'pitch_count': '',
                           'pitches': None,
                           'play': None,
                           'player_name': i[2],
                           'team_id': i[3],
                           'batting': i[4],
                           'fielding': i[5],
                           'fieldingTeamID': None
                           }
            game_dict[idx] = dct
            idx += 1

        # generate the temp data frame
        temp_df = pd.DataFrame.from_dict(game_dict, orient="index")

        # ------------------- #
        
        # --- NEW COLUMNS --- #
        
        # add remaining columns using concat with a new DataFrame
        new_columns = {'pitcherID': None,
                       'recorded_outs': 0,
                       'total_outs': 0,
                       'before_1B': None,
                       'before_2B': None,
                       'before_3B': None,
                       'after_1B': None,
                       'after_2B': None,
                       'after_3B': None,
                       'runs_scored': 0,
                       'total_scored': 0,
                       'RBI_awarded': 0,
                       'putouts': '',
                       'assists': '',
                       'errors': '',
                       'num_inh': 0,
                       'inh_run1': '',
                       'inh_run2': '',
                       'inh_run3': '',
                       'inh_earned1': '',
                       'inh_earned2': '',
                       'inh_earned3': '',
                       # 'BT_move': False,  # don't need
                       'BT_dest': 0,
                       'R1_move': False,
                       'R1_dest': 0,
                       'R1_inhp': None,
                       'R2_move': False,
                       'R2_dest': 0,
                       'R2_inhp': None,
                       'R3_move': False,
                       'R3_dest': 0,
                       'R3_inhp': None,
                       'SB2_runner': None,
                       'SB3_runner': None,
                       'SBH_runner': None,
                       'CS2_runner': None,
                       'CS3_runner': None,
                       'CSH_runner': None,
                       'PO1_runner': None,
                       'PO2_runner': None,
                       'PO3_runner': None,
                       'sub_appearance': None,
                       'prev_pitcherID': None,
                       'DP_flag': False,
                       'GDP_flag': False,
                       'TP_flag': False,
                       'SH_flag': False,
                       'SF_flag': False,
                       'WP_flag': False,
                       'PB_flag': False,
                       'BINT_flag': False,
                       'RINT_flag': False,
                       'FINT_flag': False,
                       'injured_strikeout': None,
                       'pinch_pitcher': None,
                       'new_half': False,
                       'EVENT_CD': 0,
                       'home_P': home_lineup.loc[home_lineup['fielding'] == '1', 'player_id'].values[0],
                       'home_C': home_lineup.loc[home_lineup['fielding'] == '2', 'player_id'].values[0],
                       'home_1B': home_lineup.loc[home_lineup['fielding'] == '3', 'player_id'].values[0],
                       'home_2B': home_lineup.loc[home_lineup['fielding'] == '4', 'player_id'].values[0],
                       'home_3B': home_lineup.loc[home_lineup['fielding'] == '5', 'player_id'].values[0],
                       'home_SS': home_lineup.loc[home_lineup['fielding'] == '6', 'player_id'].values[0],
                       'home_LF': home_lineup.loc[home_lineup['fielding'] == '7', 'player_id'].values[0],
                       'home_CF': home_lineup.loc[home_lineup['fielding'] == '8', 'player_id'].values[0],
                       'home_RF': home_lineup.loc[home_lineup['fielding'] == '9', 'player_id'].values[0],
                       'home_DH': home_DH,
                       'away_P': away_lineup.loc[away_lineup['fielding'] == '1', 'player_id'].values[0],
                       'away_C': away_lineup.loc[away_lineup['fielding'] == '2', 'player_id'].values[0],
                       'away_1B': away_lineup.loc[away_lineup['fielding'] == '3', 'player_id'].values[0],
                       'away_2B': away_lineup.loc[away_lineup['fielding'] == '4', 'player_id'].values[0],
                       'away_3B': away_lineup.loc[away_lineup['fielding'] == '5', 'player_id'].values[0],
                       'away_SS': away_lineup.loc[away_lineup['fielding'] == '6', 'player_id'].values[0],
                       'away_LF': away_lineup.loc[away_lineup['fielding'] == '7', 'player_id'].values[0],
                       'away_CF': away_lineup.loc[away_lineup['fielding'] == '8', 'player_id'].values[0],
                       'away_RF': away_lineup.loc[away_lineup['fielding'] == '9', 'player_id'].values[0],
                       'away_DH': away_DH,
                       'home_b1': home_lineup.loc[home_lineup['bat_lineup'] == '1', 'player_id'].values[0],
                       'home_b2': home_lineup.loc[home_lineup['bat_lineup'] == '2', 'player_id'].values[0],
                       'home_b3': home_lineup.loc[home_lineup['bat_lineup'] == '3', 'player_id'].values[0],
                       'home_b4': home_lineup.loc[home_lineup['bat_lineup'] == '4', 'player_id'].values[0],
                       'home_b5': home_lineup.loc[home_lineup['bat_lineup'] == '5', 'player_id'].values[0],
                       'home_b6': home_lineup.loc[home_lineup['bat_lineup'] == '6', 'player_id'].values[0],
                       'home_b7': home_lineup.loc[home_lineup['bat_lineup'] == '7', 'player_id'].values[0],
                       'home_b8': home_lineup.loc[home_lineup['bat_lineup'] == '8', 'player_id'].values[0],
                       'home_b9': home_lineup.loc[home_lineup['bat_lineup'] == '9', 'player_id'].values[0],
                       'away_b1': away_lineup.loc[away_lineup['bat_lineup'] == '1', 'player_id'].values[0],
                       'away_b2': away_lineup.loc[away_lineup['bat_lineup'] == '2', 'player_id'].values[0],
                       'away_b3': away_lineup.loc[away_lineup['bat_lineup'] == '3', 'player_id'].values[0],
                       'away_b4': away_lineup.loc[away_lineup['bat_lineup'] == '4', 'player_id'].values[0],
                       'away_b5': away_lineup.loc[away_lineup['bat_lineup'] == '5', 'player_id'].values[0],
                       'away_b6': away_lineup.loc[away_lineup['bat_lineup'] == '6', 'player_id'].values[0],
                       'away_b7': away_lineup.loc[away_lineup['bat_lineup'] == '7', 'player_id'].values[0],
                       'away_b8': away_lineup.loc[away_lineup['bat_lineup'] == '8', 'player_id'].values[0],
                       'away_b9': away_lineup.loc[away_lineup['bat_lineup'] == '9', 'player_id'].values[0]
                       }
        new_df = pd.DataFrame.from_dict(new_columns, orient="index").transpose()

        # ------------------- #
        
        # --- CONCAT DATA FRAMES --- #
        
        # reset index after generating data frame with same number of rows of the same values
        comb_df1 = pd.concat([single_game_df]*len(temp_df)).reset_index(drop=True)
        comb_df2 = pd.concat([new_df]*len(temp_df)).reset_index(drop=True)
        final_df = pd.concat([comb_df1, temp_df, comb_df2], axis=1)

        # -------------------------- #

        # --- EDIT HALF INNINGS --- #
        
        # add new half-inning indicator
        final_df.loc[0, 'new_half'] = True  # start of new game
        for idx, row in final_df.iterrows():
            if idx > 0:
                if row['inning'] is None:
                    final_df.loc[idx, 'inning'] = final_df.loc[idx - 1, 'inning']
                    final_df.loc[idx, 'half'] = final_df.loc[idx - 1, 'half']
                    final_df.loc[idx, 'half_innings'] = final_df.loc[idx - 1, 'half_innings']
                if final_df.loc[idx, 'half_innings'] != final_df.loc[idx - 1, 'half_innings']:
                    final_df.loc[idx, 'new_half'] = True

        # ------------------------- #

        # add to games_dfs list
        # back to "index" format which is {index -> {column -> value}}
        games_dfs.append(final_df.to_dict(orient="index"))
        
        # WARNING: DataFrame columns are not unique, some columns will be omitted.
        # Games are put into list, so within the list, the games are unique
        # print(final_df)
        
        # performance check
        # print('Game #' + str(g) + ' takes ' + dt.seconds_convert(t.time() - g1))
    
    return games_dfs
