# libraries
import re
from . import pitcher_oper as po
from . import base_running as br
from . import global_variables as gv
from . import fielding_oper as fo


# function for processing non-plate appearances
# v2: no need for lineup, run_play, pid, hid
def non_pa2(this_line, begin_play):

    pt = None

    # if bool(re.search(r'^(WP|NP|BK|PB|FLE|OA|SB|CS|PO|DI)', begin_play)):
    if re.search(r'WP', begin_play):
        this_line['EVENT_CD'] = gv.event_types['WP']
        this_line['WP_flag'] = True
    if re.search(r'NP', begin_play):
        this_line['EVENT_CD'] = gv.event_types['NOE']
    if re.search(r'BK', begin_play):
        this_line['EVENT_CD'] = gv.event_types['BK']
    if re.search(r'PB', begin_play):
        this_line['EVENT_CD'] = gv.event_types['PB']
        this_line['PB_flag'] = True
    if re.search(r'FLE', begin_play):
        this_line['EVENT_CD'] = gv.event_types['FLE']

        # assign error
        fielder = re.sub(r'FLE([1-9]).*', '\\1', begin_play)
        fo.fielding_processor3(fielder, this_line, ['E'])
    if re.search(r'OA', begin_play):
        this_line['EVENT_CD'] = gv.event_types['OA']
    if re.search(r'SB', begin_play):
        # run steal_processor
        this_line = br.steal_processor2(this_line)
        this_line['EVENT_CD'] = gv.event_types['SB']

    # POCS is runner charged with Caught Stealing
    if bool(re.search(r'CS', begin_play)) | bool(re.search(r'POCS', begin_play)):
        # run steal_processor
        this_line = br.steal_processor2(this_line)
        this_line['EVENT_CD'] = gv.event_types['CS']

    if bool(re.search(r'PO', begin_play)) and not(bool(re.search(r'POCS', begin_play))):

        this_line['EVENT_CD'] = gv.event_types['PO']
        # if not an error then PO successful
        if not(bool(re.search(r'E', begin_play))):

            # figure out which base is out
            if re.search(r'PO1', begin_play):
                this_line['R1_move'] = True
                this_line['R1_dest'] = 0
            elif re.search(r'PO2', begin_play):
                this_line['R2_move'] = True
                this_line['R2_dest'] = 0
            elif re.search(r'PO3', begin_play):
                this_line['R3_move'] = True
                this_line['R3_dest'] = 0

        # fielding for Pick-Offs (PO)
        if re.search(r'PO[123]\([1-9E/TH]+\)', begin_play):

            # pick off plays
            po_play = re.sub(r'.*PO[123]\(([1-9E/TH]+)\).*', '\\1', begin_play)
            if bool(re.search('E', po_play)):
                fo.fielding_assign_stats2(r'E|\D', po_play, this_line, ['A'], ['E'])
            else:
                fo.fielding_assign_stats2(r'\D', po_play, this_line, ['A'], ['PO'])

    if re.search(r'DI', begin_play):
        this_line['EVENT_CD'] = gv.event_types['DI']

    return this_line
