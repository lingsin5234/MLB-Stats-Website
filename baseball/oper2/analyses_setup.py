# functions used to setup database for analyses
from . import db_setup as dbs
from . import run_similarity as rsim
from . import common_functions as cf
import pandas as pd


# setup for similarity - BATTING, for PA >= 3000
def batting_setup_pa3000():
    
    # --- SETUP WORK --- #
    
    # get all batters with a minimum of 3000 plate appearances
    conn = dbs.engine.connect()
    query = 'SELECT * FROM batting b WHERE playerID in ' \
            '(SELECT playerID FROM batting b GROUP BY playerID HAVING SUM(PA) >= 3000) '
    results = conn.execute(query)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    # print(df)
    
    # get years
    results = conn.execute('SELECT * FROM years_dim')
    years = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get players
    results = conn.execute('SELECT Id, retroID, first_name, last_name FROM players_dim')
    players = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get positions
    results = conn.execute('SELECT Id, pos_abb FROM positions_dim')
    positions = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get rosters
    results = conn.execute('SELECT * FROM rosters')
    rosters = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # --- MERGE ROSTER WITH BATTING --- #
    
    # merge data frames to roster
    merge_df = pd.merge(left=rosters, right=years, left_on='yearID', right_on='Id').drop(['Id_x', 'Id_y'], axis=1)
    merge_df = merge_df.merge(players, left_on='playerID', right_on='Id').drop(['Id'], axis=1)
    merge_df = merge_df.merge(positions, left_on='positionID', right_on='Id').drop(['Id', 'positionID'], axis=1)
    
    # merge roster to batting df
    df = df.merge(merge_df, on=['yearID', 'teamID', 'playerID']).drop(['Id', 'yearID', 'playerID', 'teamID'], axis=1)
    
    # --- CALCULATE POSITION SCORE --- #
    
    # apply position score first
    df['POS'] = df['pos_abb'].apply(rsim.position_score)
    
    # --- INSERT TO DB --- #
    
    # first pull from database and remove existing entries
    results = conn.execute('SELECT * FROM batting_3000')
    exist_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    exist_df.drop(['Id'], axis=1, inplace=True)
    
    # drop ALL duplicates, both sets of existing
    diff_df = pd.concat([df, exist_df]).drop_duplicates(keep=False)
    # print(len(df))
    # print(len(exist_df))
    # print(diff_df)
    
    # insert to db
    if len(diff_df) > 0:
        diff_df.to_sql('batting_3000', conn, if_exists='append', index=False)
        print("Appended new entries to batting_3000")
    else:
        print("No new entries for batting_3000")
    
    return True


# setup for similarity - PITCHING, for IPouts >= 3000
def pitching_setup_ip3000():
    
    # --- SETUP WORK --- #
    
    # get all pitchers with a minimum of 1000 innings pitched (3000 IPouts)
    conn = dbs.engine.connect()
    query = 'SELECT * FROM pitching p WHERE playerID in ' \
            '(SELECT playerID FROM pitching p GROUP BY playerID HAVING SUM(IPouts) >= 3000)'
    results = conn.execute(query)
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get years
    results = conn.execute('SELECT * FROM years_dim')
    years = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get players
    results = conn.execute('SELECT Id, retroID, first_name, last_name, throws FROM players_dim')
    players = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get positions
    results = conn.execute('SELECT Id, pos_abb FROM positions_dim')
    positions = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # get rosters
    results = conn.execute('SELECT * FROM rosters')
    rosters = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # --- MERGE ROSTER WITH PITCHING --- #
    
    # merge data frames to roster
    merge_df = pd.merge(left=rosters, right=years, left_on='yearID', right_on='Id').drop(['Id_x', 'Id_y'], axis=1)
    merge_df = merge_df.merge(players, left_on='playerID', right_on='Id').drop(['Id'], axis=1)
    merge_df = merge_df.merge(positions, left_on='positionID', right_on='Id').drop(['Id', 'positionID'], axis=1)
    
    # merge roster to batting df
    df = df.merge(merge_df, on=['yearID', 'teamID', 'playerID']).drop(['Id', 'yearID', 'playerID', 'teamID'], axis=1)
    
    # --- INSERT TO DB --- #
    
    # first pull from database and remove existing entries
    results = conn.execute('SELECT * FROM pitching_3000')
    exist_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    exist_df.drop(['Id'], axis=1, inplace=True)
    
    # drop ALL duplicates, both sets of existing
    diff_df = pd.concat([df, exist_df]).drop_duplicates(keep=False)
    # print(len(df))
    # print(len(exist_df))
    # print(diff_df)
    
    # insert to db
    if len(diff_df) > 0:
        diff_df.to_sql('pitching_3000', conn, if_exists='append', index=False)
        print("Appended new entries to pitching_1000")
    else:
        print("No new entries for pitching_1000")
    
    return True


# batting_setup_pa3000()
# pitching_setup_ip3000()
