# these functions clear the staging tables for gameplay and stats
from . import db_setup as dbs


# clear gameplay staging
def clear_gameplay_staging():
    
    # connect to db
    conn = dbs.engine.connect()
    
    # execute query
    conn.execute('DELETE FROM gameplay_staging')
    
    return True


# clear stats testing and staging tables
def clear_stats_staging():
    
    # connect to db
    conn = dbs.engine.connect()
    
    # execute queries
    conn.execute('DELETE FROM batting_staging')
    conn.execute('DELETE FROM pitching_staging')
    conn.execute('DELETE FROM fielding_staging')
    conn.execute('DELETE FROM batting_test')
    conn.execute('DELETE FROM pitching_test')
    conn.execute('DELETE FROM fielding_test')
    
    return True
