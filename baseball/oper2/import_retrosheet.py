# this python script is to import the retrosheet files
from os import path, mkdir, remove
import sys
import time as t
from datetime import datetime
import pandas as pd
import urllib.request
from zipfile import ZipFile as zf
from . import global_variables as gv
from . import error_logger as el
from . import db_setup as dbs
from . import logging_functions as lf
from . import common_functions as cf


# if len(sys.argv) > 1:
def import_data(year):

    t1_time = t.time()
    year = str(year)  # force into a string

    # create import folder if not available
    if path.exists(gv.data_dir):
        pass
    else:
        mkdir(gv.data_dir)

    # create landing folder if not available
    if path.exists(gv.data_dir + '/landing'):
        pass
    else:
        mkdir(gv.data_dir + '/landing')

    # download file into import/landing folder
    url = 'https://www.retrosheet.org/events/'
    # year = sys.argv[1]
    zip_file = year + 'eve.zip'
    urllib.request.urlretrieve(url+zip_file, gv.data_dir + '/landing/'+zip_file)

    # create new folder for the unzipped contents
    if path.exists(gv.data_dir + '/' + year):
        pass
    else:
        mkdir(gv.data_dir + '/' + year)

    # unzip contents to the year folder
    try:
        with zf(gv.data_dir + '/landing/'+zip_file) as unzip:
            unzip.extractall(gv.data_dir + '/' + year)
    except Exception as e:
        # accept any types of errors
        el.error_logger(e, 'unzipping import year: ' + str(e), None, year, '')
        return False

    # remove landing file
    try:
        if path.exists(gv.data_dir + '/landing/' + zip_file):
            remove(gv.data_dir + '/landing/' + zip_file)
    except Exception as e:
        # accept any types of errors
        el.error_logger(e, 'removing landing file: ' + str(e), None, year, '')
        return False

    t2_time = t.time()

    # send completion notice
    conn = dbs.engine.connect()
    conn.fast_executemany = True

    # get year ID
    yearID = cf.get_year(year)

    # log completion
    lf.log_process('import_year', yearID, None, t1_time)

    # create an OUTPUT file for later use in processing --- can omit once system testing completed
    df = pd.DataFrame(columns=gv.gameplay_cols)
    df.to_csv('OUTPUT.csv', mode='w', index=False, header=True)

    return True
