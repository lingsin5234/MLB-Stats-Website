# libraries
from . import global_variables as gv
from . import stat_collector as sc
from . import base_running as br
from . import pitcher_oper as po
import pandas as pd
import re


# lineup substitutions
def lineup_substitution2(this_line, pid, sub_type, field_pos):

    # get the correct list of fielding column names
    if this_line['team_id'] == '0':
        fielding_lineup = gv.away_fielding
        batting_lineup = gv.away_batting
    else:
        fielding_lineup = gv.home_fielding
        batting_lineup = gv.home_batting
        
    # indices
    bat_spot_id = int(this_line['batting'])
    field_spot_id = field_pos
    
    # batting substitutions
    if sub_type == 'batting':
        
        # get the subbed-out player in the batting lineup
        bat_spot = batting_lineup[bat_spot_id - 1]
        sub_out_id = this_line[bat_spot]
        
        # replace with pid in fielding and batting lineups
        for idx, field_spot in enumerate(fielding_lineup):
            if this_line[field_spot] == sub_out_id:
                this_line[field_spot] = pid
                this_line[bat_spot] = pid
                field_spot_id = idx + 1
                break

        return [this_line, sub_out_id, field_spot_id, bat_spot_id]

    # fielding/pitching substitutions
    else:
        
        # directly replace in fielding lineup
        field_spot = fielding_lineup[field_spot_id - 1]
        sub_out_id = this_line[field_spot]
        
        # if pitcher substitution in AL game, do not change batting
        if bat_spot_id != 0:
            bat_spot = batting_lineup[bat_spot_id - 1]
            this_line[bat_spot] = pid
        
        # replace with pid in fielding lineup
        this_line[field_spot] = pid

        return [this_line, sub_out_id, field_spot_id, bat_spot_id]


# lineup assignment -- for each line, copy prev lineup
def lineup_assignments(this_line, prev_line):
    
    for col in gv.all_lineup:
        this_line[col] = prev_line[col]
    
    return this_line


# based on the fielding play, determine which stats should be recorded
# version 2: does not send to field collector, assign to column directly
# version 3: retrieves lineup from this_line
def fielding_processor3(field_index, this_line, stat_types):

    # check which team is batting
    if this_line['team_id'] == '0':
        lineup = gv.home_fielding  # 0 is away batting; home fielding
    else:
        lineup = gv.away_fielding

    # get the fielder ID
    field_spot = lineup[int(field_index) - 1]
    fid = this_line[field_spot]

    # using the stat_type to place the fielder ID in correct column
    for stat in stat_types:
        if stat == 'A':
            this_line['assists'] += fid + ';'
        elif stat == 'PO':
            this_line['putouts'] += fid + ';'
        elif stat == 'E':
            this_line['errors'] += fid + ';'

    return this_line


# get and send fielder id to PO column
def fielding_po3(begin_play, grep_search, this_line):

    # remove (B) if not part of grep_search
    if not(bool(re.search(r'(B)', grep_search))):
        begin_play = re.sub(r'\(B\)', '', begin_play)

    # get the fielder position who made the putout
    try:
        fielder = fielding_unique(grep_search, begin_play)[1]
    except Exception as e:
        print(grep_search, this_line['play'], begin_play, e)
    else:
        ft = ['PO']
        # send fielder position to processing (then collector)
        fielding_processor3(fielder, this_line, ft)

    return True


# return a unique list of fielding assists whilst keeping the order
# unique in reverse because the last fielder is key to put out in most cases
def fielding_unique(grep_search, begin_play):

    # extra check for double/triple plays with multiple RUNNER force outs
    reduce_play = re.sub("\([B123]\)", '', re.sub(grep_search, '', begin_play))

    fielders = [int(f) for f in reduce_play]
    length = len(fielders)

    # get last fielder and remove from fielders
    if length > 1:
        last_fielder = fielders[length - 1]
        fielders = fielders[:length-1]
        length = len(fielders)

        unique_set = set()
        for i in reversed(range(0, length)):
            if fielders[i] not in unique_set:
                unique_set.add(fielders[i])
            else:
                fielders.pop(i)

    else:
        last_fielder = fielders[0]
        fielders = []  # no assists, make it blank

    # check for Assist + Putout being the same player (equal Unassisted)
    # this is most likely to occur on unassisted double play
    if len(fielders) == 1:
        if last_fielder == fielders[0]:
            fielders = []

    return [fielders, last_fielder]


# common assigning assists and putouts/errors function
def fielding_assign_stats2(grep_unique, the_play, this_line, ft1, ft2):

    # get unique, but separate the last fielder -- in case of 343/G
    all_fielders = fielding_unique(grep_unique, the_play)
    fielders = all_fielders[0]
    last_fielder = all_fielders[1]

    # if fielders is 0, e.g. no assists, nothing happens.
    for idx in range(0, len(fielders)):
        # record the stat(s) for non-last fielders (usually this assigns 'A')
        fielding_processor3(fielders[idx], this_line, ft1)

    # record stat(s) for last fielder (usually 'PO' or 'E')
    fielding_processor3(last_fielder, this_line, ft2)

    return True


# fielders get assigned Inning
def fielder_innings(lineup, this_line, team_name, data_year, game_id, this_half, actual_play, pitch_count,
                    num_outs, bases_taken, stat_team, stat_type):

    if this_line['team_id'] == '0':
        fielding_team = '1'
    else:
        fielding_team = '0'

    # loop through fielders to recdord Inn -- exclude DH's
    fielding_bool = (lineup['team_id'] == fielding_team) & (lineup['fielding'] != '10')
    fielding_lineup = list(lineup[fielding_bool]['player_id'])
    for fid in fielding_lineup:
        sc.stat_appender(fid, team_name, data_year, game_id, this_half, stat_type, 1, actual_play, pitch_count,
                         num_outs, bases_taken, stat_team, 'fielding')
        # DEBUG
        # if fid == 'donaj001':
        #     print(fid, this_line, fielding_team)

    return True
