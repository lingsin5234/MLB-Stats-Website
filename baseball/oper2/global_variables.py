# libraries
import pandas as pd

# import directory
is_prod = True
if is_prod:
    data_dir = 'baseball/import'
else:
    data_dir = 'import'

# create global tables.
player_stats = pd.DataFrame()

# pujols tracker -- just to help check fielding lol
pujols_tracker = {
    'A': 0,
    'PO': 0,
    'E': 0,
    'DP': 0,
    'TP': 0,
    'game_id': '',
    'this_half': '',
    'play': '',
    'stat_type': '',
    'batter': ''
}

# create dictionary for storing player data to be processed later
# with an unknown number of entries - index stored in player_idx
player = {}
player_idx = 0

# create another dictionary for storing temp EVENT output data
full_output = {}
fo_idx = 0

# dictionary for game roster
game_roster = {}
gr_idx = 0

# for tracking baserunners
bases_after = '---'

# for tracking inherited baserunners: {'playerID': 'pitcherID'}
runners_inherited = {}

# debugging pyts
pyts_debug = []

# global list of gameplay column names
gameplay_cols = [
    'game_id',
    'home_team',
    'away_team',
    'win_id',
    'loss_id',
    'save_id',
    'play_id',
    'data_year',
    'gm_type',
    'inning',
    'half',
    'half_innings',
    'playerID',
    'playerTeamID',
    'pitch_count',
    'pitches',
    'play',
    'player_name',
    'team_id',
    'batting',
    'fielding',
    'fieldingTeamID',
    'pitcherID',
    'recorded_outs',
    'total_outs',
    'before_1B',
    'before_2B',
    'before_3B',
    'after_1B',
    'after_2B',
    'after_3B',
    'runs_scored',
    'total_scored',
    'RBI_awarded',
    'putouts',
    'assists',
    'errors',
    'num_inh',
    'inh_run1',
    'inh_run2',
    'inh_run3',
    'inh_earned1',
    'inh_earned2',
    'inh_earned3',
    'BT_dest',
    'R1_move',
    'R1_dest',
    'R1_inhp',
    'R2_move',
    'R2_dest',
    'R2_inhp',
    'R3_move',
    'R3_dest',
    'R3_inhp',
    'SB2_runner',
    'SB3_runner',
    'SBH_runner',
    'CS2_runner',
    'CS3_runner',
    'CSH_runner',
    'PO1_runner',
    'PO2_runner',
    'PO3_runner',
    'sub_appearance',
    'prev_pitcherID',
    'DP_flag',
    'GDP_flag',
    'TP_flag',
    'SH_flag',
    'SF_flag',
    'WP_flag',
    'PB_flag',
    'BINT_flag',
    'RINT_flag',
    'FINT_flag',
    'injured_strikeout',
    'pinch_pitcher',
    'new_half',
    'EVENT_CD',
    'home_P',
    'home_C',
    'home_1B',
    'home_2B',
    'home_3B',
    'home_SS',
    'home_LF',
    'home_CF',
    'home_RF',
    'home_DH',
    'away_P',
    'away_C',
    'away_1B',
    'away_2B',
    'away_3B',
    'away_SS',
    'away_LF',
    'away_CF',
    'away_RF',
    'away_DH',
    'home_b1',
    'home_b2',
    'home_b3',
    'home_b4',
    'home_b5',
    'home_b6',
    'home_b7',
    'home_b8',
    'home_b9',
    'away_b1',
    'away_b2',
    'away_b3',
    'away_b4',
    'away_b5',
    'away_b6',
    'away_b7',
    'away_b8',
    'away_b9'
]

gameplay_int = [
    'play_id',
    'data_year',
    'inning',
    'half',
    'recorded_outs',
    'total_outs',
    'runs_scored',
    'total_scored',
    'RBI_awarded',
    'num_inh',
    'BT_dest',
    'R1_dest',
    'R2_dest',
    'R3_dest',
    'EVENT_CD'
]

gameplay_bool = [
    'R1_move',
    'R2_move',
    'R3_move',
    'DP_flag',
    'GDP_flag',
    'TP_flag',
    'SH_flag',
    'SF_flag',
    'WP_flag',
    'PB_flag',
    'BINT_flag',
    'RINT_flag',
    'FINT_flag',
    'new_half'
]

gameplay_nullable = [
    'putouts',
    'assists',
    'errors',
    'inh_run1',
    'inh_run2',
    'inh_run3',
    'inh_earned1',
    'inh_earned2',
    'inh_earned3'
]

# home fielding
home_fielding = [
    'home_P',
    'home_C',
    'home_1B',
    'home_2B',
    'home_3B',
    'home_SS',
    'home_LF',
    'home_CF',
    'home_RF',
    'home_DH'
]

# away fielding
away_fielding = [
    'away_P',
    'away_C',
    'away_1B',
    'away_2B',
    'away_3B',
    'away_SS',
    'away_LF',
    'away_CF',
    'away_RF',
    'away_DH'
]

# home batting
home_batting = [
    'home_b1',
    'home_b2',
    'home_b3',
    'home_b4',
    'home_b5',
    'home_b6',
    'home_b7',
    'home_b8',
    'home_b9'
]

# away batting
away_batting = [
    'away_b1',
    'away_b2',
    'away_b3',
    'away_b4',
    'away_b5',
    'away_b6',
    'away_b7',
    'away_b8',
    'away_b9'
]

# lahman columns
# G AB R H 2B 3B HR RBI SB CS BB SO IBB HBP SH SF GIDP
for_lahman_bat = ['playerID', 'GP', 'AB', 'H', '2B', '3B', 'HR', 'RBI', 'R', 'BB', 'IBB', 'HBP', 'K',
                  'GDP', 'SH', 'SF', 'SB', 'CS']
lahman_bat = ['playerID', 'G', 'AB', 'H', '2B', '3B', 'HR', 'RBI', 'R', 'BB', 'IBB', 'HBP', 'SO',
              'GIDP', 'SH', 'SF', 'SB', 'CS']
# W L GS CG SHO SV IPouts H ER HR BB SO BAOpp ERA IBB WP HBP BK BFP GF R SH SF GIDP]
for_lahman_pitch = ['playerID', 'GP', 'GS', 'IPouts', 'H', 'R', 'ER', 'HR', 'BB', 'K', 'IBB', 'WP', 'HBP', 'BK', 'BF']
lahman_pitch = ['playerID', 'G', 'GS', 'IPouts', 'H', 'R', 'ER', 'HR', 'BB', 'SO', 'IBB', 'WP', 'HBP', 'BK', 'BFP']
# POS G GS InnOuts PO A E DP PB WP SB CS ZR
for_lahman_field = ['playerID', 'GP', 'GS', 'InnOuts', 'PO', 'A', 'E', 'DP']
lahman_field = ['playerID', 'G', 'GS', 'InnOuts', 'PO', 'A', 'E', 'DP']

# all lineup variables
all_lineup = []  # all_lineup = home_fielding assigns it as pointer!??!
all_lineup.extend(home_fielding)
all_lineup.extend(away_fielding)
all_lineup.extend(home_batting)
all_lineup.extend(away_batting)

# global dictionary for stat types
bat_stat_types = {
    'PID': 'player_id',
    'YEAR': 'data_year',
    'TEAM': 'team_name',
    'GP': 'games_played',
    'GS': 'games_started',
    'AB': 'at_bats',
    'PA': 'plate_appearances',
    'H': 'hits',
    'D': 'doubles',
    'T': 'triples',
    'HR': 'home_runs',
    'RBI': 'rbis',
    'R': 'runs_scored',
    'BB': 'walks',
    'IBB': 'intentional_walks',
    'K': 'strikeouts',
    'SB': 'stolen_bases',
    'CS': 'caught_stealing',
    'LOB': 'left_on_base',
    'RLSP': 'rlsp',
    'GDP': 'ground_dp',
    'HBP': 'hit_by_pitch',
    'SH': 'sac_hit',
    'SF': 'sac_fly',
    'PH': 'pinch_hit',
    'PR': 'pinch_run',
}

pitch_stat_types = {
    'PID': 'player_id',
    'YEAR': 'data_year',
    'TEAM': 'team_name',
    'GP': 'games_played',
    'GS': 'games_started',
    'IP': 'innings_pitched',
    'BF': 'batters_faced',
    'CG': 'completed_games',
    'SHO': 'shutouts',
    'W': 'wins',
    'L': 'losses',
    #  'HD': 'holds',
    'SV': 'saves',
    'R': 'runs_allowed',
    'ER': 'earned_runs',
    'H': 'hits_allowed',
    'HR': 'home_runs',
    'K': 'strikeouts',
    'BB': 'walks',
    'IBB': 'intentional_walks',
    'HBP': 'hit_batters',
    'POA': 'pick_off_attempts',
    'PO': 'pick_offs',
    'WP': 'wild_pitches',
    'PB': 'passed_balls',
    'BK': 'balks',
    'DI': 'defensive_indifference'
    # 'CI': 'catcher_interference',
    # 'PT': 'pitches_thrown',
    # 'ST': 'strikes_thrown',
    # 'BT': 'balls_thrown',
    # 'FL': 'foul_balls'
}

field_stat_types = {
    'PID': 'player_id',
    'YEAR': 'data_year',
    'TEAM': 'team_name',
    'GP': 'games_played',
    'GS': 'games_started',
    'Inn': 'innings_played',
    # 'BF': 'batters_faced',
    # 'W': 'wins',
    # 'L': 'losses',
    # 'HD': 'holds',
    # 'SV': 'saves',
    # 'R': 'runs_allowed',
    # 'ER': 'earned_runs',
    'A': 'assists',
    'PO': 'put_outs',
    'DP': 'double_plays',
    'TP': 'triple_plays',
    'E': 'errors'
}

bat_calc_stat_types = {
    ''
}

event_types = {
    'Unk': 0,  # Unknown Event
    'NOE': 1,  # No Event
    'Out': 2,  # Generic Out
    'K': 3,
    'SB': 4,
    'DI': 5,
    'CS': 6,
    'POE': 7,  # Pickoff Error
    'PO': 8,
    'WP': 9,
    'PB': 10,
    'BK': 11,
    'OA': 12,  # Other Advance
    'FLE': 13,
    'BB': 14,
    'IBB': 15,
    'HBP': 16,
    'CI': 17,  # Catcher Interference
    'ERR': 18,
    'FC': 19,
    'S1B': 20,
    'X2B': 21,
    'X3B': 22,
    'HR': 23,
    'MIS': 24,  # Missing Play
    'SUB': 25   # Substitution
}

# missing catcher interference
PA_events = [2, 3, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
AB_events = [2, 3, 18, 19, 20, 21, 22, 23]

base_running_types = {
    0: 'Out',
    1: '1st',
    2: '2nd',
    3: '3rd',
    4: 'Home',
    5: 'Home, unearned',
    6: 'Home, team unearned'
}

