# handling the Lahman Database
from . import db_setup as dbs
from . import automated_tests as aut
from . import global_variables as gv
from . import logging_functions as lgf
from . import common_functions as cf
from . import lahman_functions as lhf
import time as t
import sqlalchemy as sa
import pandas as pd


# revised lahman test using database instead of file
# test stats against lahman db
def test_lahman2(year, team, stat_type, testID):
    
    #  -----  SETTING UP VARIABLES  -----  #
    s_time = t.time()

    # log test case running
    lgf.log_test_running(testID)
    
    # connect to db
    conn = dbs.engine.connect()
    
    # get year ID
    yearID = cf.get_year(year)
    
    # get team ID
    teamID = cf.get_team(year, team)

    # get stat_typeID
    stat_typeID = cf.get_stat_type(stat_type)
    
    # get list of players from the roster
    query = 'SELECT p.retroID FROM rosters r ' \
            'LEFT JOIN teams_dim t ON r.teamID=t.Id ' \
            'LEFT JOIN players_dim p ON r.playerID=p.Id ' \
            'WHERE t.Id=?'
    results = conn.execute(query, teamID).fetchall()
    player_list = [Id for (Id,) in results]
    
    #  ----------------------------------  #
    
    #  -----  RETRIEVE LAHMAN TEST CASES  -----  #
    if stat_type == 'batting':
        
        # read from lahman database
        query = 'SELECT * FROM Batting WHERE yearID=?'
        LDF_ = lhf.read_lahman(query, year)
        lahman_col = gv.lahman_bat
        process_name = 'batting_test'
        
    elif stat_type == 'pitching':
        
        # read from lahman database
        query = 'SELECT * FROM Pitching WHERE yearID=?'
        LDF_ = lhf.read_lahman(query, year)
        lahman_col = gv.lahman_pitch
        process_name = 'pitching_test'
    
    # fielding
    else:
        # read from lahman database
        query = 'SELECT * FROM Fielding WHERE yearID=?'
        LDF_ = lhf.read_lahman(query, year)
        lahman_col = gv.lahman_field
        process_name = 'fielding_test'
        
    #  ----------------------------------------  #
        
    #  -----  GRAB TABLE AND RUN TEST CASE  -----  #
    test_case_count = 0
    for player in player_list:
    
        # get player Id from database
        playerID = cf.get_player_id(player)
        
        # get stat_testing table
        pdf = aut.get_stat_testing(yearID, player, stat_type, stat_typeID)
        ldf = LDF_[LDF_['playerID'] == player][lahman_col].reset_index(drop=True)
        pdf = pdf[lahman_col]  # follow the lahman column order

        # combine stints
        if len(ldf) > 1:
            ldf = ldf.groupby(['playerID'], as_index=False)[[c for c in lahman_col if c != 'playerID']].sum()
        if len(pdf) > 1:
            pdf = pdf.groupby(['playerID'], as_index=False)[[c for c in lahman_col if c != 'playerID']].sum()
        
        # if only ldf and no pdf -- most likely pitcher Games Played for batting
        if (len(pdf) == 0) & (len(ldf) == 1):
            check_zero = list(ldf[[c for c in lahman_col if c not in ['playerID', 'G']]].sum(axis=1))[0]
            if check_zero == 0:
                # lahman test contains only non-zero for 'G'
                continue
        
        # if not the case above. then increment test_case count
        test_case_count += 1
        
        # match GAMES PLAYED separately
        if (len(pdf) == 1) & (len(ldf) == 1):
            if pdf.loc[0, 'G'] != ldf.loc[0, 'G']:
                lgf.log_test_error(playerID, stat_typeID, testID, 'G', pdf.loc[0, 'G'], ldf.loc[0, 'G'], True)
            
                # omit GAMES PLAYED
                pdf.drop(['G'], axis=1, inplace=True)
                ldf.drop(['G'], axis=1, inplace=True)
                
        # check where the extra is if error thrown
        try:
            check = pdf.compare(ldf)
        except Exception:
            # log test error to db
            lgf.log_test_error(playerID, stat_typeID, testID, 'mismatch rows or columns', None, None, False)
        else:
            # check > 0 if there are mismatches
            if len(check) > 0:
                
                # grab the columns and values
                new_df = check.stack()
                problem_cols = list(new_df.columns)
                
                # Write a case for GS
                if len(problem_cols) == 1:
                    if problem_cols[0] == 'GS':
                        
                        # check what stat_type and what position the player is registered as
                        query = 'SELECT po.pos_abb FROM players_dim pl ' \
                                'LEFT JOIN positions_dim po ON pl.positionID=po.Id ' \
                                'LEFT JOIN rosters r ON pl.Id=r.playerID ' \
                                'WHERE pl.retroID=? AND r.yearID=?'
                        results = conn.execute(query, player, yearID).fetchall()
                        pos_abb = [pos for (pos,) in results][0]

                        # ignore errors for pitchers in batting and fielding entirely
                        if (pos_abb == 'P') & (stat_type in ['batting', 'fielding']):
                            pass

                        # other games started errors are position players, most likely just errors in addition (sum)
                        else:
                            prob_values = list(new_df.loc[0, 'GS'])
    
                            # log to db
                            lgf.log_test_error(playerID, stat_typeID, testID, 'GS', prob_values[0], prob_values[1], True)
                
                elif (year == 2019) & ((player == 'blacr001') | (player == 'blana002') | (player == 'bogax001') |
                                       (player == 'diaze005') | (player == 'dozih001') | (player == 'forsl001') |
                                       (player == 'iglej001') | (player == 'lopen001') | (player == 'schoj001') |
                                       (player == 'seguj002') | (player == 'solay001') | (player == 'starb001') |
                                       (player == 'sucrj001') | (player == 'taylb003') | (player == 'bourp001') |
                                       (player == 'albeh001') | (player == 'yelic001') | (player == 'mejia001')):
                    # lahman database error for Ray Black, only 1 putout tho given a total of 2 in baseball-reference
                    # Alex Blanino; Xander Bogaerts; Edwin Diaz; Logan Forsythe; Jose Iglesias tally assists incorrectly
                    # Jean Segura, Yangervis Solarte, Jesus Sucre, Beau Taylor tally assists incorrectly
                    # Hunter Dozier tally assists and errors incorrectly
                    # Nicky Lopez, Bubba Starling tally DP incorrectly
                    # Jonathan Schoop tally errors incorrectly
                    # Peter Bourjos, Adalberto Meija - fielding Innings
                    # Hanser Alberto, Christian Yelich - fielding GS -- will be caught in previous if statement
                    
                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
                        
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], True)
                
                elif (year == 2018) & (player == 'andet002'):
                    # lahman database error for:
                    # assists - Tyler Anderson
                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
    
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], True)
                
                elif (year == 2017) & ((player == 'bushm001') | (player == 'perem004') | (player == 'andet001') |
                                       (player == 'beckt001') | (player == 'frazt001') | (player == 'holld003') |
                                       (player == 'smitk002')):
                    # lahman database error for:
                    # ER - Matt Bush (07/19/2017) <-> Martin Perez (07/19/2017)
                    # Assists - Tim Anderson; Tim Beckham
                    # DP - Tim Anderson; Todd Frazier; Derek Holland; Kevan Smith

                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
    
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], True)
                
                elif (year == 2016) & ((player == 'belta001')):
                    # lahman database error for:
                    # Assists - Adrian Beltre
                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
    
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], True)
                
                elif (year == 2015) & ((player == 'alvaj003') | (player == 'ahmen001') | (player == 'forsl001') |
                                       (player == 'goldp001') | (player == 'kangj001') | (player == 'laroa001') |
                                       (player == 'norrd001') | (player == 'quack001') | (player == 'ramia003') |
                                       (player == 'aardd001')):
                    # lahman database error for:
                    # Runs Scored - Jose Alvarez (08/31/2015)
                    # Earned Runs - Jose Alvarez (08/31/2015)
                    # Assists - Nick Ahmed (+1); Logan Forsythe (-1); Jung Ho Kang (-1)
                    # Putouts - Paul Goldschmidt (-1); Adam LaRoche (-1); Daniel Norris (-1); Kevin Quackenbush (+1)
                    # - Alexei Ramirez (+1)
                    # Games Played (Batting) - David Aardsma (Lahman uses G from Pitching for Pitchers)

                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
    
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], True)
                
                else:
                    # add to list of errors
                    for prob in problem_cols:
                        prob_values = list(new_df.loc[0, prob])
                        
                        # log to db
                        lgf.log_test_error(playerID, stat_typeID, testID, prob, prob_values[0], prob_values[1], False)

    #  ------------------------------------------  #
    
    #  -----  LOG COMPLETION  -----  #
    # log completion
    lgf.log_process(process_name, yearID, teamID, s_time)
    
    # log test completion
    lgf.log_test_complete(testID, test_case_count)
    
    print("Test Completed", year, team)
    #  ----------------------------  #
    
    return True
