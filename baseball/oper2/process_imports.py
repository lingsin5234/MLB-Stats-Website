# this script processes the imported files
# using the extract_data.py file as reference
import pandas as pd
import re
import os
import sys
import time as t
from datetime import datetime
from . import game_converter as g
from . import play_processor as pp
from . import stat_collector as sc
from . import global_variables as gv
from . import date_time as dt
import sqlite3 as sql
from . import db_setup as dbs
from . import class_structure as cl
from . import error_logger as el
from . import logging_functions as lf
from . import common_functions as cf


# extract data for single team
def process_data_single_team(year, team):

    # make sure the variables are cleared to start off
    gv.full_output = {}
    gv.player = {}

    # check for import YEAR:
    if os.path.exists(gv.data_dir + '/' + str(year)):
        pass
    else:
        el.error_logger('gameplay_process', str(year) + ' needs to be imported first!', team, year, 'ALL')
        return False

    #  -----  OPEN AND READ EVENT FILES  -----  #
    
    try:
        dir_str = gv.data_dir + '/' + str(year)
        all_files = os.listdir(dir_str)

        # search for team
        file_nm = [f for f in all_files if str(year) + team in f]

        # start timer
        s_time = t.time()

        # get current file
        file_dir = dir_str + '/' + file_nm[0]
        f = open(file_dir, "r")
        f1 = f.readlines()

    except Exception as e:
        # accept any types of errors
        el.error_logger('gameplay_process', 'I/O Open Retrosheet Event File', team, year, 'ALL')
        return False
    
    #  ---------------------------------------------  #

    #  -----  MAIN PROCESSING FOR EVENT FILES  -----  #
    
    #  -----  COLLECT GAME IDS AND INFO  -----  #
    
    try:
        # collect id and group the games
        games = []
        game_ids = []
        game_id = ''
        game_info = []
        game_info_dict = dict()
        game_lines = []
        game_play = []
        game_start = []
        for line_item in f1:
            # Do this check first: if end of file OR (line item is ID and game_play > 0)
            if (line_item.index == f1[-1].index) | ((line_item[:2] == "id") & (len(game_play) > 0)):
                # populate game info, starts, plays
                game_lines.append(game_start.copy())
                game_lines.append(game_play.copy())
                games.append(game_lines.copy())
                game_lines.clear()
                game_start.clear()
                game_play.clear()  # Needed to clear this so it doesn't tack on for all remaining games!
                game_info_dict[game_id] = game_info.copy()
                game_info.clear()

            # separate this game with the next
            if line_item[:2] == "id":
                # append game id
                game_id = line_item[3:].replace('\n', '')
                game_ids.append(line_item)
                game_lines.append(line_item)
            elif line_item[:4] == "play" or line_item[:3] == "sub":
                game_play.append(line_item)
            elif line_item[:5] == "start":
                game_start.append(line_item)
            elif line_item[:3] == "com":
                # ignore the comments
                pass
            else:
                game_lines.append(line_item)
                if line_item[:4] == 'info':
                    game_info.append(line_item.replace('\n', ''))

        # close the read file
        f.close()
        f1 = None
        
        #  -------------------------------  #

        #  -----  LINEUP EXTRACTION  -----  #

        # extract all starting lineups by game (replaced each iteration in the variable)
        gv.game_roster = sc.game_tracker(games, year)
        games_roster = pd.DataFrame(gv.game_roster).transpose()
        # games_roster.to_csv('STARTERS.csv', sep=',', mode='a', index=False)

        # convert all games for 1 file
        gc_time = t.time()
        a_full_df = g.convert_games(games, games_roster, game_info_dict)

        # connect to database
        conn = dbs.engine.connect()

        #  -------------------------------  #
        
        #  -----  PLAY PROCESSOR LOOP  -----  #
        
        for e, each_game in enumerate(a_full_df):
            # game performance
            a1_time = t.time()

            # then run the processor
            this_game = pp.play_processor5(each_game, games_roster, team, year)

            # game performance
            a2_time = t.time()
            # reindex the DICTIONARY keys
            this_game = dict((int(k) + gv.fo_idx, value) for (k, value) in this_game.items())
            gv.fo_idx += len(this_game)

            # game performance
            a3_time = t.time()

            # store into full_output
            gv.full_output.update(this_game)

            # game performance
            a4_time = t.time()
            fgp = open('GAMEPLAY.LOG', mode='a')
            fgp.write('GAME #:' + str(e) + ' process: ' + str(dt.seconds_convert(a2_time - a1_time)) + '\n')
            fgp.write('GAME #:' + str(e) + ' reindex: ' + str(dt.seconds_convert(a3_time - a2_time)) + '\n')
            fgp.write('GAME #:' + str(e) + ' store: ' + str(dt.seconds_convert(a4_time - a3_time)) + '\n')
            fgp.write('GAME #:' + str(e) + ' TOTAL: ' + str(dt.seconds_convert(a4_time - a1_time)) + '\n')
            print('GAME #:', e, ' TOTAL: ', a4_time - a1_time)
            fgp.close()

            # output one game
            one_game = pd.DataFrame.from_dict(this_game, orient="index")
            one_game.to_csv('OUTPUT.csv', index=False, mode='a+', header=False)
            
            # output to gameplay_staging database
            one_game.to_sql('gameplay_staging', conn, if_exists='append', index=False)

        # indicator of what is completed
        e_time = t.time()
        print('COMPLETED: ', file_nm[0], ' - ', dt.seconds_convert(e_time - s_time))
        fgp = open('GAMEPLAY.LOG', mode='a')
        fgp.write('COMPLETED: ' + file_nm[0] + ' - ' + str(dt.seconds_convert(e_time - s_time)) + '\n')
        fgp.close()
        
        #  -----  COMPLETION LOG  -----  #
        # get year ID
        yearID = cf.get_year(year)

        # get team ID
        teamID = cf.get_team(year, team)

        # log completion
        lf.log_process('gameplay_process', yearID, teamID, s_time)
        
        #  ----------------------------  #

    except Exception as e:
        # accept any types of errors
        el.error_logger(e, 'gameplay_process: ' + str(e), team, year, 'ALL')
        return False

    #  ---------------------------------------------  #

    return True
