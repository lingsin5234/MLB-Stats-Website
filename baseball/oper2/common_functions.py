# list of commonly used functions
from . import db_setup as dbs


# turning None into blank strings
def xstr(s):
    if s is None:
        return ''
    return str(s)


# get year primary key from database
def get_year(year):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    results = conn.execute('SELECT Id FROM years_dim WHERE year=?', year).fetchall()
    
    # get first (and only) match
    yearID = [Id for (Id,) in results][0]
    
    return yearID


# get team primary key from database
def get_team(year, team):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    query = 'SELECT t.Id FROM teams_dim t LEFT JOIN years_dim y on t.yearID=y.Id WHERE y.year=? AND t.team_abb=?'
    results = conn.execute(query, year, team).fetchall()
    
    # get first (and only) match
    teamID = [Id for (Id,) in results][0]
    
    return teamID


# get stat type
def get_stat_type(stat_type):

    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    results = conn.execute('SELECT Id FROM stat_type_dim WHERE stat_type=?', stat_type)
    
    # get first (and only) match
    stat_typeID = [Id for (Id,) in results][0]
    
    return stat_typeID


# get test case primary key from database
def get_test_case(year, team, stat_type):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    query = 'SELECT tc.Id FROM tests_log tc ' \
            'LEFT JOIN years_dim y on tc.yearID=y.Id ' \
            'LEFT JOIN teams_dim t on tc.teamID=t.Id ' \
            'LEFT JOIN stat_type_dim st on tc.stat_typeID=st.Id ' \
            'WHERE y.year=? AND t.team_abb=? AND st.stat_type=?'  # AND tc.status=?'
    results = conn.execute(query, year, team, stat_type).fetchall()  # , 'Not Started').fetchall()
    
    # get first (and only) match
    testID = [Id for (Id,) in results][0]
    
    return testID


# get process Id
def get_process(process_name):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    result = conn.execute('SELECT Id FROM process_dim WHERE name=?', process_name)
    
    # get first (and only) match
    processID = [Id for (Id,) in result.fetchall()][0]
    
    return processID


# get game Id
def get_game(game_id):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    result = conn.execute('SELECT Id FROM games_dim WHERE game_id=?', game_id)
    
    # get first (andy only) match
    gameID = [Id for (Id,) in result.fetchall()][0]
    
    return gameID


# get player Id
def get_player_id(retroID):
    
    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    result = conn.execute('SELECT Id FROM players_dim WHERE retroID=?', retroID).fetchall()
    
    # get first (and only) match
    playerID = [pid for (pid,) in result][0]
    
    return playerID


# convert inning outs to innings format .0/.1/.2
def convert_innings(outs):
    
    total_innings = outs // 3  # whole number
    part_innings = outs % 3  # remainder
    IP = total_innings + part_innings
    
    return IP


# get list of teams -- to loop through jobs
def get_teams_list(year):

    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    results = conn.execute('SELECT team_abb FROM teams_dim WHERE yearID=?', get_year(year))
    
    # get full list out of the tuples
    teams = [tm for (tm,) in results]

    return teams


# get list of years imported -- for frontend app
def get_years_list():

    # connect to database
    conn = dbs.engine.connect()
    
    # execute query
    query = 'SELECT y.year FROM import_years iy LEFT JOIN years_dim y ON iy.yearID=y.Id WHERE iy.imported=?'
    results = conn.execute(query, True)
    
    # get full list out of hte tuples
    years = [(yr,yr) for (yr,) in results]
    
    return years


# get latest year imported / processed
def get_latest_year(process_name):
    
    # connect to db
    conn = dbs.engine.connect()
    
    # check which process to look at for retrieving latest year
    processID = get_process(process_name)
    
    # execute query
    query = 'SELECT yearID FROM process_log WHERE processID=? ORDER BY timestamp DESC LIMIT 1'
    results = conn.execute(query, processID)
    
    # get year Id
    yearID = [Id for (Id,) in results][0]
    
    return yearID


# get actual year based on Id
def get_actual_year(yearID):
    
    # connect to db
    conn = dbs.engine.connect()
    
    # execute query
    results = conn.execute('SELECT year FROM years_dim WHERE Id=?', yearID)
    
    # year value
    year = [yr for (yr,) in results][0]
    
    return year
