# adds dimension entries to database (dim tables)
from . import db_setup as dbs
from . import class_structure as cs


# add new process
def add_new_process(process_name, group_name, process_desc):
    
    # generate process details
    process_dict = [{
        'name': process_name,
        'group_name': group_name,
        'desc': process_desc
    }]
    
    # connect to database
    conn = dbs.engine.connect()
    
    # load to process_dim table
    conn.execute(cs.process.insert(), process_dict)

    return True


# add analyses_setup
add_new_process('analyses_setup', 'Load Statistics', 'append analyses tables with latest data')
