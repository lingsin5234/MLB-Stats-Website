# this file processes the roster_staging table for the players and rosters
from . import db_setup as dbs
from . import logging_functions as lf
from . import common_functions as cf
from . import error_logger as el
import pandas as pd
import time as t


def roster_load(year):
    
    s_time = t.time()

    # get year ID
    yearID = cf.get_year(year)
    
    # connect to database
    conn = dbs.engine.connect()
    
    # get staging rosters for year
    query = 'SELECT * FROM roster_staging WHERE yearID=?'
    results = conn.execute(query, yearID)
    stg_df = pd.DataFrame(results.fetchall(), columns=results.keys())

    # strip the trailing blank spaces, particularly for 'P ' and 'C '
    stg_df['pos_abb'] = stg_df['pos_abb'].apply(lambda x: x.strip())

    #  -----  transform and load to players table  -----  #
    
    # compress to ignore multiple stints
    players_df = stg_df[['retroID', 'last_name', 'first_name', 'bats', 'throws', 'pos_abb']].drop_duplicates()
    
    # check for existing retroIDs
    unique_ids = conn.execute('SELECT retroID FROM players_dim').fetchall()
    if len(unique_ids) > 0:
        unique_ids = [id for (id,) in unique_ids]
        players_df = players_df[~players_df['retroID'].isin(unique_ids)]

    #  -----  HANDLE REMAINING DUPLICATE RETRO IDS  -----  #
    
    # check for any remaining duplicate retroID -- e.g. 2018 andrj001 incorrectly listed as P for SEA
    check_dup = players_df['retroID'].value_counts()
    check_dup = check_dup.reset_index()
    dupe = check_dup[check_dup['retroID'] > 1]
    dupe_players = []
    if len(dupe) > 0:
        
        # retrieve all duplicate player entries
        unique_dupe = dupe['index']
        dupe_df = players_df[players_df['retroID'].isin(unique_dupe)]
        dupe_dict = dupe_df.to_dict(orient='records')

        # add as string; dupes will have 2 entries for each pair of dupes
        for idx, entry in enumerate(dupe_dict):
            el.error_logger(str(entry), 'roster_load', None, year, None)

        # keep the smaller of the indices for the dupe player
        for each_dupe in unique_dupe:
            indices = list(dupe_df[dupe_df['retroID'] == each_dupe].index)
            indices.remove(min(indices))
            players_df.drop(indices, axis=0, inplace=True)
            dupe_players.append(each_dupe)

    #  --------------------------------------------------  #

    #  -----  handle Positions  -----  #
    
    # get positions table
    results = conn.execute('SELECT Id, pos_abb FROM positions_dim')
    positions = results.fetchall()
    pos_df = pd.DataFrame(positions, columns=results.keys())

    # convert positions to position IDs
    for (Id, pos) in positions:
        players_df.loc[players_df['pos_abb'] == pos, 'pos_abb'] = Id
    players_df.rename(columns={'pos_abb': 'positionID'}, inplace=True)

    # write players_df to database
    players_df.to_sql('players_dim', conn, if_exists='append', index=False)

    #  ------------------------------  #

    #  -----  transform and load to rosters table  -----  #

    # need playerID from players table and teamID from teams table
    results = conn.execute('SELECT Id, retroID FROM players_dim')
    pid_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    results = conn.execute('SELECT Id, team_abb FROM teams_dim WHERE yearID=?', yearID)
    team_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # merge players, teams and positions to stg_df, then filter out only the foreign keys
    roster_df = stg_df.merge(pid_df, on="retroID").rename(columns={'Id_y': 'playerID'})
    roster_df = roster_df.merge(team_df, on="team_abb").rename(columns={'Id': 'teamID'})
    roster_df = roster_df.merge(pos_df, on="pos_abb").rename(columns={'Id': 'positionID'})
    
    # add the yearID; isolate only year, player, team and position Ids
    roster_df['yearID'] = yearID
    roster_df = roster_df[['yearID', 'playerID', 'teamID', 'positionID']]
    
    # write roster_df to database
    roster_df.to_sql('rosters', conn, if_exists='append', index=False)
    
    # clear roster staging for this year except for the dupe_players
    if len(dupe_players) > 0:
        dupe_players = '(' + ','.join("'{0}'".format(p) for p in dupe_players) + ')'
        del_query = 'DELETE FROM roster_staging WHERE yearID={} AND retroID NOT IN {}'.format(yearID, dupe_players)
    else:
        del_query = 'DELETE FROM roster_staging WHERE yearID={}'.format(yearID)
    conn.execute(del_query)
    
    #  -----  Completion Log  -----  #
    lf.log_process('roster_load', yearID, None, s_time)

    return True
