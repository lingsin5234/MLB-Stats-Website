# this script extracts the teams and players from the roster files
import os
import pandas as pd
import time as t
from datetime import datetime
from . import global_variables as gv
from . import error_logger as el
from . import db_setup as dbs
from . import class_structure as cl
from . import date_time as dt
from . import database_reader as dr
from . import logging_functions as lf
from . import common_functions as cf


# extract teams for single year
def extract_teams(year):

    s_time = t.time()

    # check for team inside import YEAR:
    file_str = gv.data_dir + '/' + str(year) + '/TEAM' + str(year)
    if os.path.exists(file_str):
        pass
    else:
        el.error_logger('NO TEAM FILE', str(year) + ' needs to be imported first!', year, '')
        return False

    # open the file and list all the teams
    try:
        f = open(file_str, 'r')
        f1 = f.readlines()
        f1 = [f.replace('\n', '').split(',') for f in f1]
        teams_list = []
        for tm in f1:
            teams_list.append(dict(zip(['team_id', 'league_id', 'city_name', 'name_of_team'], tm)))
        f.close()

    except Exception as e:
        # accept any types of errors
        el.error_logger(e, 'I/O Open Retrosheet Event File', '', year, '')
        return False

    # process to data frame
    try:
        # get year ID
        yearID = cf.get_year(year)
        
        # connect to database
        conn = dbs.engine.connect()
        
        # formulate the teams data frame
        teams = pd.DataFrame.from_dict(teams_list)
        teams['team_name'] = teams['city_name'] + ' ' + teams['name_of_team']
        teams['yearID'] = yearID
        teams = teams.rename(columns={'team_id': 'team_abb', 'league_id': 'league_abb'})
        teams = teams[['team_abb', 'team_name', 'league_abb', 'yearID']]
        
        # write the teams to sql database
        conn.execute(cl.teams.insert(), teams.to_dict('records'))

        # log completion
        lf.log_process('extract_teams', yearID, None, s_time)

    except Exception as e:
        # accept any types of errors
        el.error_logger(e, 'team_import', '', year, '')
        return False

    return True


# extract ALL players for single year
def extract_players(year):

    s_time = t.time()

    # check for import YEAR:
    dir_str = gv.data_dir + '/' + str(year)
    if os.path.exists(dir_str):
        pass
    else:
        el.error_logger('NO IMPORT YEAR', str(year) + ' needs to be imported first!', year, '')
        return False

    # get all the team rosters; the loop will ignore the all-stars
    all_files = os.listdir(dir_str)
    roster_files = [r for r in all_files if '.ROS' in r]

    # connect to db
    conn = dbs.engine.connect()

    # get year ID
    yearID = cf.get_year(year)

    # grab all team names from database
    teams = conn.execute('SELECT Id, team_abb FROM teams_dim WHERE yearID=?', yearID).fetchall()

    # untuple within the loop; for each team; get the players, then store
    player_fields = ['retroID', 'last_name', 'first_name', 'bats', 'throws', 'team_abb', 'pos_abb']
    for (tm_id, tm) in teams:
        # open the file and list all the players
        try:
            file_str = gv.data_dir + '/' + str(year) + '/' + tm + str(year) + '.ROS'
            f = open(file_str, 'r')
            f1 = f.readlines()
            f1 = [f.replace('\n', '').split(',') for f in f1]
            players_list = []
            for p in f1:
                players_list.append(dict(zip(player_fields, p)))
            f.close()

        except Exception as e:
            # accept any types of errors
            el.error_logger(e, 'I/O Open Retrosheet Event File', tm, year, '')
            return False

        # process to data frame
        try:
            players = pd.DataFrame.from_dict(players_list)
            players['yearID'] = yearID

            # write the players to sql database
            conn.execute(cl.roster_staging.insert(), players.to_dict('records'))

        except Exception as e:
            # accept any types of errors
            el.error_logger(e, 'player_import', tm, year, '')
            return False

    # log completion
    lf.log_process('extract_rosters', yearID, None, s_time)

    return True
