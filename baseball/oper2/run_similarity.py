# this file runs the similarity scores for a player
from . import db_setup as dbs
from . import run_careerTrajectory as rtraj
import pandas as pd


# similarity function - BATTING
def run_similarity_batting(retroID, num_sim):
    
    # get batting from analyses table
    conn = dbs.engine.connect()
    results = conn.execute('SELECT * FROM batting_3000')
    df = pd.DataFrame(results.fetchall(), columns=results.keys())

    # drop year and pos_abb
    df.drop(['year', 'pos_abb', 'Id'], axis=1, inplace=True)

    # check if retroID in the df
    if retroID not in list(df['retroID']):
        return None
    # print(df)

    names_list = ['retroID', 'first_name', 'last_name']
    df = df.groupby(names_list, as_index=False)[[c for c in df.columns if c not in names_list]].sum()
    
    # set a few more parameters for similarity scores
    df.rename(columns={'2B': 'X2B', '3B': 'X3B'}, inplace=True)
    df['AVG'] = df.eval('H / AB')
    df['SLG'] = df.eval('(H - X2B - X3B - HR + 2 * X2B + 3 * X3B + 4 * HR) / AB')
    
    # split to retroID player and the rest
    career_player = df[df['retroID'] == retroID].copy()
    
    # let career stats for retroID player to be duplicated for same number of rows as remainder
    career_player = career_player.append([career_player] * (len(df) - 1), ignore_index=True)
    
    # formula from Bill James -- first substract
    sim_df = career_player[[c for c in career_player.columns if c not in names_list]] - \
             df[[c for c in df.columns if c not in names_list]]
             
    sim_df = pd.concat([df[names_list], sim_df], axis=1)

    # then run eval
    sim_df['SIM_SCORE'] = sim_df.eval('1000 - abs(GP)/20 - abs(AB)/75 - abs(R)/10 - abs(H)/15 - abs(X2B)/5 - '
                                      'abs(X3B)/4 - abs(HR)/2 - abs(RBI)/10 - abs(BB)/25 - abs(K)/150 - abs(SB)/20 - '
                                      'abs(AVG)/0.01 - abs(SLG)/0.02 - abs(POS)')
    sim_df = sim_df.sort_values(['SIM_SCORE'], ascending=False)
    
    # keep the top "num_sim"
    sim_df = sim_df.head((num_sim + 1))  # +1 to keep the original player
    
    # limit the columns
    sim_df = sim_df[['retroID', 'first_name', 'last_name', 'SIM_SCORE']]
    # print(sim_df)
    
    return sim_df


# Position Value similarity score
def position_score(pos_abb):
    
    if pos_abb == 'C':
        return 240
    elif pos_abb == 'SS':
        return 168
    elif pos_abb == '2B':
        return 132
    elif pos_abb == '3B':
        return 84
    elif pos_abb == 'OF':
        return 48
    elif pos_abb == '1B':
        return 12
    else:
        return 0


# similarity function - PITCHING
def run_similarity_pitching(retroID, num_sim):
    
    # get pitching from analyses table
    conn = dbs.engine.connect()
    results = conn.execute('SELECT * FROM pitching_3000')
    df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # drop year and pos_abb
    df.drop(['year', 'pos_abb', 'Id'], axis=1, inplace=True)
    
    # check if retroID in the df
    if retroID not in list(df['retroID']):
        return None
    # print(df)
    
    names_list = ['retroID', 'first_name', 'last_name', 'throws']
    df = df.groupby(names_list, as_index=False)[[c for c in df.columns if c not in names_list]].sum()
    
    # set a few more parameters for similarity scores
    df['ERA'] = df.eval('ER * 9 / (IPouts / 3)')
    df['WinPer'] = df.eval('W / (W + L)')
    # missing -- Shutouts, Complete Games

    # --- RELIEF PITCHER ADJUSTMENT --- #
    df['RELIEF1'] = df.eval('(IPouts / 3 / GP) <= 4')
    df['RELIEF2'] = df.eval('(GP - GS) > GS')
    df['RELIEF'] = df['RELIEF1'].apply(lambda x: 1 if x else 0) + df['RELIEF1'].apply(lambda x: 1 if x else 0)
    df['RELIEF'] = df['RELIEF'].apply(lambda x: -25 if x > 0 else 0)
    df.drop(['RELIEF1', 'RELIEF2'], axis=1, inplace=True)
    # print(df)

    # --- HANDEDNESS ADJUSTMENT --- #
    df['HAND'] = df['throws'].apply(lambda x: -10 if x == 'L' else 0)  # lol double handed is 1?
    print(df[df['retroID'] == 'kersc001'])
    
    # split to retroID player and the rest
    career_player = df[df['retroID'] == retroID].copy()
    print(career_player)
    
    # let career stats for retroID player to be duplicated for same number of rows as remainder
    career_player = career_player.append([career_player] * (len(df) - 1), ignore_index=True)

    # formula from Bill James -- first substract
    sim_df = career_player[[c for c in career_player.columns if c not in names_list]] - \
             df[[c for c in df.columns if c not in names_list]]
    
    sim_df = pd.concat([df[names_list], sim_df], axis=1)
    
    # --- SIM SCORE ADJUSTMENTS -- #
    
    # Win percentage; if relief pitcher, the penalty is half; also only up to 100 points penalty
    sim_df['SIM_WinPer'] = sim_df.eval('abs(WinPer)/0.02')
    sim_df.loc[sim_df['RELIEF'] == 0, 'SIM_WinPer'] = sim_df.loc[sim_df['RELIEF'] == 0, 'SIM_WinPer'] / 2
    sim_df['SIM_WinPer'] = sim_df['SIM_WinPer'].apply(lambda x: 100 if x > 100 else x)
    
    # ERA up to 100 points penalty
    sim_df['SIM_ERA'] = sim_df.eval('abs(ERA)/0.2')
    sim_df['SIM_ERA'] = sim_df['SIM_ERA'].apply(lambda x: 100 if x > 100 else x)
    
    # WinPCT penalty cannot be more than 1.5 times larger than Wins + Losses penalty
    sim_df['Check_SIM_WinPer'] = sim_df.eval('SIM_WinPer > 1.5 * (abs(W) + abs(L)/2)')
    sim_df.loc[sim_df['Check_SIM_WinPer'], 'SIM_WinPer'] = sim_df[sim_df['Check_SIM_WinPer']].eval(
        '1.5 * (abs(W) + abs(L)/2)')
    
    # --- RUN SIMILARITY SCORE --- #
    sim_df['SIM_SCORE'] = sim_df.eval('1000 - abs(W) - abs(L)/2 - SIM_WinPer - SIM_ERA - abs(GP)/10 - '
                                      'abs(GS)/20 - abs(CG)/20 - abs(IPouts)/50 - abs(H)/50 - abs(K)/30 - '
                                      'abs(BB)/10 - abs(SHO)/5 - abs(SV)/3 - abs(HAND) - abs(RELIEF)')
    sim_df = sim_df.sort_values(['SIM_SCORE'], ascending=False)
    # print(sim_df)
    
    # keep the top "num_sim"
    sim_df = sim_df.head((num_sim + 1))  # +1 to keep the original player
    
    # limit the columns
    sim_df = sim_df[['retroID', 'first_name', 'last_name', 'SIM_SCORE']]
    # print(sim_df)
    
    return sim_df


# similarity_df = run_similarity_batting('suzui001', 9)
# print(similarity_df)
# retroIDs = similarity_df['retroID'].tolist()
# for pid in retroIDs:
#     rtraj.run_career_trajectory_batting(pid)
# run_similarity_pitching('hallr001', 9)
