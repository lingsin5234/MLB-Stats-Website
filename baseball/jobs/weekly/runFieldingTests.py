# run fielding test cases
from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import lahman_test as lt
from baseball.oper2 import error_logger as el


# this job run fielding test cases for each team in given year
class Job(WeeklyJob):
    help = "Run fielding test cases for each team in given year"

    def execute(self):
        
        # get the year ID recently processed (stat_collection)
        yearID = cf.get_latest_year('stat_collection')
    
        # get actual year
        year = cf.get_actual_year(yearID)
    
        # list of teams for that year
        teams = cf.get_teams_list(year)
    
        # loop through each team
        for team in teams:

            # get test case
            testID = cf.get_test_case(year, team, 'fielding')

            try:
                # run test case
                lt.test_lahman2(year, team, 'fielding', testID)
            
            except Exception as e:
                el.error_logger(str(e), 'fielding_test', team, year, 'fielding')
                
            else:
                # crontab log
                print("Fielding Test: {} {}... Success".format(year, team))
    
        return True
