from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import clear_staging as cg
from baseball.oper2 import logging_functions as lf
import time as t


# this job clears the staging tables for gameplay, batting, pitching and fielding.
class Job(WeeklyJob):
    help = "Clear staging tables for gameplay, batting, pitching, fielding"

    def execute(self):
    
        # start time
        start_time = t.time()
        
        # get the year ID recently processed (fielding_load)
        yearID = cf.get_latest_year('fielding_load')

        # clear gameplay
        status1 = cg.clear_gameplay_staging()
        
        # clear stats
        status2 = cg.clear_stats_staging()

        # these messages should be stored in /var/log/syslog
        # use cat syslog | grep CRON to view
        if status1 and status2:
            lf.log_process('clear_staging', yearID, None, start_time)
            print("Clear Staging... Success")
            return True
        else:
            print("Clear Staging... Failed")
            return False
