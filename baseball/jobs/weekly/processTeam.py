from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import process_imports as pi


# this job processes all teams for a specific year
# Once tested, this should run WEEKLY
class Job(WeeklyJob):
    help = "Processes event files for all teams in X year"

    def execute(self):
    
        # get the year ID recently processed (roster_load)
        yearID = cf.get_latest_year('roster_load')
        
        # get actual year
        year = cf.get_actual_year(yearID)

        # list of teams for that year
        teams = cf.get_teams_list(year)

        # loop through each team, recording status each time
        for team in teams:
            status = pi.process_data_single_team(year, team)

            # these messages should be stored in /var/log/syslog
            # use cat syslog | grep CRON to view
            if status:
                print("Process Team: {} {}... Success".format(year, team))
            else:
                print("Process Team: {} {}... Failed".format(year, team))
            
        return True
