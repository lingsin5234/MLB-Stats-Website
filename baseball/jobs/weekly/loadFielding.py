# run load fielding from staging
from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import stats_load as sl
from baseball.oper2 import error_logger as el
from baseball.oper2 import logging_functions as lf
import time as t


# this job load fielding from staging to main fielding table
class Job(WeeklyJob):
    help = "Load fielding from staging to main fielding table"
    
    def execute(self):
        
        # get the year ID recently processed (gameplay_load)
        yearID = cf.get_latest_year('gameplay_load')
        
        # get actual year
        year = cf.get_actual_year(yearID)
        
        t1 = t.time()
            
        try:
            # run stats load
            sl.stats_load(cf.get_year(year), 'fielding')
        
        except Exception as e:
            el.error_logger(str(e), 'fielding_load', None, year, 'fielding')
        
        else:
            lf.log_process('fielding_load', cf.get_year(year), None, t1)
            print("Fielding Stats Load: {}... Success".format(year))
        
        return True
