from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import db_setup as dbs
from baseball.oper2 import extract_teams_players as etp


# this job extracts Team and Player names from TEAM and .ROS files
class Job(WeeklyJob):
    help = "Extracts Team and Player names from TEAM and .ROS files"

    def execute(self):
    
        # get the year recently imported (marked as False)
        yearID = cf.get_latest_year('import_year')
    
        # get the actual year value
        year = cf.get_actual_year(yearID)

        # run the extract_player/teams
        status1 = etp.extract_teams(year)
        status2 = etp.extract_players(year)

        # these messages should be stored in /var/log/syslog
        # use cat syslog | grep CRON to view
        if status1 and status2:
            print("Extract Names: {}... Success".format(year))
            return True
        else:
            print("Extract Names: {}... Failed".format(year))
            return False
