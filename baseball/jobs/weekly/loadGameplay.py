# load games and gameplay from staging
from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import gameplay_load as gl


# this job load gameplay_staging to games and gameplay
class Job(WeeklyJob):
    help = "Load the gameplay from staging to main games and gameplay table"

    def execute(self):
        
        # get the year ID recently processed (fielding_test)
        yearID = cf.get_latest_year('fielding_test')
    
        # get actual year
        year = cf.get_actual_year(yearID)
    
        # list of teams for that year
        teams = cf.get_teams_list(year)
    
        # loop through each team, recording status each time
        for team in teams:
    
            # run gameplay load
            status = gl.gameplay_load(cf.get_year(year), year, cf.get_team(year, team), team)

            if status:
                print("Loading Gameplay: {} {}... Success".format(year, team))
            else:
                print("Loading Gameplay: {} {}... Failed".format(year, team))
    
        return True
