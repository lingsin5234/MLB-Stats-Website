# run load batting from staging
from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import stats_load as sl
from baseball.oper2 import error_logger as el
from baseball.oper2 import logging_functions as lf
import time as t


# this job load batting from staging to main batting table
class Job(WeeklyJob):
    help = "Load batting from staging to main batting table"

    def execute(self):
        
        # get the year ID recently processed (gameplay_load)
        yearID = cf.get_latest_year('gameplay_load')
    
        # get actual year
        year = cf.get_actual_year(yearID)
    
        t1 = t.time()
            
        try:
            # run stats load
            sl.stats_load(cf.get_year(year), 'batting')
        
        except Exception as e:
            el.error_logger(str(e), 'batting_load', None, year, 'batting')
            
        else:
            lf.log_process('batting_load', cf.get_year(year), None, t1)
            print("Batting Stats Load: {}... Success".format(year))
    
        return True
