from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import db_setup as dbs
from baseball.oper2 import import_retrosheet as ir
from baseball.oper2 import common_functions as cf
from baseball.oper2 import class_structure as cs


# this job imports a certain Year from the retrosheet.org
# Once tested, this should run WEEKLY
class Job(WeeklyJob):
    help = "Imports a year of files from Retrosheet.org"

    def execute(self):

        # get a list of the years not imported yet
        conn = dbs.engine.connect()
        query = 'SELECT y.year FROM import_years iy LEFT JOIN years_dim y ON iy.yearID=y.Id WHERE iy.imported=?'
        results = conn.execute(query, False)
        year = [yr for (yr,) in results][0]
        status = ir.import_data(year)
        
        # get year ID
        yearID = cf.get_year(year)

        # mark year imported
        conn = dbs.engine.connect()
        conn.execute('UPDATE import_years SET imported=? WHERE yearID=?', True, yearID)

        # once this is done, add a new entry for imported years
        results = conn.execute('SELECT yearID FROM import_years WHERE imported=?', True)
        completed_years = '(' + ','.join([str(Id) for (Id,) in results]) + ')'
        results = conn.execute('SELECT Id FROM years_dim WHERE Id NOT IN {} ORDER BY year DESC'.format(completed_years))
        latest_yearID = [Id for (Id,) in results][0]
        
        # insert into import_years as the next year to work on
        year_dict = {'yearID': latest_yearID, 'imported': False}
        conn.execute(cs.import_years.insert(), year_dict)

        # these messages should be stored in /var/log/syslog
        # use cat syslog | grep CRON to view
        if status:
            print("Import Year: {}... Success".format(year))
            return True
        else:
            print("Import Year: {}... Failed".format(year))
            return False
