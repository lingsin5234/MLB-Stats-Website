from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import analyses_setup as asp
from baseball.oper2 import logging_functions as lf
import time as t


# this job appends the analyses tables
class Job(WeeklyJob):
    help = "Append analyses tables with latest data"

    def execute(self):
    
        # start time
        start_time = t.time()
        
        # get the year ID recently processed (clear_staging)
        yearID = cf.get_latest_year('clear_staging')

        # batting
        status1 = asp.batting_setup_pa3000()
        
        # pitching
        status2 = asp.pitching_setup_ip3000()

        # these messages should be stored in /var/log/syslog
        # use cat syslog | grep CRON to view
        if status1 and status2:
            lf.log_process('analyses_setup', yearID, None, start_time)
            print("Append Analyses Tables... Success")
            return True
        else:
            print("Append Analyses Tables... Failed")
            return False
