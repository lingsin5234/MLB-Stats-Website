from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import roster_load as rst


# this job loads the rosters and players from the roster_staging table
class Job(WeeklyJob):
    help = "Loads rosters and players from the roster_staging table"

    def execute(self):
    
        # get the year recently processed (extract_rosters)
        yearID = cf.get_latest_year('extract_rosters')
        
        # get the actual year value
        year = cf.get_actual_year(yearID)

        # load players, rosters
        status = rst.roster_load(year)
        
        # these messages should be stored in /var/log/syslog
        # use cat syslog | grep CRON to view
        if status:
            print("Load Rosters: {}... Success".format(year))
            return True
        else:
            print("Load Rosters: {}... Failed".format(year))
            return False
