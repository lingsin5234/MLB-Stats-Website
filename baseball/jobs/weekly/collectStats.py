from django_extensions.management.jobs import WeeklyJob
from baseball.oper2 import common_functions as cf
from baseball.oper2 import db_setup as dbs
from baseball.oper2 import stat_collector as sc
from baseball.oper2 import stats_load as ss
from baseball.oper2 import error_logger as el
from baseball.oper2 import logging_functions as lf
import time as t
import pandas as pd


# this job collect stats for all teams in given year
class Job(WeeklyJob):
    help = "Collect Stats for all teams in given year"

    def execute(self):
        
        # get the year ID recently processed (gameplay_process)
        yearID = cf.get_latest_year('gameplay_process')
    
        # get actual year
        year = cf.get_actual_year(yearID)

        # list of teams for that year
        teams = cf.get_teams_list(year)
        
        # connect to db
        conn = dbs.engine.connect()
    
        # loop through each team
        for team in teams:
            
            # start time
            start_time = t.time()
    
            try:
                # get gameplay
                results = conn.execute('SELECT * FROM gameplay_staging WHERE data_year=? AND home_team=?', year, team)
                gameplay = pd.DataFrame(results.fetchall(), columns=results.keys())
        
                #  -- run stat collector, stats staging and tests -- #
                # batting
                stat_output = sc.stat_collector2(gameplay, 'batting')
                ss.stats_staging(stat_output, 'batting')
        
                # pitching
                stat_output = sc.stat_collector2(gameplay, 'pitching')
                ss.stats_staging(stat_output, 'pitching')
        
                # fielding
                stat_output = sc.stat_collector2(gameplay, 'fielding')
                ss.stats_staging(stat_output, 'fielding')
    
                # add test case logs for each stat_type
                lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'batting')
                lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'pitching')
                lf.log_test_case(cf.get_year(year), cf.get_team(year, team), 'fielding')
                
            except Exception as e:
                el.error_logger(str(e), 'stat_collection', team, year, None)
                print("Stat Collection: {} {}... Failed".format(year, team))
            
            else:
                # log completion
                lf.log_process('stat_collection', cf.get_year(year), cf.get_team(year, team), start_time)
                print("Stat Collection: {} {}... Success".format(year, team))

        return True
