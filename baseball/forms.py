from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from crispy_forms.bootstrap import InlineRadios
import os
import numpy as np
from .models import JobRequirements, FavPlayers
# from .oper import check_latest_imports as chk
from .oper2 import common_functions as cf2


class GetYear(forms.ModelForm):

    year = cf2.get_years_list()

    def __init__(self, year_choices, *args, **kwargs):
        super(GetYear, self).__init__(*args, **kwargs)
        self.fields['year'].choices = year_choices

    class Meta:
        model = JobRequirements
        fields = ['year', 'form_type']
        widgets = {'form_type': forms.HiddenInput()}


class ProcessTeam(forms.ModelForm):

    year = [2019]
    team = cf2.get_teams_list(2019)

    def __init__(self, year_choices, team_choices, *args, **kwargs):
        super(ProcessTeam, self).__init__(*args, **kwargs)
        self.fields['year'].choices = year_choices
        self.fields['team'].choices = team_choices

    class Meta:
        model = JobRequirements
        fields = ['year', 'team', 'form_type']
        widgets = {'form_type': forms.HiddenInput()}


class GenerateStats(forms.ModelForm):
    
    year = [2019]
    team = cf2.get_teams_list(2019)

    def __init__(self, year_choices, team_choices, *args, **kwargs):
        super(GenerateStats, self).__init__(*args, **kwargs)
        self.fields['year'].choices = year_choices
        self.fields['team'].choices = team_choices

    class Meta:
        model = JobRequirements
        fields = ['year', 'team', 'form_type']
        widgets = {'form_type': forms.HiddenInput()}


class ViewStats(forms.ModelForm):

    def __init__(self, year_choices, team_choices, *args, **kwargs):
        super(ViewStats, self).__init__(*args, **kwargs)
        self.fields['year'].choices = year_choices
        self.fields['team'].choices = team_choices

    class Meta:
        model = JobRequirements
        fields = ['year', 'team', 'form_type']
        widgets = {'form_type': forms.HiddenInput()}


#  -----  FAV PLAYERS DASHBOARD  -----  #
class GetFavPlayer(forms.ModelForm):
    
    def __init__(self, players, *args, **kwargs):
        super(GetFavPlayer, self).__init__(*args, **kwargs)
        self.fields['name'].choices = [(p['playerID'] + ';' + p['stat_type'], p['name']) for p in players]
        
    class Meta:
        model = FavPlayers
        fields = ['name']

