# libraries
from . import stat_collector as sc
from . import global_variables as gv
from . import fielding_oper as fo
import re

# constant
max_pitch = 30  # maximum pitches thrown in at-bat, in case it ever gets beaten!


# assign the pitcher
def assign_pitcher(lineup, this_line):

    # check which team is batting
    if this_line['team_id'] == '0':
        pitch_team = '1'
    else:
        pitch_team = '0'

    # filter the corresponding pitcher_id
    pitch_filter = (lineup.team_id == pitch_team) & (lineup.fielding == '1')
    pitch_index = lineup.index[pitch_filter]
    if bool(len(pitch_index) == 0) & bool(this_line['play'] == 'NP'):
        pitcher_id = None
    elif bool(len(pitch_index) > 0):
        pitcher_id = lineup.at[pitch_index[0], 'player_id']
    else:
        print(lineup)
        print(this_line)
        print('Error with Assign Pitcher where the play is not "NP".')
        exit()

    return pitcher_id


# pitching stats collector
def pitch_collector(hid, lineup, the_line, stat_types):

    # get game info
    game_info = pitch_get_game_info(lineup, the_line)
    team_name, game_id, this_half, actual_play, \
        num_outs, bases_taken, stat_team = game_info
    pitch_count = the_line['pitch_count']
    data_year = the_line['data_year']

    # for each stat_type, call stat_appender
    for s_type in stat_types:
        sc.stat_appender(hid, team_name, data_year, game_id, this_half, s_type, 1, actual_play, pitch_count,
                         num_outs, bases_taken, stat_team, 'pitching')

        # every time there is a Strikeout -- K, add Inn to the entire lineup; only if batter is out
        if s_type == 'K' and not(bool(re.search(r'B-[123H]', the_line['play']))) and \
                not(bool(re.search(r'BX[123H](\(UR\))?(\(NR\))?([1-9]?E[1-9])', the_line['play']))):
            fo.fielder_innings(lineup, the_line, team_name, data_year, game_id, this_half, actual_play, pitch_count,
                               num_outs, bases_taken, stat_team, 'Inn')

        # every time there is a Batter Faced -- BF, add BF for entire lineup for fielding stat
        if s_type == 'BF':
            fo.fielder_innings(lineup, the_line, team_name, data_year, game_id, this_half, actual_play, pitch_count,
                               num_outs, bases_taken, stat_team, 'BF')

    return True


# game info for pitcher collectors
def pitch_get_game_info(lineup, the_line):

    # game info values
    game_id = the_line['game_id']
    this_half = the_line['half_innings']
    actual_play = the_line['play']

    # get vis_team and home_team
    vis_team = lineup.loc[0, 'team_name']
    home_team = lineup.loc[10, 'team_name']

    # which team? find out in the_line -- THIS IS OPPOSITE to stat_collector!
    if the_line['half'] == '0':
        team_name = home_team  # home is pitching
        stat_team = 'HOME'
    else:
        team_name = vis_team  # visitor is pitching
        stat_team = 'VISIT'

    # gather more information about the play
    num_outs = the_line['outs']

    # base runners
    bases_taken = []
    if the_line['before_1B']:
        bases_taken.append('1')
    else:
        bases_taken.append('-')
    if the_line['before_2B']:
        bases_taken.append('2')
    else:
        bases_taken.append('-')
    if the_line['before_3B']:
        bases_taken.append('3')
    else:
        bases_taken.append('-')
    bases_taken = ''.join(map(str, bases_taken))

    return [team_name, game_id, this_half, actual_play, num_outs, bases_taken, stat_team]


# pitch count collector
def pitch_count_collector(pitcherID, playerID, lineup, the_line):

    # get game info
    game_info = pitch_get_game_info(lineup, the_line)
    team_name, game_id, this_half, actual_play, \
        num_outs, bases_taken, stat_team = game_info
    pitch_count = the_line['pitch_count']
    data_year = the_line['data_year']

    # the pitches
    pc = the_line['pitches']

    # pitches thrown - ignore N for no pitch
    thrown = re.subn('[A-MO-Z]', '', pc, max_pitch)[1]

    # strikes thrown
    strikes = re.subn('[CFKLMOQRSTXY]', '', pc, max_pitch)[1]

    # foul balls
    fouls = re.subn('[FLORT]', '', pc, max_pitch)[1]

    # balls thrown
    balls = re.subn('[BIPV]', '', pc, max_pitch)[1]

    # unknown or missed pitch - U

    # call stat_appender for PT, ST, FL, BT
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'PT', thrown, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'ST', strikes, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'FL', fouls, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')
    sc.stat_appender(pitcherID, team_name, data_year, game_id, this_half, 'BT', balls, actual_play, pitch_count,
                     num_outs, bases_taken, stat_team, 'pitching')

    return True


# PITCHER SUBSTITUTION: assign the inherited baserunners: pid is the PREVIOUS pitcherID
def assign_baserunners(this_line, pid):

    br_num = len(gv.bases_after)
    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]

    # if there are baserunners, then they need to be inherited!
    if br_num > 0:
        for r in runners:
            if r is not None:
                if r in gv.runners_inherited:
                    # if runner is already inherited by a pitcher, then pass
                    pass
                else:
                    # otherwise, add the playerID: pitcherID key-pair
                    gv.runners_inherited[r] = pid

    return True


# AFTER A PLAY: evaluate the baserunners after a play for removal
def eval_baserunners(this_line):

    ri_num = len(gv.runners_inherited)
    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]

    # check inherited to see if they are still on base
    remove_keys = []
    if ri_num > 0:
        for i, key in enumerate(gv.runners_inherited):
            if key not in runners:
                # if not a baserunner anymore, pop it after
                remove_keys.append(key)

    for r in remove_keys:
        gv.runners_inherited.pop(r)

    return True


# PINCH-RUNNER: adjust inherited runner for pinch-runner: br_id is the PREVIOUS baserunner playerID
def pinch_runner_inherited(this_line):

    runners = [this_line['after_1B'], this_line['after_2B'], this_line['after_3B']]
    prev_runners = [this_line['before_1B'], this_line['before_2B'], this_line['before_3B']]

    # use SET difference set(b) - set(a), for set(b) values different in set(a)
    runner_list = list(set(prev_runners) - set(runners))
    br_id = ''
    # print(runner_list)
    if len(runner_list) > 0:
        br_id = runner_list[0]

    # if previous runner is on the runners_inherited list
    # then the pitching substitution was made while that runner was on base
    if br_id in gv.runners_inherited:
        # pitcherID of previous runner
        pid = gv.runners_inherited[br_id]

        # add new inherited runner (the pinch-runner) with pitcher assigned previously
        gv.runners_inherited[this_line['playerID']] = pid

        # pop the previous runner
        gv.runners_inherited.pop(br_id)

    # otherwise, the pitcher has not changed, so no need to add to runners_inherited

    return True


# ON RUNNER SCORING: check if runner is inherited, and assign pitcherID accordingly
def inherit_runner_scored(this_line, runner_id):

    # if runner id is part of inherited runners, return the pitcher id
    if runner_id in gv.runners_inherited:
        return gv.runners_inherited[runner_id]
    # otherwise, return the current pitcher id
    else:
        return this_line['pitcherID']
