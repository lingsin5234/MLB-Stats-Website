# write the pujols and trea turner 2018 stat checks for automated testing
from . import db_setup as dbs
from . import global_variables as gv
from . import date_time as dt
from . import error_logger as el
from sqlalchemy.exc import IntegrityError
import os, re
import pandas as pd
import numpy as np
import time as t
import json


# import pujols and turner 2018 stats
def import_test_cases(yr):

    conn = dbs.engine.connect()
    conn.fast_executemany = True
    file_dir = 'baseball/import/tests/' + str(yr)
    try:
        files = os.listdir(file_dir)
        for f in files:
            df = pd.read_csv(os.path.join(file_dir, f))
            stat_category = re.sub(r'\.csv', '', re.sub(r'.*/', '', f))
            player_id = re.sub(r'.*_', '', stat_category)
            stat_category = re.sub(r'_.*', '', stat_category)
            if stat_category == 'batting':
                df = df.rename(columns={'2B': 'D', '3B': 'T'})  # cannot start with number
            print(player_id, stat_category)
            # write test case
            test_case = pd.DataFrame({
                'player_id': player_id,
                'data_year': yr,
                'stat_category': stat_category
            }, index=[0])
            test_case.to_sql('test_cases', conn, if_exists='append', index=False)

            # get the test case Id
            query = 'SELECT Id FROM test_cases WHERE player_id=? AND data_year=? AND stat_category=?'
            results = conn.execute(query, player_id, yr, stat_category).fetchall()
            print([r for (r,) in results])
            test_id = [r for (r,) in results]

            # write test data for specific test_id
            df['test_case'] = test_id[0]
            df = df.fillna(0)
            db_name = 'test_' + stat_category
            df.to_sql(db_name, conn, if_exists='append', index=False)
    except IntegrityError as e:
        # print(type(e))
        ex = str(e)
        if 'UNIQUE constraint' in ex:
            print('Unique Constraint error', ex)
        else:
            print('Other SQL error', ex)
            return False
    except Exception as e:
        print('Import Test Cases Failed.', e)
        return False

    '''
    # read from database
    query = 'SELECT * FROM test_batting'
    results = conn.execute(query).fetchall()
    batting = {}
    idx = 0
    for x in results:
        batting[idx] = dict(x)
        idx += 1
    print(pd.DataFrame.from_dict(batting).transpose())

    query = 'SELECT * FROM test_fielding'
    results = conn.execute(query).fetchall()
    fielding = {}
    idx = 0
    for x in results:
        fielding[idx] = dict(x)
        idx += 1
    print(pd.DataFrame.from_dict(fielding).transpose())
    '''
    conn.close()

    return True


# import_test_cases()


# run test cases
def run_test_cases(stat_category, yr):

    # connect to database
    conn = dbs.engine.connect()
    conn.fast_executemany = True
    db_name = 'test_' + stat_category

    # get test cases
    query = 'SELECT ts.*, tc.player_id, tc.data_year FROM ' + db_name + ' ts JOIN test_cases tc ' + \
            'ON ts.test_case = tc.Id WHERE tc.data_year=?'
    a = t.time()
    results = conn.execute(query, yr).fetchall()
    # print('Fetch All:', dt.seconds_convert(t.time() - a))
    b = t.time()
    columns = conn.execute(query, yr)
    # print('Columns:', dt.seconds_convert(t.time() - b))
    # print(columns.keys())
    # print(len(results))
    conn.close()

    # if no results, quit
    if len(results) == 0:
        return False

    # test case data frame
    tc = pd.DataFrame(results)
    tc.columns = columns.keys()

    # take out any all 0 rows
    # tc = tc[not(tc.sum(axis=1) == 0), ]
    # print(sum(tc.sum(axis=1) == 0))
    # print(tc)

    # get category results
    player_id = tc['player_id'].unique().tolist()
    data_year = tc['data_year'].unique().tolist()
    for pid in player_id:
        conn = dbs.engine.connect()
        conn.fast_executemany = True
        query = 'SELECT * FROM raw_player_stats WHERE player_id=? AND bat_pitch=? AND data_year=?'
        results = conn.execute(query, pid, stat_category, data_year[0]).fetchall()
        columns = conn.execute(query, pid, stat_category, data_year[0])
        stats = pd.DataFrame(results)
        if len(stats) > 0:
            # print(stats)
            # print(columns.keys())
            stats.columns = columns.keys()
            conn.close()
            # print(stats)

            # tally by date
            stats['GameDate'] = stats['game_id'].replace(r'^[A-Z]{3}([0-9]{4})([0-9]{2})([0-9]{2})(.*)',
                                                         '\\1-\\2-\\3(\\4)', regex=True)
            stats['GameDate'] = stats['GameDate'].replace(r'\(0\)', '', regex=True)  # for double headers
            agg_stats = stats[['GameDate', 'stat_type', 'stat_value']].groupby(['GameDate', 'stat_type'])\
                .agg({'stat_value': 'sum'}).reset_index()
            agg_stats = agg_stats.set_index(['GameDate']).pivot(columns='stat_type')['stat_value']
            agg_stats = agg_stats.fillna(0)
            # print(len(agg_stats), len(tc[tc['player_id'] == pid]))

            # adjust innings count - IP for pitching, Inn for fielding
            if 'IP' in agg_stats:
                floor_innings = agg_stats['IP'] // 3
                modulus_innings = agg_stats['IP'] % 3 / 10
                agg_stats['IP'] = floor_innings + modulus_innings
            if 'Inn' in agg_stats:
                floor_innings = agg_stats['Inn'] // 3
                modulus_innings = agg_stats['Inn'] % 3 / 10
                agg_stats['Inn'] = floor_innings + modulus_innings
                # print(agg_stats)
            # print(agg_stats)
            '''
            temp = tc[tc['player_id'] == pid].set_index(['GameDate'])
            temp = temp.loc[
                (temp.index == '2019-08-16') | (temp.index == '2019-08-28') | (temp.index == '2019-09-09') |
                (temp.index == '2019-09-15') | (temp.index == '2019-09-27'), ]
            '''
            # print(temp)

            # compare with test case
            print(compare_data(agg_stats, tc[tc['player_id'] == pid].set_index(['GameDate']), pid, yr, stat_category))

    # df = df.rename(columns=gv.bat_stat_types)

    return True


# compare columns for data frames
def compare_data(df1, df2, pid, yr, stat_category):

    # first check if lengths are the same
    # print(len(df1), len(df2))
    if len(df1) != len(df2):

        results = {
            'player': pid,
            'year': yr,
            'category': stat_category,
            'Status': 'Failed. DataFrame Lengths Different',
            'columns': 'agg:tc ' + str(len(df1)) + ':' + str(len(df2))
        }
        # write to log
        fgp = open('TEST_CASES.LOG', mode='a')
        fgp.write('Automated Testing for ' + pid + ', ' + str(yr) + '\n' + json.dumps(results, indent=4) + '\n')
        fgp.close()

        try:
            # write to database
            error = 'Failed. DataFrame Lengths Different - agg:tc ' + str(len(df1)) + ':' + str(len(df2))
            el.processing_errors(error, 'stat_test_cases ' + stat_category, '---', str(yr), '', '')
        except Exception as e:
            print(e)

        return results

    # get comparable columns
    col1 = df1.columns.tolist()
    col2 = df2.columns.tolist()
    # print(col1)
    # print(col2)
    # ignores index: GameDate
    col_comp = set(col1) & set(col2)
    # print(col_comp)

    # compare the columns
    fail_flag = False
    failed = []
    for c in col_comp:
        df1[c] = df1[c].apply(float)
        # print(df1[c], df2[c])
        comp_bool = df1[c].where(df1[c] == df2[c]).notna()
        if comp_bool.all():
            # success!
            pass
        else:
            # print(c, '\n', comp_bool)
            idx = comp_bool[comp_bool == False].index
            df_comp = pd.concat([df1.loc[idx, c], df2.loc[idx, c]], axis=1)
            df_comp.columns = ['raw_' + c, 'test_case_' + c]
            fail_flag = True
            failed.append(df_comp.to_dict())
            print(df_comp.to_dict())

    if not fail_flag:
        results = {
            'player': pid,
            'year': yr,
            'category': stat_category,
            'Status': 'Success',
            'columns': '100% match'
        }
    else:
        results = {
            'player': pid,
            'year': yr,
            'category': stat_category,
            'Status': 'Failed. Column Mismatches',
            'columns': failed
        }

        try:
            # write to database
            error = 'Failed. Column Mismatches - columns ' + ','.join(failed)
            el.processing_errors(error, 'stat_test_cases ' + stat_category, '---', str(yr), '', '')
        except Exception as e:
            print(e)

    # write to log
    fgp = open('TEST_CASES.LOG', mode='a')
    fgp.write('Automated Testing for ' + pid + ', ' + str(yr) + '\n' + json.dumps(results, indent=4) + '\n')
    fgp.close()

    return results


# run_test_cases('batting')
