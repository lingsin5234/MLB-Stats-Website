from django.shortcuts import render
from django.forms.models import model_to_dict
import json
from flask import jsonify
from django.core.serializers.json import DjangoJSONEncoder
from .models import StatCollect, JobRequirements
from djangoapps.utils import get_this_template
from .forms import GetYear, ProcessTeam, GenerateStats, ViewStats, GetFavPlayer
#  ---  OPER 2  ---  #
from .oper2 import db_setup as dbs2
from .oper2 import class_structure as cs2
from .oper2 import global_variables as gv2
from .oper2 import common_functions as cf2
from .oper2 import run_analysis as ra2
from .oper2 import run_similarity as rsim
from .oper2 import run_careerTrajectory as rtraj
#  ----------------  #
import pandas as pd
'''
API Section
'''
from rest_framework import views
from rest_framework.response import Response
from .serializers import BatStatSerializer2, PitchStatSerializer2, FieldStatSerializer2
from .serializers import FavBattingSerializer, FavPitchingSerializer


# API View for getting Stats Results table -- VERSION 2 using oper2
class StatsResults2(views.APIView):

    # get request
    def get(self, request):
        
        # get cols and pop the columns not being displayed
        if request.GET['stats_table'] == 'batting':
            viewer_col = gv2.bat_stat_types.copy()
        elif request.GET['stats_table'] == 'pitching':
            viewer_col = gv2.pitch_stat_types.copy()
        elif request.GET['stats_table'] == 'fielding':
            viewer_col = gv2.field_stat_types.copy()
        else:
            viewer_col = gv2.bat_stat_types.copy()  # default batting for now
        viewer_col.pop('PID')
        viewer_col.pop('YEAR')

        # get the keys and values, then add in name to the viewer_col
        query_col = ['player_nm'] + list(viewer_col.values())
        post_col_keys = ['NAME'] + list(viewer_col.keys())
        viewer_col['NAME'] = 'player_nm'  # this way the order for will remain the same
        # print(request.GET['stats_table'], request.GET['year'])
        # read from database; || is concatenate in sqlite!
        query = "SELECT s.*, (p.first_name || ' ' || p.last_name) as player_nm FROM " + \
                str(request.GET['stats_table']) + " s " \
                "LEFT JOIN rosters r ON s.playerID=r.playerID " \
                "LEFT JOIN players_dim p ON r.playerID=p.Id " \
                "LEFT JOIN teams_dim t ON r.teamID=t.Id AND s.teamID=t.Id " \
                "LEFT JOIN years_dim y ON s.yearID=y.Id AND t.yearID=y.Id " \
                "WHERE t.team_abb=? AND y.year=?"
        # print(query)

        # get data frame
        conn = dbs2.engine.connect()
        results = conn.execute(query, str(request.GET['team']), int(str(request.GET['year'])))
        df = pd.DataFrame(results.fetchall(), columns=results.keys())
        
        # use correct columns
        df.drop(['Id', 'playerID', 'yearID'], axis=1, inplace=True)
        df.rename(columns={'player_nm': 'NAME', '2B': 'D', '3B': 'T'}, inplace=True)
        curr_col = [col for col in df.columns if col != 'NAME']
        new_col = ['NAME', 'TEAM']
        new_col.extend(curr_col)
        df['TEAM'] = str(request.GET['team'])
        df = df[new_col]
        df.fillna(0, inplace=True)
        
        # convert innings here
        if request.GET['stats_table'] == 'pitching':
            df['IP'] = df['IPouts'].apply(cf2.convert_innings)
            df.drop(['IPouts'], axis=1, inplace=True)
            
        elif request.GET['stats_table'] == 'fielding':
            df['Inn'] = df['InnOuts'].apply(cf2.convert_innings)
            df.drop(['InnOuts'], axis=1, inplace=True)
        
        # convert to dictionary
        results = df.to_dict(orient="records")

        # run thru the corresponding serializer
        if request.GET['stats_table'] == 'batting':
            data_output = BatStatSerializer2(results, many=True).data
        elif request.GET['stats_table'] == 'pitching':
            data_output = PitchStatSerializer2(results, many=True).data
        else:
            data_output = FieldStatSerializer2(results, many=True).data

        return Response(data_output)


# API View for retrieving Fav Players data
class FavPlayers(views.APIView):
    
    # get request
    def get(self, request):
        
        # get stats
        stat_type = request.GET['stat_type']
        playerID = request.GET['playerID']
        df = ra2.get_circleBar_stats(playerID, stat_type)
        
        # --- CAREER STATS --- #
        # get int columns
        int_cols = list(df.select_dtypes(include=['int64']))
        int_cols.extend(['first_name'])  # keep name for aggregate use
        int_cols.remove('year')

        # aggregate
        agg_df = df[int_cols].copy()
        agg_df = agg_df.groupby(['first_name'], as_index=False)[
            [col for col in int_cols if col != 'first_name']].sum()
        agg_dict = agg_df.to_dict(orient="records")

        # --- SIMILARITIES --- #
        if stat_type == 'batting':
            similarity_df = rsim.run_similarity_batting(playerID, 9)
            retroIDs = similarity_df['retroID'].tolist()
            similarity_df['Player Name'] = similarity_df['first_name'] + ' ' + similarity_df['last_name']
            player_names = similarity_df['Player Name'].tolist()
            simData = {}
            for pid in retroIDs:
                simData[pid] = rtraj.run_career_trajectory_batting(pid).to_dict(orient="records")
        else:
            similarity_df = rsim.run_similarity_pitching(playerID, 9)
            retroIDs = similarity_df['retroID'].tolist()
            similarity_df['Player Name'] = similarity_df['first_name'] + ' ' + similarity_df['last_name']
            player_names = similarity_df['Player Name'].tolist()
            simData = {}
            for pid in retroIDs:
                simData[pid] = rtraj.run_career_trajectory_pitching(pid).to_dict(orient="records")
            
        sim_scores = similarity_df[['Player Name', 'SIM_SCORE']].copy()
        sim_scores.rename(columns={'SIM_SCORE': 'Similarity Score'}, inplace=True)
        sim_scores = sim_scores.to_dict(orient="records")
            
        # --- CIRCLE BAR --- #
        if stat_type == 'batting':
            circleBar_df = df[['year', 'AVG', 'SLG', 'OBP', 'OPS', 'K%', 'BB%', 'SB%', 'FLD%']].copy()
        else:
            circleBar_df = df[['year', 'ERA', 'WHIP', 'AVG', 'K9', 'BB9', 'HR%', 'CS%', 'FLD%']].copy()
        circleBar_dict = circleBar_df.to_dict(orient="records")

        context = {
            'results': {
                'mainStat': stat_type,
                'aggStats': agg_dict[0],
                'simData': simData,
                'simScores': sim_scores,
                'playerNames': player_names,
                'circleBarData': circleBar_dict
            }
        }
    
        return Response(context)


# home page
def home_page(request):
    return render(request, 'pages/homepage.html')


# project page
def project_markdown(request):

    page_height = 1050
    f = open('baseball/README.md', 'r')
    if f.mode == 'r':
        readme = f.read()
        page_height = len(readme)/2 + 200

    content = {
        'readme': readme,
        'page_height': page_height
    }

    template_page = get_this_template('baseball', 'project.html')

    return render(request, template_page, content)


# load drop-down teams based on year
def load_teams(request):

    year = request.GET['year']
    teams = cf2.get_teams_list(year)
    teams.sort()  # sort alphabetical
    # team_choices = [(tm, tm) for tm in teams]

    return render(request, 'partials/teams_dropdown_options.html', {'teams': teams})


# view stats
def stats_view(request):

    # get the year and team choices then grab the ViewStats Form
    year_choices = cf2.get_years_list()
    teams = cf2.get_teams_list(2019)
    teams.sort()  # sort alphabetical
    team_choices = [(tm, tm) for tm in teams]
    form_view_stats = ViewStats(year_choices, team_choices, initial={'form_type': 'view_stats'})

    # process all columns and send it out
    col_types = {'batting': gv2.bat_stat_types, 'pitching': gv2.pitch_stat_types, 'fielding': gv2.field_stat_types}
    all_cols = dict()
    for k, ct in enumerate(col_types):
        viewer_col = col_types[ct].copy()
        viewer_col.pop('PID')
        viewer_col.pop('YEAR')

        # get the keys and values, then add in name to the viewer_col
        query_col = ['player_nm'] + list(viewer_col.values())
        post_col_keys = ['NAME'] + list(viewer_col.keys())

        # change columns into ajax format for the DataTable.js
        ajax_col = []
        for col in post_col_keys:
            ac = {'data': col, 'title': col}
            ajax_col.append(ac)

        # query_col -- dict of post_col vs column desc
        query_col = [q.replace('_', ' ') for q in query_col]
        col_dict = dict(zip(post_col_keys, query_col))

        ct_dict = {
            'column_name': ajax_col,
            'column_desc': col_dict
        }

        all_cols[ct] = ct_dict

    # change heading
    heading = "Batting Stats for ANA in 2019"

    context = {
        'form_view_stats': form_view_stats,
        'col_types': all_cols
    }

    return render(request, 'pages/viewStats.html', context)


# dashboard version 2, with the new db schema
def jobs_dashboard2(request):
    
    # connect to db
    conn = dbs2.engine.connect()
    
    #  -----  JOB PROCESS RUNS  -----  #
    
    # get process group runs + time elapsed (combined for teamID not None)
    query = 'SELECT p.group_name, y.year, time_elapsed FROM process_log pl ' \
            'LEFT JOIN process_dim p ON pl.processID=p.Id ' \
            'LEFT JOIN years_dim y ON pl.yearID=y.Id'
    results = conn.execute(query)
    process_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    
    # assign value per group process and label so that we can keep the order
    processes = process_df['group_name'].unique().tolist()
    group_df = pd.DataFrame({'Id': range(0,len(processes)), 'group_name': processes})
    process_df = process_df.merge(group_df, on=['group_name'])
    
    process_df = process_df.groupby(['Id', 'group_name', 'year'], as_index=False)['time_elapsed'].sum()
    process_df.rename(columns={'time_elapsed': 'times'}, inplace=True)
    
    # shorten the times length
    process_df['times'] = process_df['times'].apply(lambda x: round(x, 3))
    
    # prep the times into a list by year, order is default correct
    new_df = process_df[['year', 'times']]
    new_df = new_df.groupby(['year'])['times'].apply(list).reset_index(name='times')
    process_dict = new_df.to_dict(orient='records')
    
    #  ------------------------------  #
    
    #  -----  KEY STATISTICS  -----  #
    # get the numbers
    query = 'SELECT ("{};" || COUNT(Id)) FROM players_dim UNION '.format('total players') + \
            'SELECT ("{};" || COUNT(Id)) FROM gameplay UNION '.format('number of plays processed') + \
            'SELECT ("{};" || COUNT(Id)) FROM games_dim UNION '.format('total games') + \
            'SELECT ("{};" || COUNT(Id)) FROM import_years WHERE imported=? UNION '.format('years processed') + \
            'SELECT ("{};" || SUM(tests_count)) FROM tests_log WHERE status=? UNION '.format('total test cases') + \
            'SELECT ("{};" || COUNT(Id)) FROM test_errors WHERE known_error=?'.format('unknown test errors')
    results = conn.execute(query, True, 'Completed', False).fetchall()
    key_stats = [k for (k,) in results]
    key_stats.sort()  # sort alphabetical
    key_stats = [{'stat': k.split(';')[0], 'value': k.split(';')[1]} for k in key_stats]
    
    #  ----------------------------  #
    
    #  -----  TEST CASE ERRORS  -----  #
    # get test case unknown errors
    query = 'SELECT (p.first_name || " " || p.last_name) as player_nm, st.stat_type, y.year, ' \
            'te.testID, te.error, te.self_value, te.test_value FROM test_errors te ' \
            'LEFT JOIN players_dim p ON te.playerID=p.Id ' \
            'LEFT JOIN stat_type_dim st ON te.stat_typeID=st.Id ' \
            'LEFT JOIN tests_log t ON te.testID=t.Id ' \
            'LEFT JOIN years_dim y ON t.yearID=y.Id ' \
            'WHERE te.known_error=? AND te.resolved=?'
    results = conn.execute(query, False, False)
    test_errors = pd.DataFrame(results.fetchall(), columns=results.keys())
    if len(test_errors) > 0:
        test_errors.sort_values(by=['testID'], ascending=False, inplace=True)
        test_dict = test_errors.head(10).to_dict(orient='records')
    else:
        test_dict = []
    
    #  ------------------------------  #
    
    #  -----  LEADERBOARD DATA  -----  #
    
    # get team stats (move this tally to a job in future)
    query = 'SELECT year, H, HR, RBI FROM batting b ' \
            'LEFT JOIN years_dim y ON b.yearID=y.Id ' \
            'LEFT JOIN teams_dim t ON b.teamID=t.Id'
    results = conn.execute(query)
    leaders_df = pd.DataFrame(results.fetchall(), columns=results.keys())
    leaders_df = leaders_df.groupby(['year'], as_index=False)[['H', 'HR', 'RBI']].sum()
    
    # top 50 offensive; convert to dictionary
    leaders_df.sort_values(by=['RBI'], ascending=False, inplace=True)
    leaders_df = leaders_df.head(10)
    leaders_dict = leaders_df.to_dict(orient='records')

    #  ------------------------------  #
    
    context = {
        'processes': processes,
        'process_data': process_dict,
        'key_stats': key_stats,
        'error_data': test_dict,
        'team_data': leaders_dict
    }

    return render(request, 'pages/jobsDashboard2.html', context)


# show the process flowchart of baseball project
def process_flowchart(request):

    context = {

    }

    return render(request, 'pages/flowchart.html', context)


# fav players dashboard
def fav_players(request):
    
    # suzui001, hallr001, kersc001, bettm001, verlj001, staim001
    players = [
        {'name': 'Ichiro Suzuki', 'playerID': 'suzui001', 'stat_type': 'batting'},
        {'name': 'Roy Halladay', 'playerID': 'hallr001', 'stat_type': 'pitching'},
        {'name': 'Clayton Kershaw', 'playerID': 'kersc001', 'stat_type': 'pitching'},
        {'name': 'Mookie Betts', 'playerID': 'bettm001', 'stat_type': 'batting'},
        {'name': 'Justin Verlander', 'playerID': 'verlj001', 'stat_type': 'pitching'},
        {'name': 'Matt Stairs', 'playerID': 'staim001', 'stat_type': 'batting'}
    ]
    
    # form for dropdown
    favForm = GetFavPlayer(players, prefix="fav_players")
    
    # default to ichiro
    df = ra2.get_circleBar_stats('suzui001', 'batting')

    # --- CAREER STATS --- #
    # get int columns
    int_cols = list(df.select_dtypes(include=['int64']))
    int_cols.extend(['first_name'])  # keep name for aggregate use
    int_cols.remove('year')

    # aggregate
    agg_df = df[int_cols].copy()
    agg_df = agg_df.groupby(['first_name'], as_index=False)[[col for col in int_cols if col != 'first_name']].sum()
    agg_dict = agg_df.to_dict(orient="records")
    
    # --- SIMILARITIES --- #
    similarity_df = rsim.run_similarity_batting('suzui001', 9)
    retroIDs = similarity_df['retroID'].tolist()
    similarity_df['Player Name'] = similarity_df['first_name'] + ' ' + similarity_df['last_name']
    player_names = similarity_df['Player Name'].tolist()
    simData = {}
    for pid in retroIDs:
        simData[pid] = rtraj.run_career_trajectory_batting(pid).to_dict(orient="records")
    sim_scores = similarity_df[['Player Name', 'SIM_SCORE']].copy()
    sim_scores.rename(columns={'SIM_SCORE': 'Similarity Score'}, inplace=True)
    sim_scores = sim_scores.to_dict(orient="records")
    
    # --- CIRCLE BAR --- #
    circleBar_df = df[['year', 'AVG', 'SLG', 'OBP', 'OPS', 'K%', 'BB%', 'SB%', 'FLD%']].copy()
    circleBar_dict = circleBar_df.to_dict(orient="records")
    
    context = {
        'favForm': favForm,
        'results': {
            'mainStat': 'batting',
            'aggStats': agg_dict[0],
            'simData': simData,
            'simScores': sim_scores,
            'playerNames': player_names,
            'circleBarData': circleBar_dict
        }
    }
    
    return render(request, 'pages/favPlayers.html', context)
